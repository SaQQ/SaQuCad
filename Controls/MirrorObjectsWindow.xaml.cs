﻿using SaQuCAD.Tools;
using System.Windows;

namespace SaQuCAD.Controls
{
	/// <summary>
	/// Interaction logic for MirrorEntitiesWindow.xaml
	/// </summary>
	public partial class MirrorObjectsWindow : Window
	{

		#region Constructors

		public MirrorObjectsWindow(MirrorObjectsVM viewModel)
		{
			InitializeComponent();

			DataContext = this.viewModel = viewModel;

			viewModel.MirroringDone += mirroringDone;
			Closing += WindowClosing;
		}

		#endregion

		#region Private Helpers

		private void mirroringDone()
		{
			Closing -= WindowClosing;
			Close();
		}

		private void WindowClosing(object sender, System.ComponentModel.CancelEventArgs e)
		{
			viewModel.MirroringDone -= mirroringDone;
			viewModel.Reject.Execute(null);
		}

		#endregion

		#region Private Data

		private MirrorObjectsVM viewModel;

		#endregion

	}
}
