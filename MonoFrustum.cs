﻿using SaQuCAD.Maths;
using System;

namespace SaQuCAD
{
	public class MonoFrustum : Frustum
	{
		public Matrix4x4 ProjectionMatrix { get; private set; }
		public Matrix4x4 InvertedProjectionMatrix { get; private set; }

		public MonoFrustum(double nearDist, double farDist, double aspect, double fov = 45.0)
		{
			double horizontalAngle = fov * System.Math.PI / 360.0;
			var xmax = System.Math.Tan(horizontalAngle) * nearDist;
			var ymax = xmax * aspect;
			double verticalAngle = System.Math.Atan2(ymax, nearDist);

			double sinh = System.Math.Sin(horizontalAngle);
			double sinv = System.Math.Sin(verticalAngle);
			double cosh = System.Math.Cos(horizontalAngle);
			double cosv = System.Math.Cos(verticalAngle);

			Plane left = new Plane(new Vector3(cosh, 0, -sinh), new Vector3(0, 0, 0));
			Plane right = new Plane(new Vector3(-cosh, 0, -sinh), new Vector3(0, 0, 0));
			Plane top = new Plane(new Vector3(0, -cosv, -sinv), new Vector3(0, 0, 0));
			Plane bottom = new Plane(new Vector3(0, cosv, -sinv), new Vector3(0, 0, 0));
			Plane near = new Plane(new Vector3(0, 0, -1), new Vector3(0, 0, -nearDist));
			Plane far = new Plane(new Vector3(0, 0, 1), new Vector3(0, 0, -farDist));
			
			ClipPlanes = new Plane[] { left, right, top, bottom, near, far };

			ProjectionMatrix = Matrix4x4.Frustum(-xmax, xmax, -ymax, ymax, nearDist, farDist);
			InvertedProjectionMatrix = ProjectionMatrix.Inverse();
		}

	}
}
