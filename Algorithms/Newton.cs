﻿using SaQuCAD.Maths;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SaQuCAD.Algorithms
{
	public static class Newton
	{

		#region Public Types

		public delegate bool WrapFuncDelegate<T>(ref T vec);

		#endregion

		#region Public Methods

		public static int MaxIterations = 100000;

        public static Vector4 Solve(Func<Vector4, Vector4> fun, Func<Vector4, Matrix4x4> df, WrapFuncDelegate<Vector4> wrapFunc, Vector4 start)
		{            
			var xk = start;
            int iters = 0;
            do
            {
                var fxk = fun(xk);

                var jacobian = df(xk);

				var invJacobian = jacobian.Inverse();

                var xnk = xk - (invJacobian * fxk);

                if (wrapFunc(ref xnk))
                    return xnk;

                var fxnk = fun(xnk);

                if ((xk - xnk).LengthSquared < Constants.Eps * Constants.Eps)
                    return xnk;

                xk = xnk;

                iters++;
            } while (iters < MaxIterations);

			throw new Exception("Newton method did not converge");

		}

		#endregion

	}
}
