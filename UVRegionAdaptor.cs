﻿using System;
using System.Collections.ObjectModel;
using SaQuCAD.Maths;
using SaQuCAD.Shapes.Properties;

namespace SaQuCAD
{
    public class UVRegionAdaptor : IUVParametrized
    {

        #region Public Properties

        public int USegments { get { throw new NotImplementedException(); } set { throw new NotImplementedException(); } }

        public int VSegments { get { throw new NotImplementedException(); } set { throw new NotImplementedException(); } }

        public double UMin => uMin;

        public double UMax => uMax;

        public double VMin => vMin;

        public double VMax => vMax;

        public ParamProps UProperties => 0;

        public ParamProps VProperties => 0;

        public ObservableCollection<ITrimmer> Trimmers { get { throw new NotImplementedException(); } }

        public TrimMode TrimMode { get { throw new NotImplementedException(); } set { throw new NotImplementedException(); } }

        public Vector3 Evaluate(double u, double v)
        {
            return surf.Evaluate(u, v);
        }

        public Vector3 EvaluateUDerivative(double u, double v)
        {
            return surf.EvaluateUDerivative(u, v);
        }

        public Vector3 EvaluateVDerivative(double u, double v)
        {
            return surf.EvaluateVDerivative(u, v);
        }

        #endregion

        #region Constructors

        public UVRegionAdaptor(IUVParametrized surface, double uMin, double uMax, double vMin, double vMax)
        {
            this.uMin = uMin;
            this.uMax = uMax;
            this.vMin = vMin;
            this.vMax = vMax;

            surf = surface;
        }

        #endregion

        #region Private Data

        private IUVParametrized surf;

        private double uMin, uMax, vMin, vMax;

        #endregion

    }
}
