﻿using System.Windows;
using System.Windows.Media.Imaging;

namespace SaQuCAD
{
    /// <summary>
    /// Interaction logic for Window1.xaml
    /// </summary>
    public partial class ParametrizationViewer : Window
    {
        public ParametrizationViewer(WriteableBitmap bitmap)
        {
            InitializeComponent();
            Width = bitmap.Width;
            Height = bitmap.Height;
            Raster.Source = bitmap;
        }
    }
}
