﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace SaQuCAD.Controls
{
	/// <summary>
	/// Interaction logic for IntegerValueChooser.xaml
	/// </summary>
	public partial class IntegerValueChooser : UserControl
	{
		public IntegerValueChooser()
		{
			InitializeComponent();
		}

		public int Value
		{
			get { return (int)GetValue(ValueProperty); }
			set { SetValue(ValueProperty, value); }
		}

		public static readonly DependencyProperty ValueProperty =
			DependencyProperty.Register("Value", typeof(int), typeof(IntegerValueChooser), new PropertyMetadata(0));

		public string Text
		{
			get { return (string)GetValue(TextProperty); }
			set { SetValue(TextProperty, value); }
		}

		public static readonly DependencyProperty TextProperty =
			DependencyProperty.Register("Text", typeof(string), typeof(IntegerValueChooser), new PropertyMetadata(string.Empty));



		public int Minimum
		{
			get { return (int)GetValue(MinimumProperty); }
			set { SetValue(MinimumProperty, value); }
		}

		// Using a DependencyProperty as the backing store for Minimum.  This enables animation, styling, binding, etc...
		public static readonly DependencyProperty MinimumProperty =
			DependencyProperty.Register("Minimum", typeof(int), typeof(IntegerValueChooser), new PropertyMetadata(0));



		public int Maximum
		{
			get { return (int)GetValue(MaximumProperty); }
			set { SetValue(MaximumProperty, value); }
		}

		// Using a DependencyProperty as the backing store for Maximum.  This enables animation, styling, binding, etc...
		public static readonly DependencyProperty MaximumProperty =
			DependencyProperty.Register("Maximum", typeof(int), typeof(IntegerValueChooser), new PropertyMetadata(100));



		public int Increment
		{
			get { return (int)GetValue(IncrementProperty); }
			set { SetValue(IncrementProperty, value); }
		}

		// Using a DependencyProperty as the backing store for Increment.  This enables animation, styling, binding, etc...
		public static readonly DependencyProperty IncrementProperty =
			DependencyProperty.Register("Increment", typeof(int), typeof(IntegerValueChooser), new PropertyMetadata(1));


	}
}
