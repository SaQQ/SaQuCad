﻿using System;
using System.Linq;

namespace SaQuCAD
{
	public partial class Scene
	{

		#region Private Helpers

		private void SaveScene(string path)
		{
			var points = SceneObjects.OfType<Shapes.Point>().ToArray();

			var scene = new GM1.Serialization.Scene()
			{
				Points = points.Select(p => new GM1.Serialization.Point()
				{
					Name = p.Name,
					Position = new GM1.Serialization.Vector3()
					{
						X = (float)p.X,
						Y = (float)p.Y,
						Z = (float)p.Z
					}
				}).ToArray(),
				BezierCurvesC0 = SceneObjects.OfType<Shapes.Curves.BezierC0>().Select(curve => new GM1.Serialization.BezierCurveC0()
				{
					Name = curve.Name,
					Points = curve.ControlPoints.Select(p => Array.IndexOf(points, p)).ToArray()
				}).ToArray(),
				BezierCurvesC2 = SceneObjects.OfType<Shapes.Curves.BSpline>().Select(curve => new GM1.Serialization.BezierCurveC2()
				{
					Name = curve.Name,
					Points = curve.ControlPoints.Select(p => Array.IndexOf(points, p)).ToArray()
				}).ToArray(),
				InterpolationBezierCurvesC2 = SceneObjects.OfType<Shapes.Curves.InterpolatingBSpline>().Select(curve => new GM1.Serialization.InterpolationBezierCurveC2()
				{
					Name = curve.Name,
					Points = curve.ControlPoints.Select(p => Array.IndexOf(points, p)).ToArray()
				}).ToArray(),
				BezierSurfacesC0 = SceneObjects.OfType<Shapes.Surfaces.BezierC0>().Select(surface =>
				{
					var patchesU = surface.ControlPoints.GetLength(0) / 3;
					var patchesV = surface.ControlPoints.GetLength(1) / 3;
					var ret = new GM1.Serialization.BezierSurfaceC0()
					{
						Name = surface.Name,
						PatchesU = patchesU,
						PatchesV = patchesV,
						Patches = new GM1.Serialization.BezierSurfaceC0Patch[patchesU * patchesV]
					};

					for (int uPatch = 0; uPatch < ret.PatchesU; ++uPatch)
					{
						for(int vPatch = 0; vPatch < ret.PatchesV; ++vPatch)
						{
							GM1.Serialization.PointsU4V4 cp = new GM1.Serialization.PointsU4V4();

							for (int u = 0; u < 4; ++u)
								for (int v = 0; v < 4; ++v)
									cp[u, v] = Array.IndexOf(points, surface.ControlPoints[uPatch * 3 + u, vPatch * 3 + v]);

							ret.Patches[uPatch * patchesV + vPatch] = new GM1.Serialization.BezierSurfaceC0Patch()
							{
								PatchU = uPatch,
								PatchV = vPatch,
								Points = cp,
								SurfaceDivisionsU = surface.USegments,
								SurfaceDivisionsV = surface.VSegments,
								Name = surface.Name + "(" + uPatch + " - " + vPatch + ")"
							};
						}
					}

					return ret;
				}).ToArray(),
				BezierSurfacesC2 = SceneObjects.OfType<Shapes.Surfaces.BSpline>().Select(surface =>
				{
					var patchesU = surface.ControlPoints.GetLength(0) - 3;
					var patchesV = surface.ControlPoints.GetLength(1) - 3;
					var ret = new GM1.Serialization.BezierSurfaceC2()
					{
						Name = surface.Name,
						PatchesU = patchesU,
						PatchesV = patchesV,
						Patches = new GM1.Serialization.BezierSurfaceC2Patch[patchesU * patchesV]
					};

					for (int uPatch = 0; uPatch < ret.PatchesU; ++uPatch)
					{
						for (int vPatch = 0; vPatch < ret.PatchesV; ++vPatch)
						{
							GM1.Serialization.PointsU4V4 cp = new GM1.Serialization.PointsU4V4();

							for (int u = 0; u < 4; ++u)
								for (int v = 0; v < 4; ++v)
									cp[u, v] = Array.IndexOf(points, surface.ControlPoints[uPatch + u, vPatch + v]);

							ret.Patches[uPatch * patchesV + vPatch] = new GM1.Serialization.BezierSurfaceC2Patch()
							{
								PatchU = uPatch,
								PatchV = vPatch,
								Points = cp,
								SurfaceDivisionsU = surface.USegments,
								SurfaceDivisionsV = surface.VSegments,
								Name = surface.Name + "(" + uPatch + " - " + vPatch + ")"
							};
						}
					}

					return ret;
				}).ToArray()
			};

			xmlSerializer.SerializeToFile(path, scene);
		}

		private void LoadScene(string path)
		{
			Clear();

			var scene = xmlSerializer.DeserializeFromFile(path);

			var points = scene.Points.Select(p => new Shapes.Point(new Maths.Vector3(p.Position.X, p.Position.Y, p.Position.Z), p.Name)).ToArray();

			foreach (var point in points)
				AddObject(point);

			foreach (var curve in scene.BezierCurvesC0.Select(curve => new Shapes.Curves.BezierC0(curve.Points.Select(i => points[i]).ToArray(), curve.Name)))
				AddObject(curve);

			foreach (var curve in scene.BezierCurvesC2.Select(curve => new Shapes.Curves.BSpline(curve.Points.Select(i => points[i]).ToArray(), curve.Name)))
				AddObject(curve);

			foreach (var curve in scene.InterpolationBezierCurvesC2.Select(curve => new Shapes.Curves.InterpolatingBSpline(curve.Points.Select(i => points[i]).ToArray(), curve.Name)))
				AddObject(curve);

			foreach (var surface in scene.BezierSurfacesC0.Select(surf => {

				var cp = new Shapes.Point[surf.PatchesU * 3 + 1, surf.PatchesV * 3 + 1];

				int divU = 0;
				int divV = 0;

				for(int uPatch = 0; uPatch < surf.PatchesU; ++uPatch)
				{
					for (int vPatch = 0; vPatch < surf.PatchesV; ++vPatch)
					{
						var patch = surf.Patches.Where(p => p.PatchU == uPatch && p.PatchV == vPatch).First();

						divU = Math.Max(divU, patch.SurfaceDivisionsU);
						divV = Math.Max(divV, patch.SurfaceDivisionsV);

						for (int u = 0; u < 4; ++u)
							for (int v = 0; v < 4; ++v)
								cp[uPatch * 3 + u, vPatch * 3 + v] = points[patch.Points[u, v]];
					}
				}

				return new Shapes.Surfaces.BezierC0(cp, surf.Name)
				{
					USegments = divU,
					VSegments = divV
				};
			}))
				AddObject(surface);

			foreach (var surface in scene.BezierSurfacesC2.Select(surf =>
			{
				var cp = new Shapes.Point[surf.PatchesU + 3, surf.PatchesV + 3];

				int divU = 0;
				int divV = 0;

				for (int uPatch = 0; uPatch < surf.PatchesU; ++uPatch)
				{
					for (int vPatch = 0; vPatch < surf.PatchesV; ++vPatch)
					{
						var patch = surf.Patches.Where(p => p.PatchU == uPatch && p.PatchV == vPatch).First();

						divU = Math.Max(divU, patch.SurfaceDivisionsU);
						divV = Math.Max(divV, patch.SurfaceDivisionsV);

						for (int u = 0; u < 4; ++u)
							for (int v = 0; v < 4; ++v)
								cp[uPatch + u, vPatch + v] = points[patch.Points[u, v]];
					}
				}

                // TODO: Load cyclicness
				return new Shapes.Surfaces.BSpline(cp, surf.Name)
				{
					USegments = divU,
					VSegments = divV
				};
			}))
				AddObject(surface);

		}

		#endregion

	}
}
