﻿using SaQuCAD.Maths;
using System;

namespace SaQuCAD.Algorithms
{
	public static class Gradients
	{

        #region Public Methods

        public static int MaxIterations = 1000000;

		public static Vector4 Simple(Func<Vector4, double> f, Func<Vector4, Vector4> g, Vector4 x, Newton.WrapFuncDelegate<Vector4> wrapFunc)
		{
            int iters = 0;
			var alpha = 0.01;
			var fx = f(x);

			do
			{
				var xn = x - alpha * g(x);

				if (wrapFunc(ref xn))
					return x;

				var fxn = f(xn);
				
				if(fxn > fx)
				{
					alpha /= 2.0;
					continue;
				}

                if ((xn - x).LengthSquared <= Constants.Eps * Constants.Eps)
                    return xn;

				x = xn;
                fx = fxn;
                ++iters;
				
			} while (iters < MaxIterations);

            throw new Exception("Gradient method did not converge");

		}

		public static Vector3 Simple(Func<Vector3, double> f, Func<Vector3, Vector3> g, Vector3 x, Newton.WrapFuncDelegate<Vector3> wrapFunc)
		{
			int iters = 0;
			var alpha = 0.01;
			var fx = f(x);

			do
			{
				var xn = x - alpha * g(x);

				if (wrapFunc(ref xn))
					return x;

				var fxn = f(xn);

				if (fxn > fx)
				{
					alpha /= 2.0;
					continue;
				}

				if ((xn - x).LengthSquared <= Constants.Eps * Constants.Eps)
					return xn;

				x = xn;
				fx = fxn;
				++iters;

			} while (iters < MaxIterations);

			throw new Exception("Gradient method did not converge");

		}

		public static Vector2 Simple(Func<Vector2, double> f, Func<Vector2, Vector2> g, Vector2 x, Newton.WrapFuncDelegate<Vector2> wrapFunc)
        {
            int iters = 0;
            var alpha = 0.01;
            var fx = f(x);

            do
            {
                var xn = x - alpha * g(x);

                if (wrapFunc(ref xn))
                    return x;

                var fxn = f(xn);

                if (fxn > fx)
                {
                    alpha /= 2.0;
                    continue;
                }

                if ((xn - x).LengthSquared <= Constants.Eps * Constants.Eps)
                    return xn;

                x = xn;
                fx = fxn;
                ++iters;
            } while (iters < MaxIterations);

            throw new Exception("Gradient method did not converge");

        }

		public static double Simple(Func<double, double> f, Func<double, double> g, double x, Newton.WrapFuncDelegate<double> wrapFunc)
		{
			int iters = 0;
			var alpha = 0.01;
			var fx = f(x);

			do
			{
				var xn = x - alpha * g(x);

				if (wrapFunc(ref xn))
					return x;

				var fxn = f(xn);

				if (fxn > fx)
				{
					alpha /= 2.0;
					continue;
				}

				if ((xn - x) <= Constants.Eps)
					return xn;

				x = xn;
				fx = fxn;
				++iters;

			} while (iters < MaxIterations);

			throw new Exception("Gradient method did not converge");

		}

		#endregion

	}
}
