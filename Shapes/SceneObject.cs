﻿using SaQuCAD.Infrastructure;
using System.Collections.Generic;
using System.Windows.Input;

namespace SaQuCAD.Shapes
{
	public delegate void SceneObjectRemovedEventHandler(SceneObject obj);

	public abstract class SceneObject : BindableObject
	{

		public ICommand Remove { get; private set; }

		#region Constructors

		public SceneObject()
		{
			Remove = new RelayCommand(() => { OnRemoved(); });
        }

		#endregion

		public event ChangedEventHandler Changed;

		protected virtual void OnChanged()
		{
            Changed?.Invoke();
        }

		public event SceneObjectRemovedEventHandler Removed;

		protected virtual void OnRemoved()
		{
            Removed?.Invoke(this);
        }

		private string name;

		public string Name
		{
			get { return name; }
			set { name = value; RaisePropertyChanged(); }
		}

        private bool selected = false;

        public bool Selected
        {
            get { return selected; }
            set { selected = value; RaisePropertyChanged(); OnChanged(); }
        }

		public int ColorCode
		{
			get { return Selected ? ColorCodes.RenderSelected : ColorCodes.RenderPrimary; }
		}

		private bool visible = true;

		public bool Visible
		{
			get { return visible; }
			set { visible = value; RaisePropertyChanged(); OnChanged(); }
		}

        public abstract void Render(Renderer renderer);

		public virtual IEnumerable<SceneObject> Mirror(Plane plane)
		{
			return null;
		}

	}
}
