﻿using SaQuCAD.Maths;
using SaQuCAD.Shapes.Properties;

namespace SaQuCAD.Shapes.Curves
{
	public class Segment : SceneObject, IUParametrized
	{

		#region Contructors

		public Segment(Point from, Point to, string name = "New Segment")
		{
			Name = name;

			this.from = from;
			this.to = to;

			from.Changed += PointChanged;
			to.Changed += PointChanged;
			from.Removed += PointRemoved;
			to.Removed += PointRemoved;
		}

		#endregion

		#region Public Properties

		public double UMin => 0;

		public double UMax => (to.Position - from.Position).Length;

		#endregion

		#region Public Methods

		public Vector3 Evaluate(double u)
		{
			double t = u / UMax;
			return (1 - t) * from.Position + t * to.Position;
		}

		public Vector3 EvaluateDerivative(double u)
		{
			return (to.Position - from.Position) / UMax;
		}

		public override void Render(Renderer renderer)
		{
			if (!Visible)
				return;

			renderer.DrawSegment(from.Position, to.Position, ColorCode);
		}

		#endregion

		#region Private Member

		private Point from, to;

		private void PointRemoved(SceneObject obj)
		{
			OnRemoved();
		}

		private void PointChanged()
		{
			OnChanged();
		}

		#endregion

	}
}
