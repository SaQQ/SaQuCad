﻿using SaQuCAD.Infrastructure;
using SaQuCAD.Maths;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace SaQuCAD.Shapes.Surfaces.CreationViewModels
{
	public delegate void EditingDoneEventHanlder();

	public abstract class ControlpointsBaseCreationVM : BindableObject
	{

		#region Public Properties

		public double Width { get { return width; } set { width = value; RepositionPoints(); RaisePropertyChanged(); } }

		public double Height { get { return height; } set { height = value; RepositionPoints(); RaisePropertyChanged(); } }

		public int WidthSegments { get { return widthSegments; } set { widthSegments = value; RemoveAndRecreateSurface(); RaisePropertyChanged(); } }

		public int HeightSegments { get { return heightSegments; } set { heightSegments = value; RemoveAndRecreateSurface(); RaisePropertyChanged(); } }

		public bool Looped { get { return looped; } set { looped = value; RemoveAndRecreateSurface(); RaisePropertyChanged(); } }

		public Plane CreationPlane { get { return creationPlane; } set { creationPlane = value; RemoveAndRecreateSurface(); RaisePropertyChanged(); } }

		public ICommand ResetAll { get; private set; }

		public ICommand Apply { get; private set; }

		public ICommand Reject { get; private set; }

		public event EditingDoneEventHanlder EditingDone;

		#endregion

		#region Constructors

		public ControlpointsBaseCreationVM(Scene scene)
		{
			this.scene = scene;

			scene.D3Cursor.Changed += CursorChanged;

			CreateSurface(scene);

			ResetAll = new RelayCommand(() => RemoveAndRecreateSurface());

			Apply = new RelayCommand(() =>
			{
				Done();
			});

			Reject = new RelayCommand(() =>
			{
				RemoveSurface();
				Done();
			});
		}

		#endregion

		#region Protected Methods

		protected abstract void CreateSurface(Scene scene);

		protected abstract void RepositionPoints();

		protected abstract void RemoveSurface();

		protected Vector3 LocalToGlobal(Vector3 point)
		{
			switch (creationPlane)
			{
				case Plane.XY:
					return point + scene.D3Cursor.Position;
				case Plane.XZ:
					return new Vector3(point.x, point.z, point.y) + scene.D3Cursor.Position;
				case Plane.YZ:
					return new Vector3(point.z, point.x, point.y) + scene.D3Cursor.Position;
			}

			throw new ArgumentException("Unknown plane");
		}

		#endregion

		#region Private Helpers

		private void Done()
		{
			scene.D3Cursor.Changed -= CursorChanged;
			EditingDone?.Invoke();
		}

		private void CursorChanged()
		{
			RepositionPoints();
		}

		private void RemoveAndRecreateSurface()
		{
			RemoveSurface();
			CreateSurface(scene);
		}

		#endregion

		#region Private Data

		//private Point[,] points;

		//private BezierC0 surface;

		private Scene scene;

		private double width = 0.5, height = 0.5;

		private int widthSegments = 1, heightSegments = 1;

		private bool looped = false;

		private Plane creationPlane = Plane.XY;

		#endregion

	}
}
