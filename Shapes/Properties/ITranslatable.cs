﻿using SaQuCAD.Maths;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SaQuCAD.Shapes.Properties
{
	public interface ITranslatable
	{
		double X { get; set; }
		double Y { get; set; }
		double Z { get; set; }

		Vector3 Position { get; set; }
	}
}
