﻿using SaQuCAD.Maths;

namespace SaQuCAD.Algorithms
{
	public static class Lagrange
	{

		#region Public Methods

		public static Vector4 EvaluateBasis3(double t)
		{
			Vector4 B = new Vector4();

            var tt = t * t;
            var ttt = tt * t;

            B.x = -9.0 / 2.0 * ttt + 9.0 * tt - 11.0 / 2.0 * t + 1.0;
            B.y = 27.0 / 2.0 * ttt - 45.0 / 2.0 * tt + 9.0 * t;
            B.z = -27.0 / 2.0 * ttt + 18.0 * tt - 9.0 / 2.0 * t;
            B.w = 9.0 / 2.0 * ttt - 9.0 / 2.0 * tt + t;

			return B;
		}

		#endregion

	}
}
