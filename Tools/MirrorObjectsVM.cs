﻿using SaQuCAD.Infrastructure;
using SaQuCAD.Shapes;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Windows.Input;

namespace SaQuCAD.Tools
{

	public delegate void MirroringDoneEventHanlder();

	public class MirrorObjectsVM : BindableObject
	{

		#region Public Properties

		public ObservableCollection<SceneObject> Objects { get; private set; } = new ObservableCollection<SceneObject>();

		public Shapes.Plane MirrorPlane { get { return mirrorPlane; } set { mirrorPlane = value; RemoveMirroredObjects(); MirrorObjects(); RaisePropertyChanged(); } }

		public ICommand Apply { get; private set; }

		public ICommand Reject { get; private set; }

		public event MirroringDoneEventHanlder MirroringDone;

		#endregion

		#region Constructors

		public MirrorObjectsVM(Scene scene, IEnumerable<SceneObject> objects)
		{
			this.scene = scene;

			foreach (var obj in objects)
				Objects.Add(obj);

			Apply = new RelayCommand(() =>
			{
				Done();
			});

			Reject = new RelayCommand(() =>
			{
				RemoveMirroredObjects();
				Done();
			});

			MirrorObjects();
		}

		#endregion

		#region Public Methods

		public void Done()
		{
			MirroringDone?.Invoke();
		}

		#endregion

		#region Private Helpers

		private void RemoveMirroredObjects()
		{
			foreach (var obj in mirroredObjects)
				obj.Remove.Execute(null);
			mirroredObjects.Clear();
		}

		private void MirrorObjects()
		{
			foreach (var obj in Objects)
				MirrorObject(obj);
		}

		private void MirrorObject(SceneObject obj)
		{
			var mirrored = obj.Mirror(mirrorPlane);
			if (mirrored != null)
			{
				foreach (var mirroredObj in mirrored)
				{
					mirroredObjects.Add(mirroredObj);
					scene.AddObject(mirroredObj);
				}
			}
		}

		#endregion

		#region Private Data

		private Scene scene;

		private Shapes.Plane mirrorPlane;

		private List<SceneObject> mirroredObjects = new List<SceneObject>();

		#endregion

	}
}
