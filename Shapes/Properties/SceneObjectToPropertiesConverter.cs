﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Globalization;
using System.Windows.Data;

namespace SaQuCAD.Shapes.Properties
{
	public class SceneObjectToPropertiesConverter : IValueConverter
	{
		public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
		{
			if (value == null)
				return new Type[0];

			return value.GetType().GetInterfaces().Where((Type t) => {
				return t.Namespace == typeof(SceneObjectToPropertiesConverter).Namespace;
			});
		}

		public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
		{
			throw new NotImplementedException();
		}
	}
}
