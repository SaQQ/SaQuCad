﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace SaQuCAD.Shapes
{
	public class ParametricMesh : SceneObject, IEnumerable<Segment>
	{
		public List<Vector4> Vertices = new List<Vector4>();
		public List<Tuple<int, int>> Indices = new List<Tuple<int, int>>();

		public IEnumerator<Segment> GetEnumerator()
		{
			foreach (Tuple<int, int> inds in Indices)
			{
				yield return new Segment()
				{
					From = Vertices[inds.Item1],
					To = Vertices[inds.Item2]
				};
			}
		}

		IEnumerator IEnumerable.GetEnumerator()
		{
			foreach (Tuple<int, int> inds in Indices)
			{
				yield return new Segment()
				{
					From = Vertices[inds.Item1],
					To = Vertices[inds.Item2]
				};
			}
		}
	}
}
