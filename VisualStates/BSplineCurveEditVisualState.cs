﻿using SaQuCAD.Maths;
using SaQuCAD.Shapes;
using SaQuCAD.Shapes.Curves;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace SaQuCAD.VisualStates
{
	public sealed class BSplineCurveEditVisualState : ControlpointsCurveEditVisualState
	{

		#region Protected Types

		private class BezierDragger : Dragger
		{

			#region Public Types

			public enum Mode
			{
				Next, Previous, Side
			}

			#endregion

			#region Public Properties

			public override Vector3 CurrentPosition { get { return currentPosition; } }

			#endregion

			#region Constructors

			public BezierDragger(BSpline bspline, int hookedDeBoorIndex, Mode dragMode, Vector3 catchPoint)
			{
				spline = bspline;
				deBoorIndex = hookedDeBoorIndex;
				mode = dragMode;
				currentPosition = catchPoint;
			}

			#endregion

			#region Public Methods

			public override void Drag(Vector3 to)
			{
				currentPosition = to;

				switch (mode)
				{
					case Mode.Next:
						spline.ControlPoints[deBoorIndex + 1].Position = spline.ControlPoints[deBoorIndex].Position + 3.0 * (to - spline.ControlPoints[deBoorIndex].Position) / 2.0;
						break;
					case Mode.Previous:
						spline.ControlPoints[deBoorIndex - 1].Position = spline.ControlPoints[deBoorIndex].Position + 3.0 * (to - spline.ControlPoints[deBoorIndex].Position) / 2.0;
						break;
					case Mode.Side:
						spline.ControlPoints[deBoorIndex].Position = (6.0 * to - spline.ControlPoints[deBoorIndex - 1].Position - spline.ControlPoints[deBoorIndex + 1].Position) / 4.0;
						break;
				}
			}

			#endregion

			#region Private Fields

			private BSpline spline;
			private Mode mode;
			private int deBoorIndex;
			private Vector3 currentPosition;

			#endregion

		}

		#endregion

		#region Public Properties

		public override string Description { get { return "Editing B-Spline Curve"; } }

		#endregion

		#region Constructors

		public BSplineCurveEditVisualState(Scene scene, BSpline bspline) : base(scene, bspline)
		{
		}

		#endregion

		public override void OnEnter()
		{
			base.OnEnter();
			(Curve as BSpline).DrawBezierPolygon = true;
		}

		public override void OnExit()
		{
			base.OnExit();
			(Curve as BSpline).DrawBezierPolygon = false;
		}

		#region Protected Methods 

		protected override Dragger TryClickSpecialPoints(IVector2 pos)
		{
			return TryClickBezierPoint(pos);
		}

		#endregion

		#region Private Helpers

		private Dragger TryClickBezierPoint(IVector2 pos)
		{
			var curve = Curve as BSpline;

			foreach (var bezierPolygon in curve.BezierPolygons())
			{
				if (TryClickCoords(pos, bezierPolygon.Points[0]))
					if (bezierPolygon.StartCollapsed)
						return new ScenePointDragger(curve.ControlPoints[bezierPolygon.DeBoorIndex]);
					else
						return new BezierDragger(curve, bezierPolygon.DeBoorIndex, BezierDragger.Mode.Side, bezierPolygon.Points[0]);
				else if (TryClickCoords(pos, bezierPolygon.Points[1]))
					return new BezierDragger(curve, bezierPolygon.DeBoorIndex + 1, BezierDragger.Mode.Previous, bezierPolygon.Points[1]);
				else if (TryClickCoords(pos, bezierPolygon.Points[2]))
					return new BezierDragger(curve, bezierPolygon.DeBoorIndex, BezierDragger.Mode.Next, bezierPolygon.Points[2]);
				else if (TryClickCoords(pos, bezierPolygon.Points[3]))
					if (bezierPolygon.EndCollapsed)
						return new ScenePointDragger(curve.ControlPoints[bezierPolygon.DeBoorIndex + 1]);
					else
						return new BezierDragger(curve, bezierPolygon.DeBoorIndex + 1, BezierDragger.Mode.Side, bezierPolygon.Points[3]);
			}

			return null;
		}

		#endregion

	}

}
