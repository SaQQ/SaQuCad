﻿using SaQuCAD.Shapes.Properties;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;

namespace SaQuCAD.Shapes.Surfaces
{
    public sealed class Gregory : SceneObject, IFrameable, IUVParametrized, IPointBased
    {

        #region Public Properties

        public bool FrameVisible { get { return frameVisible; } set { frameVisible = value; RaisePropertyChanged(); OnChanged(); } }

        public int USegments { get { return uSegments; } set { uSegments = value; OnUSegmentsChange(); RaisePropertyChanged(); OnChanged(); } }

        public int VSegments { get { return vSegments; } set { vSegments = value; OnVSegmentsChange(); RaisePropertyChanged(); OnChanged(); } }

        public double UMin => 0.0;

        public double UMax => 0.0;

        public double VMin => 0.0;

        public double VMax => 0.0;

        public ParamProps UProperties => 0;

        public ParamProps VProperties => 0;

        public ObservableCollection<ITrimmer> Trimmers { get; private set; } = new ObservableCollection<ITrimmer>();

        public TrimMode TrimMode { get; set; }

        #endregion

        #region Constructors

        public Gregory(List<Algorithms.Gregory.ISideDescriptor> sides, string name = "New Gregory Patch")
		{
			Name = name;
			this.sides = sides.Select(s => new SidePointsAdaptor(s)).ToArray();

			foreach (var side in sides)
				for (int u = 0; u < 4; ++u)
					for (int v = 0; v < 2; ++v)
						side.Points[u, v].Removed += PointRemoved;

			patches = new Patch[sides.Count];

			for (int i = 0; i < sides.Count; ++i)
				patches[i] = new Patch(this.sides[i], this.sides[(i + 1) % sides.Count], UpdateSides());

			OnUSegmentsChange();
			OnVSegmentsChange();
		}

		#endregion

		#region Public Methods

		public Maths.Vector3 Evaluate(double u, double v)
		{
			throw new NotImplementedException();
		}

		public Maths.Vector3 EvaluateUDerivative(double u, double v)
		{
			throw new NotImplementedException();
		}

		public Maths.Vector3 EvaluateVDerivative(double u, double v)
		{
			throw new NotImplementedException();
		}

		public override void Render(Renderer renderer)
		{
			var Qs = UpdateSides();

			UpdatePatches(Qs);

			foreach (var patch in patches)
			{
				patch.Draw(renderer, hermiteUValues, hermiteVValues, ColorCode);
				if(frameVisible)
					patch.DrawFrame(renderer);
			}
		}

		public int SwapPoint(Point existing, Point replacement)
		{
			var replaced = 0;

			foreach (var side in sides)
			{
				for (int u = 0; u < 4; ++u)
					for (int v = 0; v < 4; ++v)
						if (side.Side.Points[u, v] == existing)
						{
							side.Side.Points[u, v] = replacement;
							++replaced;
						}
			}
			return replaced;
		}

		#endregion

		#region Private Types

		private sealed class Patch
		{

			#region Public Properties

			public Maths.Vector3[] Points { get; private set; } = new Maths.Vector3[20];

			public Maths.Vector3 Q = new Maths.Vector3();

			public SidePointsAdaptor Side1 { get; private set; }

			public SidePointsAdaptor Side2 { get; private set; }

			#endregion

			#region Constructor

			public Patch(SidePointsAdaptor side1, SidePointsAdaptor side2, Maths.Vector3 Qs)
			{
				Side1 = side1;
				Side2 = side2;

				Update(Qs);
			}

			#endregion

			#region Public Methods

			public void Update(Maths.Vector3 Qs)
			{
				Points[0] = Side1.T[0, 0];
				Points[4] = Side1.S[1, 0];
				Points[10] = Side1.R[2, 0];

				Points[16] = Side1.Side.Points[3, 0].Position;

				Points[17] = Side2.R[0, 0];
				Points[18] = Side2.S[0, 0];
				Points[19] = Side2.T[0, 0];

				Points[1] = 2.0 * Points[0] - Side1.T[0, 1];
				Points[15] = 2.0 * Points[19] - Side2.T[0, 1];

				Points[5] = Points[6] = 2.0 * Points[4] - Side1.S[1, 1];
				Points[13] = Points[14] = 2.0 * Points[18] - Side2.S[0, 1];

				Points[11] = 2.0 * Points[10] - Side1.R[2, 1];
				Points[12] = 2.0 * Points[17] - Side2.R[0, 1];

				Points[3] = Qs;
				Points[2] = 2.0 / 3.0 * Side1.Q + 1.0 / 3.0 * Qs;
				Points[9] = 2.0 / 3.0 * Side2.Q + 1.0 / 3.0 * Qs;
			}

			public void Draw(Renderer renderer, Maths.Vector4[] hu, Maths.Vector4[] hv, int colorCode)
			{
				int uSegments = hu.Length - 1;
				int vSegments = hv.Length - 1;

				Maths.Matrix4x4 GGHx = new Maths.Matrix4x4();
				Maths.Matrix4x4 GGHy = new Maths.Matrix4x4();
				Maths.Matrix4x4 GGHz = new Maths.Matrix4x4();

				// P
				GGHx.v11 = Points[3].x; GGHy.v11 = Points[3].y; GGHz.v11 = Points[3].z;
				GGHx.v22 = Points[16].x; GGHy.v22 = Points[16].y; GGHz.v22 = Points[16].z;
				GGHx.v21 = Points[19].x; GGHy.v21 = Points[19].y; GGHz.v21 = Points[19].z;
				GGHx.v12 = Points[0].x; GGHy.v12 = Points[0].y; GGHz.v12 = Points[0].z;

				// Vu
				var vu00 = 3.0 * (Points[9] - Points[3]);
				GGHx.v31 = vu00.x; GGHy.v31 = vu00.y; GGHz.v31 = vu00.z;
				var vu11 = 3.0 * (Points[16] - Points[10]);
				GGHx.v42 = vu11.x; GGHy.v42 = vu11.y; GGHz.v42 = vu11.z;
				var vu10 = 3.0 * (Points[19] - Points[15]);
				GGHx.v41 = vu10.x; GGHy.v41 = vu10.y; GGHz.v41 = vu10.z;
				var vu01 = 3.0 * (Points[4] - Points[0]);
				GGHx.v32 = vu01.x; GGHy.v32 = vu01.y; GGHz.v32 = vu01.z;

				// Vv
				var vv00 = 3.0 * (Points[2] - Points[3]);
				GGHx.v13 = vv00.x; GGHy.v13 = vv00.y; GGHz.v13 = vv00.z;
				var vv11 = 3.0 * (Points[16] - Points[17]);
				GGHx.v24 = vv11.x; GGHy.v24 = vv11.y; GGHz.v24 = vv11.z;
				var vv10 = 3.0 * (Points[18] - Points[19]);
				GGHx.v23 = vv10.x; GGHy.v23 = vv10.y; GGHz.v23 = vv10.z;
				var vv01 = 3.0 * (Points[0] - Points[1]);
				GGHx.v14 = vv01.x; GGHy.v14 = vv01.y; GGHz.v14 = vv01.z;

				// Vab
				var vuv00 = 3.0 * (Points[8] - Points[3]);
				var vvu00 = 3.0 * (Points[7] - Points[3]);

				var vuv11 = 3.0 * (Points[16] - Points[11]);
				var vvu11 = 3.0 * (Points[16] - Points[12]);

				var vuv10 = 3.0 * (Points[19] - Points[14]);
				var vvu10 = 3.0 * (Points[19] - Points[13]);

				var vuv01 = 3.0 * (Points[5] - Points[0]);
				var vvu01 = 3.0 * (Points[6] - Points[0]);

				Func<int, int, Maths.Vector3> pathPoint = (int uSeg, int vSeg) => {

					if (uSeg == 0 && vSeg == 0)
						return Points[3];
					if (uSeg == uSegments && vSeg == vSegments)
						return Points[16];
					if (uSeg == uSegments && vSeg == 0)
						return Points[19];
					if (uSeg == 0 && vSeg == vSegments)
						return Points[0];

					var u = uSeg / (double)uSegments;
					var v = vSeg / (double)vSegments;

					var vab00 = (u * vuv00 + v * vvu00) / (u + v);
					GGHx.v33 = vab00.x; GGHy.v33 = vab00.y; GGHz.v33 = vab00.z;
					var vab11 = ((1.0 - u) * vuv11 + (1.0 - v) * vvu11) / ((1.0 - u) + (1.0 - v));
					GGHx.v44 = vab11.x; GGHy.v44 = vab11.y; GGHz.v44 = vab11.z;
					var vav10 = ((1.0 - u) * vuv10 + v * vvu10) / ((1.0 - u) + v);
					GGHx.v43 = vav10.x; GGHy.v43 = vav10.y; GGHz.v43 = vav10.z;
					var vav01 = (u * vuv01 + (1.0 - v) * vvu01) / (u + (1.0 - v));
					GGHx.v34 = vav01.x; GGHy.v34 = vav01.y; GGHz.v34 = vav01.z;

					return new Maths.Vector3(hu[uSeg] * GGHx * hv[vSeg], hu[uSeg] * GGHy * hv[vSeg], hu[uSeg] * GGHz * hv[vSeg]);
				};

				for (int uSeg = 0; uSeg < uSegments; ++uSeg)
				{
					for (int vSeg = 0; vSeg < vSegments; ++vSeg)
					{
						renderer.DrawSegment(pathPoint(uSeg, vSeg), pathPoint(uSeg + 1, vSeg), colorCode);
						renderer.DrawSegment(pathPoint(uSeg, vSeg), pathPoint(uSeg, vSeg + 1), colorCode);
					}
				}

				for (int vSeg = 0; vSeg < vSegments; ++vSeg)
					renderer.DrawSegment(pathPoint(uSegments, vSeg), pathPoint(uSegments, vSeg), colorCode);

				for (int uSeg = 0; uSeg < uSegments; ++uSeg)
					renderer.DrawSegment(pathPoint(uSeg, vSegments), pathPoint(uSeg + 1, vSegments), colorCode);
			}

			public void DrawFrame(Renderer renderer)
			{
				renderer.DrawSegment(Points[0], Points[4], ColorCodes.RenderFrame);
				renderer.DrawSegment(Points[4], Points[10], ColorCodes.RenderFrame);
				renderer.DrawSegment(Points[10], Points[16], ColorCodes.RenderFrame);

				renderer.DrawSegment(Points[16], Points[17], ColorCodes.RenderFrame);
				renderer.DrawSegment(Points[17], Points[18], ColorCodes.RenderFrame);
				renderer.DrawSegment(Points[18], Points[19], ColorCodes.RenderFrame);

				renderer.DrawSegment(Points[0], Points[1], ColorCodes.RenderFrame);
				renderer.DrawSegment(Points[4], Points[5], ColorCodes.RenderFrame);
				renderer.DrawSegment(Points[10], Points[11], ColorCodes.RenderFrame);

				renderer.DrawSegment(Points[17], Points[12], ColorCodes.RenderFrame);
				renderer.DrawSegment(Points[18], Points[13], ColorCodes.RenderFrame);
				renderer.DrawSegment(Points[19], Points[15], ColorCodes.RenderFrame);

				renderer.DrawSegment(Points[1], Points[6], ColorCodes.RenderFrame);
				renderer.DrawSegment(Points[15], Points[14], ColorCodes.RenderFrame);

				// renderer.DrawPoint(Side1.Q, ColorCodes.RenderFrame);

				renderer.DrawSegment(Points[1], Points[2], ColorCodes.RenderFrame);
				renderer.DrawSegment(Points[2], Points[3], ColorCodes.RenderFrame);

				renderer.DrawSegment(Points[15], Points[9], ColorCodes.RenderFrame);
				renderer.DrawSegment(Points[9], Points[3], ColorCodes.RenderFrame);

				renderer.DrawSegment(Points[2], Points[7], ColorCodes.RenderFrame);
				renderer.DrawSegment(Points[9], Points[8], ColorCodes.RenderFrame);
			}

			#endregion

		}

		private sealed class SidePointsAdaptor
		{

			#region Public Properties

			public Algorithms.Gregory.ISideDescriptor Side { get; private set; }

			public Maths.Vector3[,] R { get; private set; } = new Maths.Vector3[3, 2];

			public Maths.Vector3[,] S { get; private set; } = new Maths.Vector3[2, 2];

			public Maths.Vector3[,] T { get; private set; } = new Maths.Vector3[1, 2];

			public Maths.Vector3 Q { get; private set; } = new Maths.Vector3();

			#endregion

			#region Constructors

			public SidePointsAdaptor(Algorithms.Gregory.ISideDescriptor side)
			{
				Side = side;

				Update();
			}

			#endregion

			#region Public Methods

			public Maths.Vector3 Update()
			{
				for (int u = 0; u < 3; ++u)
					for (int v = 0; v < 2; ++v)
						R[u, v] = (Side.Points[u, v].Position + Side.Points[u + 1, v].Position) / 2.0;

				for (int u = 0; u < 2; ++u)
					for (int v = 0; v < 2; ++v)
						S[u, v] = (R[u, v] + R[u + 1, v]) / 2.0;

				for (int u = 0; u < 1; ++u)
					for (int v = 0; v < 2; ++v)
						T[u, v] = (S[u, v] + S[u + 1, v]) / 2.0;


				Q = T[0, 0] + 3.0 * (T[0, 0] - T[0, 1]) / 2.0;

				return Q;
			}

			#endregion

		}

		#endregion

		#region Private Methods

		private Maths.Vector3 UpdateSides()
		{
			var Qs = new Maths.Vector3();
			foreach (var side in sides)
				Qs += side.Update();

			return Qs / sides.Length;
		}

		private void UpdatePatches(Maths.Vector3 Qs)
		{
			foreach (var patch in patches)
				patch.Update(Qs);

			UpdateInternalDerivatives();
		}

		private void UpdateInternalDerivatives()
		{
			for (int i = 0; i < patches.Length; ++i)
			{
				var patch = patches[i];
				var ppatch = patches[i > 0 ? i - 1 : patches.Length - 1];
				var npatch = patches[i < patches.Length - 1 ? i + 1 : 0];

				var p0 = patch.Points[3];

				var g00 = patch.Points[4] - patch.Points[0];
				var g10 = patch.Points[18] - patch.Points[19];

				var g02 = ((p0 - ppatch.Points[2]) + (npatch.Points[2] - p0)) / 2.0;
				var g12 = ((p0 - npatch.Points[9]) + (ppatch.Points[9] - p0)) / 2.0;

				var g01 = (g00 + g02) / 2.0;
				var g11 = (g10 + g12) / 2.0;

				patch.Points[7] = g02 * (1.0 / 9.0) + g01 * (4.0 / 9.0) + g00 * (4.0 / 9.0) + patch.Points[2];
				patch.Points[8] = g12 * (1.0 / 9.0) + g11 * (4.0 / 9.0) + g10 * (4.0 / 9.0) + patch.Points[9];
			}
		}

		private static Maths.Vector4 Hermite(double t)
		{
			var t2 = t * t;
			var t3 = t2 * t;
			return new Maths.Vector4(2.0 * t3 - 3.0 * t2 + 1, -2.0 * t3 + 3.0 * t2, t3 - 2.0 * t2 + t, t3 - t2);
		}

		private void PointRemoved(SceneObject obj)
		{
			OnRemoved();
		}

		private void OnUSegmentsChange()
		{
			hermiteUValues = Enumerable.Range(0, uSegments + 1).Select(uSeg => Hermite(uSeg / (double)uSegments)).ToArray();
		}

		private void OnVSegmentsChange()
		{
			hermiteVValues = Enumerable.Range(0, vSegments + 1).Select(vSeg => Hermite(vSeg / (double)vSegments)).ToArray();
		}

		#endregion

		#region Private Data

		SidePointsAdaptor[] sides;

		Patch[] patches;

		private bool frameVisible = false;

		private int uSegments = 10, vSegments = 10;

		private Maths.Vector4[] hermiteUValues, hermiteVValues;

		#endregion

	}
}
