﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.CompilerServices;

namespace SaQuCAD.Infrastructure
{
	[Serializable]
	public abstract class BindableObject : INotifyPropertyChanged
	{
		#region Data

		private static readonly Dictionary<string, PropertyChangedEventArgs> eventArgCache;
		private const string ERROR_MSG = "{0} is not a public property of {1}";

		#endregion

		#region Constructors

		static BindableObject()
		{
			eventArgCache = new Dictionary<string, PropertyChangedEventArgs>();
		}

		protected BindableObject()
		{
		}

		#endregion

		#region Public Members

		public event PropertyChangedEventHandler PropertyChanged;

		public static PropertyChangedEventArgs GetPropertyChangedEventArgs(string propertyName)
		{
			if (string.IsNullOrEmpty(propertyName))
				throw new ArgumentException("propertyName cannot be null or empty");

			PropertyChangedEventArgs args;

			lock (typeof(BindableObject))
			{
				bool isCached = eventArgCache.ContainsKey(propertyName);
				if (!isCached)
					eventArgCache.Add(propertyName, new PropertyChangedEventArgs(propertyName));

				args = eventArgCache[propertyName];
			}

			return args;
		}

		#endregion

		#region Protected Members

		protected virtual void AfterPropertyChanged(string propertyName)
		{
		}

		protected void RaisePropertyChanged([CallerMemberName] string propertyName = "")
		{
			VerifyProperty(propertyName);

			PropertyChangedEventHandler handler = PropertyChanged;
			if (handler != null)
			{
				PropertyChangedEventArgs args = GetPropertyChangedEventArgs(propertyName);

				handler(this, args);
			}

			AfterPropertyChanged(propertyName);
		}

		#endregion

		#region Private Helpers

		[Conditional("DEBUG")]
		private void VerifyProperty(string propertyName)
		{
			Type type = GetType();

			PropertyInfo propInfo = type.GetProperty(propertyName);

			if (propInfo == null)
			{
				string msg = string.Format(ERROR_MSG, propertyName, type.FullName);

				Debug.Fail(msg);
			}
		}

		#endregion
	}
}
