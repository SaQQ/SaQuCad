﻿using SaQuCAD.Maths;
using SaQuCAD.Shapes.Properties;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SaQuCAD.Shapes
{
    public class Cursor : SceneObject, ITranslatable
    {
        private Vector3 pos;
        private double size = 1.0;

        public Cursor(double x = 0.0, double y = 0.0, double z = 0.0)
        {
            Name = "3D Cursor";
            pos = new Vector3(x, y, z);
        }

        public Cursor(Vector3 pos)
        {
            Name = "3D Cursor";
            this.pos = pos;
        }

        public double X { get { return pos.x; } set { pos.x = value; RaisePropertyChanged(); RaisePropertyChanged(nameof(Position)); OnChanged(); } }
        public double Y { get { return pos.y; } set { pos.y = value; RaisePropertyChanged(); RaisePropertyChanged(nameof(Position)); OnChanged(); } }
        public double Z { get { return pos.z; } set { pos.z = value; RaisePropertyChanged(); RaisePropertyChanged(nameof(Position)); OnChanged(); } }

		public Vector3 Position { get { return pos; } set { pos = value; RaisePropertyChanged(); RaisePropertyChanged(nameof(X)); RaisePropertyChanged(nameof(Y)); RaisePropertyChanged(nameof(Z)); OnChanged(); } }

		private bool xex = false, yex = false, zex = false;

		public bool XExtend { get { return xex; } set { xex = value; RaisePropertyChanged(); OnChanged(); } }
		public bool YExtend { get { return yex; } set { yex = value; RaisePropertyChanged(); OnChanged(); } }
		public bool ZExtend { get { return zex; } set { zex = value; RaisePropertyChanged(); OnChanged(); } }

		public void ResetExtensions()
		{
			XExtend = YExtend = ZExtend = false;
		}

		#region TotallyUselessProperties

		private double screenX, screenY;
		public double ScreenX { get { return screenX; } set { screenX = value; RaisePropertyChanged(); } }
		public double ScreenY { get { return screenY; } set { screenY = value; RaisePropertyChanged(); } }

		#endregion

		public override void Render(Renderer renderer)
        {
			if (!Visible)
				return;

			if (xex)
				renderer.DrawSegment(pos - new Vector3(1000.0, 0.0, 0.0), pos + new Vector3(1000.0, 0.0, 0.0), ColorCodes.RenderRed);
			else
				renderer.DrawSegment(pos, pos + new Vector3(size, 0.0, 0.0), ColorCodes.RenderRed);
			if(yex)
				renderer.DrawSegment(pos - new Vector3(0.0, 1000.0, 0.0), pos + new Vector3(0.0, 1000.0, 0.0), ColorCodes.RenderGreen);
			else
				renderer.DrawSegment(pos, pos + new Vector3(0.0, size, 0.0), ColorCodes.RenderGreen);
			if(zex)
				renderer.DrawSegment(pos - new Vector3(0.0, 0.0, 1000.0), pos + new Vector3(0.0, 0.0, 1000.0), ColorCodes.RenderBlue);
			else
				renderer.DrawSegment(pos, pos + new Vector3(0.0, 0.0, size), ColorCodes.RenderBlue);
		}
    }
}
