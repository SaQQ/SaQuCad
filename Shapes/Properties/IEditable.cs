﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace SaQuCAD.Shapes.Properties
{
	public interface IEditable
	{
		ICommand Edit { get; }		
	}
}
