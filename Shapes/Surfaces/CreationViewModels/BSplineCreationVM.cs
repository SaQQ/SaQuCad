﻿using SaQuCAD.Maths;
using System;

namespace SaQuCAD.Shapes.Surfaces.CreationViewModels
{
	public class BSplineCreationVM : ControlpointsBaseCreationVM
	{

		#region Constructors

		public BSplineCreationVM(Scene scene) : base(scene)
		{
		}

		#endregion

		#region Protected Methods

		protected override void CreateSurface(Scene scene)
		{
			points = new Point[HeightSegments + 3, WidthSegments + 3];

			if(!Looped)
			{
				double dw = Width / (WidthSegments + 2);
				double dh = Height / (HeightSegments + 2);

				for (int i = 0; i < HeightSegments + 3; ++i)
				{
					for (int j = 0; j < WidthSegments + 3; ++j)
					{
						points[i, j] = new Point(LocalToGlobal(new Vector3(j * dw, i * dh, 0.0)));
						scene.AddObject(points[i, j]);
					}
				}
			}
			else
			{
				double dh = Height / (HeightSegments + 2);
				double da = 2.0 * Math.PI / WidthSegments;

				for(int i = 0; i < HeightSegments + 3; ++i)
				{
					for(int j = 0; j < WidthSegments; ++j)
					{
						points[i, j] = new Point(LocalToGlobal(new Vector3(Width * Math.Cos(j * da), Width * Math.Sin(j * da), i * dh)));
						scene.AddObject(points[i, j]);
					}

					points[i, WidthSegments] = points[i, 0];
					points[i, WidthSegments + 1] = points[i, 1];
					points[i, WidthSegments + 2] = points[i, 2];
				}

			}

			surface = new BSpline(points);
			scene.AddObject(surface);
		}

		protected override void RemoveSurface()
		{
			if (surface != null)
				surface.Remove.Execute(null);

			if (points != null)
				foreach (var point in points)
					point.Remove.Execute(null);
		}

		protected override void RepositionPoints()
		{
			if (!Looped)
			{
				double dw = Width / (WidthSegments + 2);
				double dh = Height / (HeightSegments + 2);

				for (int i = 0; i < HeightSegments + 3; ++i)
					for (int j = 0; j < WidthSegments + 3; ++j)
						points[i, j].Position = LocalToGlobal(new Vector3(j * dw, i * dh, 0.0));
			}
			else
			{
				double dh = Height / (HeightSegments + 2);
				double da = 2.0 * Math.PI / WidthSegments;

				for (int i = 0; i < HeightSegments + 3; ++i)
					for (int j = 0; j < WidthSegments; ++j)
						points[i, j].Position = LocalToGlobal(new Vector3(Width * Math.Cos(j * da), Width * Math.Sin(j * da), i * dh));

			}
		}

		#endregion

		#region Private Data

		private Point[,] points;

		private BSpline surface;

		#endregion

	}
}
