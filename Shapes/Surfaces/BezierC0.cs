﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SaQuCAD.Maths;
using SaQuCAD.Shapes.Properties;

namespace SaQuCAD.Shapes.Surfaces
{
	public class BezierC0 : ControlpointsBase
	{

		#region Public Properties

		public override double UMin => 0.0;

		public override double UMax => UPatches;

		public override double VMin => 0.0;

		public override double VMax => VPatches;

		public int UPatches => (ControlPoints.GetLength(0) - 1) / 3;

		public int VPatches => (ControlPoints.GetLength(1) - 1) / 3;

        public override ParamProps UProperties => IsUCyclic(false) ? ParamProps.Cyclic : (IsUCyclic(true) ? ParamProps.Cyclic | ParamProps.Inverted : 0);

        public override ParamProps VProperties => IsVCyclic(false) ? ParamProps.Cyclic : (IsVCyclic(true) ? ParamProps.Cyclic | ParamProps.Inverted : 0);

        #endregion

        #region Constructors

        public BezierC0(Point[,] points, string name = "New Bezier C0 Surface") : base(points)
		{
			Name = name;
		}

		#endregion

		#region Public Methods

		public override Vector3 Evaluate(double u, double v)
		{
			int uPatch, vPatch;
			double pu, pv;
			DecomposeParameters(u, v, out uPatch, out vPatch, out pu, out pv);

			var bv = Algorithms.DeCasteljau.EvaluateBasis3(pu).MatrixMultiply(Algorithms.DeCasteljau.EvaluateBasis3(pv));

			Vector3 value = new Vector3();

			for (int r = 0; r < 4; ++r)
				for (int c = 0; c < 4; ++c)
					value += bv[r, c] * ControlPoints[3 * uPatch + r, 3 * vPatch + c].Position;

			return value;
		}

		public override Vector3 EvaluateUDerivative(double u, double v)
		{
			int uPatch, vPatch;
			double pu, pv;
			DecomposeParameters(u, v, out uPatch, out vPatch, out pu, out pv);

			var bv = Algorithms.DeCasteljau.EvaluateBasis3(pv);

			Vector3[] w = new Vector3[4];

			for (int iu = 0; iu < 4; ++iu)
				for (int iv = 0; iv < 4; ++iv)
					w[iu] += bv[iv] * ControlPoints[3 * uPatch + iu, 3 * vPatch + iv].Position;

			Vector3[] dw = new Vector3[3];

			for (int i = 0; i < 3; ++i)
				dw[i] = 3.0 * (w[i + 1] - w[i]);

			Algorithms.DeCasteljau.Evaluate(dw, pu);

			return dw[0];
		}

		public override Vector3 EvaluateVDerivative(double u, double v)
		{
			int uPatch, vPatch;
			double pu, pv;
			DecomposeParameters(u, v, out uPatch, out vPatch, out pu, out pv);

			var bu = Algorithms.DeCasteljau.EvaluateBasis3(pu);

			Vector3[] w = new Vector3[4];

			for (int iv = 0; iv < 4; ++iv)
				for (int iu = 0; iu < 4; ++iu)
					w[iv] += bu[iu] * ControlPoints[3 * uPatch + iu, 3 * vPatch + iv].Position;

			Vector3[] dw = new Vector3[3];

			for (int i = 0; i < 3; ++i)
				dw[i] = 3.0 * (w[i + 1] - w[i]);

			Algorithms.DeCasteljau.Evaluate(dw, pv);

			return dw[0];
		}

		public override void Render(Renderer renderer)
		{
			if (FrameVisible)
				DrawFrame(renderer);

            base.Render(renderer);
		}

		public override IEnumerable<SceneObject> Mirror(Plane plane)
		{
			var mirroredPoints = GetMirroredPoints(plane);

			foreach (Point p in mirroredPoints)
				yield return p;

            // TODO: Load cyclicness
			yield return new BezierC0(mirroredPoints, "Mirrored " + Name);
		}

		#endregion

		#region Private Helpers

		private bool IsUCyclic(bool reversed = false)
		{
			var U = ControlPoints.GetLength(0);
			var V = ControlPoints.GetLength(1);
			for (int v = 0; v < V; ++v)
				if (ControlPoints[0, v] != ControlPoints[U - 1, reversed ? V - 1 - v : v])
					return false;
			return true;
		}

		private bool IsVCyclic(bool reversed = false)
		{
			var U = ControlPoints.GetLength(0);
			var V = ControlPoints.GetLength(1);
			for (int u = 0; u < U; ++u)
				if (ControlPoints[u, 0] != ControlPoints[reversed ? U - 1 - u : u, V - 1])
					return false;
			return true;
		}

		private void DecomposeParameters(double u, double v, out int uPatch, out int vPatch, out double pu, out double pv)
		{
            var uv = new Vector2(u, v);
            Algorithms.Wrapping.CraftWrapFunc(this)(ref uv);
            u = uv.x;
            v = uv.y;
			uPatch = (int)u == UPatches ? (int)u - 1 : (int)u;
			vPatch = (int)v == VPatches ? (int)v - 1 : (int)v;
			pu = u - uPatch;
			pv = v - vPatch;
		}

		#endregion

	}
}
