﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SaQuCAD.Shapes
{
	public interface IPointBased
	{

		int SwapPoint(Point existing, Point replacement);

	}
}
