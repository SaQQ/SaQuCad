﻿using SaQuCAD.Maths;

namespace SaQuCAD.Algorithms
{
	public static class PowerBasis
	{

		#region Public Methods

		public static Vector4 EvaluateBasis3(double t)
		{
			Vector4 B = new Vector4();

			B.x = 1.0;
            B.y = t;
            B.z = B.y * t;
            B.w = B.z * t;

			return B;
		}

		#endregion

	}
}
