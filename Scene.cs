﻿using SaQuCAD.Shapes;
using System.Collections.ObjectModel;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Linq;
using SaQuCAD.Infrastructure;
using System.Windows;
using Microsoft.Win32;
using System;
using SaQuCAD.Shapes.Surfaces.CreationViewModels;
using SaQuCAD.Controls;
using System.Collections.Generic;
using System.ComponentModel;
using System.Windows.Data;

namespace SaQuCAD
{
    public delegate void ChangedEventHandler();
    public delegate void SceneChangedEventHandler(bool recreateRenderer);

    public partial class Scene : BindableObject
    {

        #region Public Properties

        public Shapes.Cursor D3Cursor { get; private set; } = new Shapes.Cursor();

        public ObservableCollection<SceneObject> SceneObjects { get; private set; } = new ObservableCollection<SceneObject>();

        public ObservableCollection<SceneObject> SelectedObjects { get; private set; } = new ObservableCollection<SceneObject>();

        public SceneObject SelectedObject { get; set; } = null;

        public List<Type> SceneObjectTypes { get; private set; } = new List<Type>() {
            typeof(SceneObject),
            typeof(Shapes.Point),
            typeof(Shapes.Curves.BezierC0),
            typeof(Shapes.Curves.BSpline),
            typeof(Shapes.Curves.InterpolatingBSpline),
            typeof(Shapes.Curves.Polyline),
            typeof(Shapes.Surfaces.BezierC0),
            typeof(Shapes.Surfaces.BSpline),
            typeof(Shapes.Surfaces.Gregory)
        };

        public Type SceneObjectFilter { get { return sceneObjectFilter; } set { sceneObjectFilter = value; FilteredSceneObjects.Refresh(); RaisePropertyChanged(); } }

        private CollectionViewSource SceneObjectViewSource = new CollectionViewSource();

        public ICollectionView FilteredSceneObjects => SceneObjectViewSource.View;

        public Renderer Renderer { get; set; }

        public Camera Camera { get; private set; } = new Camera(0.0, 0.0, 0.0);

        public double IntersectionStep { get { return intersectionStep; } set { intersectionStep = value; RaisePropertyChanged(); } }

        #region File Commands

        public ICommand New { get; private set; }
        public ICommand Open { get; private set; }
        public ICommand Save { get; private set; }
        public ICommand SaveAs { get; private set; }
        public ICommand Exit { get; private set; }

        #endregion

        #region View Commands

        public ICommand ToggleStereoscopy { get; private set; }

        public ICommand ResetCamera { get; private set; }

        public ICommand FrontView { get; private set; }
        public ICommand TopView { get; private set; }
        public ICommand RightView { get; private set; }

        public ICommand ToggleRenderMode { get; private set; }

        #endregion

        #region Insert Commands

        public ICommand AddTorus { get; private set; }
        public ICommand AddMoebius { get; private set; }
        public ICommand AddPoint { get; private set; }
		public ICommand AddSegment { get; private set; }
		public ICommand CreateBezierC0 { get; private set; }
        public ICommand CreateBSpline { get; private set; }
        public ICommand CreateInterpolatingBSpline { get; private set; }
        public ICommand CreateBezierC0Surface { get; private set; }
        public ICommand CreateBSplineSurface { get; private set; }

        #endregion

        #region Modify Commands

        public ICommand Remove { get; private set; }

        public ICommand MergePoints { get; private set; }

        public ICommand MirrorObjects { get; private set; }

        public ICommand FillGregory { get; private set; }

        public ICommand Intersect { get; private set; }

        public ICommand SelfIntersect { get; private set; }

        #endregion

        #region Convert Commands

        public ICommand ConvertToBezierC0Curve { get; private set; }

        public ICommand ConvertToBSplineCurve { get; private set; }

        public ICommand ConvertToInterpolatingBSplineCurve { get; private set; }

        public ICommand ConvertToPolyline { get; private set; }

        #endregion

        public double EyeSeparation { get { return eyeSeparation; } set { eyeSeparation = value; RaisePropertyChanged(); OnChanged(true); } }

        public double FieldOfView { get { return fieldOfView; } set { fieldOfView = value; RaisePropertyChanged(); OnChanged(true); } }

        public double Convergence { get { return convergence; } set { convergence = value; RaisePropertyChanged(); OnChanged(true); } }

        public bool Stereocopy { get { return stereoscopy; } private set { stereoscopy = value; RaisePropertyChanged(); OnChanged(true); } }

        public string OpenDocumentPath { get { return openDocumentPath; } private set { openDocumentPath = value; RaisePropertyChanged(); } }

        public event SceneChangedEventHandler Changed;

        #endregion

        #region Constructors

        public Scene()
        {

            Camera.Changed += () => OnChanged(false);
            D3Cursor.Changed += () => OnChanged(false);

            SceneObjectViewSource.Source = SceneObjects;
            SceneObjectViewSource.Filter += (o, a) =>
            {
                a.Accepted = sceneObjectFilter == null || a.Item.GetType().IsSubclassOf(sceneObjectFilter) || a.Item.GetType() == sceneObjectFilter;
            };

            #region File Commands

            New = new RelayCommand(() => { OpenDocumentPath = null; Clear(); });
            Open = new RelayCommand(() =>
            {
                OpenFileDialog dialog = new OpenFileDialog();

                dialog.DefaultExt = ".xml";
                dialog.Filter = "XML files (*.xml)|*.xml";

                if (dialog.ShowDialog() == true)
                {
                    LoadScene(dialog.FileName);
                    OpenDocumentPath = dialog.FileName;
                }

            });
            Save = new RelayCommand(() =>
            {
                if (OpenDocumentPath == null)
                    SaveAs.Execute(null);
                else
                    SaveScene(OpenDocumentPath);
            });
            SaveAs = new RelayCommand(() =>
            {
                SaveFileDialog dialog = new SaveFileDialog();

                dialog.DefaultExt = ".xml";
                dialog.Filter = "XML files (*.xml)|*.xml";

                if (dialog.ShowDialog() == true)
                {
                    SaveScene(dialog.FileName);
                    OpenDocumentPath = dialog.FileName;
                }
            });
            Exit = new RelayCommand(() => { Application.Current.Shutdown(); });

            #endregion

            #region View Commands

            ToggleStereoscopy = new RelayCommand(() => { Stereocopy = !Stereocopy; }) { ToolTip = "Toggle stereoscopic rendering" };

            ResetCamera = new RelayCommand(() =>
            {
                Camera.Position = new Maths.Vector3(0.0, 0.0, 0.0); Camera.Pitch = 0.0; Camera.Yaw = -90.0;
            })
            { ToolTip = "Set camera position to origin and reset rotation angles" };

            FrontView = new RelayCommand(() => { Camera.Pitch = 0.0; Camera.Yaw = -90.0; })
            { ToolTip = "Set rotation angles to look from the front" };

            TopView = new RelayCommand(() => { Camera.Pitch = -90.0; Camera.Yaw = -90.0; })
            { ToolTip = "Set rotation angles to look from the top" };

            RightView = new RelayCommand(() => { Camera.Pitch = 0.0; Camera.Yaw = 180.0; })
            { ToolTip = "Set rotation angles to look from the right" };

            ToggleRenderMode = new RelayCommand(() => { if (ColorCodes.RenderMode == ColorCodes.Mode.Dark) ColorCodes.RenderMode = ColorCodes.Mode.Light; else ColorCodes.RenderMode = ColorCodes.Mode.Dark; })
            { ToolTip = "Switch between light and dark rendering theme" };

            #endregion

            #region Insert Commands

            AddTorus = new RelayCommand(() => { AddObject(new Shapes.Surfaces.Torus(1.0, 0.2)); }) { ToolTip = "Add Torus" };
            AddMoebius = new RelayCommand(() => { AddObject(new Shapes.Surfaces.Moebius(1.0, 0.2)); }) { ToolTip = "Add Moebius" };
            AddPoint = new RelayCommand(() => { AddObject(new Shapes.Point(D3Cursor.Position)); }) { ToolTip = "Add Point" };
			AddSegment = new RelayCommand(() => {
				var points = SelectedObjects.Where(obj => obj is Shapes.Point).Select(obj => obj as Shapes.Point).ToArray();
				AddObject(new Shapes.Curves.Segment(points[0], points[1]));
			}, 
			() =>
			{
				return SelectedObjects.Where(obj => obj is Shapes.Point).Select(obj => obj as Shapes.Point).Count() == 2;
			})
			{ ToolTip = "Add Segment" };
			CreateBezierC0 = new RelayCommand(() =>
            {
                var bezier = new Shapes.Curves.BezierC0(SelectedObjects.Where(obj => obj is Shapes.Point).Select(obj => obj as Shapes.Point).ToArray());

                bezier.EditRequested += (Shapes.Curves.ControlpointsBase curve) =>
                {
                    EnterVisualState(new VisualStates.BeizerC0CurveEditVisualState(this, curve as Shapes.Curves.BezierC0));
                };

                AddObject(bezier);
            })
            { ToolTip = "Create C0 Bezier Curve" };
            CreateBSpline = new RelayCommand(() =>
            {
                var bspline = new Shapes.Curves.BSpline(SelectedObjects.Where(obj => obj is Shapes.Point).Select(obj => obj as Shapes.Point).ToArray());

                bspline.EditRequested += (Shapes.Curves.ControlpointsBase curve) =>
                {
                    EnterVisualState(new VisualStates.BSplineCurveEditVisualState(this, curve as Shapes.Curves.BSpline));
                };

                AddObject(bspline);
            })
            { ToolTip = "Create C2 B-Spline Curve" };
            CreateInterpolatingBSpline = new RelayCommand(() =>
            {
                var bspline = new Shapes.Curves.InterpolatingBSpline(SelectedObjects.Where(obj => obj is Shapes.Point).Select(obj => obj as Shapes.Point).ToArray());

                bspline.EditRequested += (Shapes.Curves.ControlpointsBase curve) =>
                {
                    EnterVisualState(new VisualStates.InterpolatingBSplineCurveEditVisualState(this, curve as Shapes.Curves.InterpolatingBSpline));
                };

                AddObject(bspline);
            })
            { ToolTip = "Create Interpolating B-Spline Curve" };
            CreateBezierC0Surface = new RelayCommand(() =>
            {
                new ControlpointsSurfaceCreationWindow(new BezierC0CreationVM(this)).Show();
            })
            { ToolTip = "Create C0 Bezier Surface" };
            CreateBSplineSurface = new RelayCommand(() =>
            {
                new ControlpointsSurfaceCreationWindow(new BSplineCreationVM(this)).Show();
            })
            { ToolTip = "Create B-Spline Surface" };

            #endregion

            #region Modify Commands

            Remove = new RelayCommand(() =>
            {
                foreach (var obj in SelectedObjects.ToList())
                    obj.Remove.Execute(obj);
            },
            () =>
            {
                return SelectedObjects.Count > 0;
            })
            { ToolTip = "Remove selected scene objects" };

            MergePoints = new RelayCommand(() =>
            {
                var pointsToMerge = SelectedObjects.OfType<Shapes.Point>();

                Maths.Vector3 average = new Maths.Vector3();

                foreach (var p in pointsToMerge)
                    average += p.Position;

                average /= pointsToMerge.Count();

                var masterPoint = pointsToMerge.First();
                masterPoint.Position = average;
                var mergedPoints = pointsToMerge.Skip(1).ToArray();

                foreach (var obj in SceneObjects.OfType<IPointBased>())
                    foreach (var point in mergedPoints)
                        obj.SwapPoint(point, masterPoint);

                foreach (var point in mergedPoints)
                    point.Remove.Execute(null);
            },
            () =>
            {
                return SelectedObjects.OfType<Shapes.Point>().Count() >= 2;
            })
            { ToolTip = "Merge selected points into one" };

            MirrorObjects = new RelayCommand(() =>
            {
                new MirrorObjectsWindow(new Tools.MirrorObjectsVM(this, SelectedObjects)).Show();
            },
            () =>
            {
                return SelectedObjects.Count > 0;
            })
            { ToolTip = "Mirror selected entities through plane" };

            FillGregory = new RelayCommand(() =>
            {
                AddObject(new Shapes.Surfaces.Gregory(Algorithms.Gregory.FindHole(SelectedObjects.OfType<Shapes.Surfaces.BezierC0>())));
            },
            () =>
            {
                return Algorithms.Gregory.FindHole(SelectedObjects.OfType<Shapes.Surfaces.BezierC0>()) != null;
            })
            { ToolTip = "Fill holes in Bezier surfaces with Gregory Patches" };

            Action<Shapes.Properties.IUVParametrized, Algorithms.Intersections.IntersectionPoint> pointsToPolyline = (Shapes.Properties.IUVParametrized surf, Algorithms.Intersections.IntersectionPoint start) =>
            {
                List<Shapes.Point> points = new List<Shapes.Point>();
                List<Shapes.Point> revpoints = new List<Shapes.Point>();

                points.Add(new Shapes.Point(surf.Evaluate(start.UV.x, start.UV.y)));

                var curr = start.Next;

                while (curr != null & curr != start)
                {
                    points.Add(new Shapes.Point(surf.Evaluate(curr.UV.x, curr.UV.y)));
                    curr = curr.Next;
                }

                if (curr == null)
                {
                    curr = start.Previous;

                    while (curr != null & curr != start)
                    {
                        revpoints.Add(new Shapes.Point(surf.Evaluate(curr.UV.x, curr.UV.y)));
                        curr = curr.Previous;
                    }

                }
                else
                {
                    points.Add(points.First());
                }

                revpoints.Reverse();

                points = revpoints.Concat(points).ToList();

                foreach (var point in points)
                    AddObject(point);

                AddObject(new Shapes.Curves.Polyline(points.ToArray(), "Intersection Polyline"));
            };

            Intersect = new RelayCommand(() =>
            {
                var surfaces = SelectedObjects.OfType<Shapes.Properties.IUVParametrized>().ToArray();
				var curves = SelectedObjects.OfType<Shapes.Properties.IUParametrized>().ToArray();

				try
                {
					if(surfaces.Count() == 2)
					{
						var result = Algorithms.Intersections.Intersect(surfaces[0], surfaces[1], D3Cursor.Position, (Maths.Vector4 arg) => { }, intersectionStep);

						result.Item1.ShowUV = new RelayCommand(() =>
						{
							new ParametrizationViewer(result.Item1.Image).Show();
						});

						result.Item2.ShowUV = new RelayCommand(() =>
						{
							new ParametrizationViewer(result.Item2.Image).Show();
						});

						pointsToPolyline(surfaces[0], result.Item3);
						pointsToPolyline(surfaces[1], result.Item4);

						surfaces[0].Trimmers.Add(result.Item1);
						surfaces[1].Trimmers.Add(result.Item2);
					}
					else
					{
						var coords = Algorithms.Intersections.FindIntersection(surfaces[0], curves[0], D3Cursor.Position);
						AddObject(new Shapes.Point(surfaces[0].Evaluate(u: coords.x, v: coords.y), "Intersection point"));
					}

                    
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message, "Could not find intersection", MessageBoxButton.OK, MessageBoxImage.Error);
                }
            },
            () =>
            {
				var uvCount = SelectedObjects.OfType<Shapes.Properties.IUVParametrized>().Count();
				var uCount = SelectedObjects.OfType<Shapes.Properties.IUParametrized>().Count();

				return uvCount == 2 || (uvCount == 1 && uCount == 1);
            })
            { ToolTip = "Intersect two parametric surfaces" };

            SelfIntersect = new RelayCommand(() =>
            {
                var surface = SelectedObjects.OfType<Shapes.Properties.IUVParametrized>().First();

                const int tries = 100;

                var rand = new Random();

                for (int i = 0; i < tries; ++i)
                {
                    var Usplit = (rand.NextDouble() * 0.9 + 0.05) * (surface.UMax - surface.UMin) + surface.UMin;
                    var Vsplit = (rand.NextDouble() * 0.9 + 0.05) * (surface.VMax - surface.VMin) + surface.VMin;

                    var Vadaptor1 = new UVRegionAdaptor(surface, surface.UMin, surface.UMax, surface.VMin, Vsplit);
                    var Vadaptor2 = new UVRegionAdaptor(surface, surface.UMin, surface.UMax, Vsplit, surface.VMax);

                    var Uadaptor1 = new UVRegionAdaptor(surface, surface.UMin, Usplit, surface.VMin, surface.VMax);
                    var Uadaptor2 = new UVRegionAdaptor(surface, Usplit, surface.UMax, surface.VMin, surface.VMax);

                    try
                    {
                        var result = Algorithms.Intersections.Intersect(Uadaptor1, Uadaptor2, D3Cursor.Position, (Maths.Vector4 arg) => { }, intersectionStep);

                        result.Item1.ShowUV = new RelayCommand(() =>
                        {
                            new ParametrizationViewer(result.Item1.Image).Show();
                        });

                        result.Item2.ShowUV = new RelayCommand(() =>
                        {
                            new ParametrizationViewer(result.Item2.Image).Show();
                        });

                        pointsToPolyline(surface, result.Item3);
                        pointsToPolyline(surface, result.Item4);

                        surface.Trimmers.Add(result.Item1);
                        surface.Trimmers.Add(result.Item2);

                        return;
                    }
                    catch (Exception)
                    {

                    }

                    try
                    {
                        var result = Algorithms.Intersections.Intersect(Vadaptor1, Vadaptor2, D3Cursor.Position, (Maths.Vector4 arg) => { }, intersectionStep);

                        result.Item1.ShowUV = new RelayCommand(() =>
                        {
                            new ParametrizationViewer(result.Item1.Image).Show();
                        });

                        result.Item2.ShowUV = new RelayCommand(() =>
                        {
                            new ParametrizationViewer(result.Item2.Image).Show();
                        });

                        pointsToPolyline(surface, result.Item3);
                        pointsToPolyline(surface, result.Item4);

                        surface.Trimmers.Add(result.Item1);
                        surface.Trimmers.Add(result.Item2);

                        return;
                    }
                    catch (Exception)
                    {
                        
                    }

                }

                MessageBox.Show("Failed to corretly split surface", "Could not find intersection", MessageBoxButton.OK, MessageBoxImage.Error);
            },
            () =>
            {
                return SelectedObjects.OfType<Shapes.Properties.IUVParametrized>().Count() == 1;
            })
            { ToolTip = "Self interserct single parametric surface" };

            #endregion

            #region Convert Commands

            ConvertToBezierC0Curve = new RelayCommand(() =>
            {
                var curve = SelectedObjects.OfType<Shapes.Curves.ControlpointsBase>().First();

                AddObject(new Shapes.Curves.BezierC0(curve.ControlPoints.ToArray(), curve.Name + " [conv]"));

                curve.Remove.Execute(curve);
            },
            () =>
            {
                return SelectedObjects.OfType<Shapes.Curves.ControlpointsBase>().Count() == 1;
            })
            { ToolTip = "Convert curve to Bezier C0 Curve" };

            ConvertToBSplineCurve = new RelayCommand(() =>
            {
                var curve = SelectedObjects.OfType<Shapes.Curves.ControlpointsBase>().First();

                AddObject(new Shapes.Curves.BSpline(curve.ControlPoints.ToArray(), curve.Name + " [conv]"));

                curve.Remove.Execute(curve);
            },
            () =>
            {
                return SelectedObjects.OfType<Shapes.Curves.ControlpointsBase>().Count() == 1;
            })
            { ToolTip = "Convert curve to B-Spline Curve" };

            ConvertToInterpolatingBSplineCurve = new RelayCommand(() =>
            {
                var curve = SelectedObjects.OfType<Shapes.Curves.ControlpointsBase>().First();

                AddObject(new Shapes.Curves.InterpolatingBSpline(curve.ControlPoints.ToArray(), curve.Name + " [conv]"));

                curve.Remove.Execute(curve);
            },
            () =>
            {
                return SelectedObjects.OfType<Shapes.Curves.ControlpointsBase>().Count() == 1;
            })
            { ToolTip = "Convert curve to Interpolating B-Spline Curve" };

            ConvertToPolyline = new RelayCommand(() =>
            {
                var curve = SelectedObjects.OfType<Shapes.Curves.ControlpointsBase>().First();

                AddObject(new Shapes.Curves.Polyline(curve.ControlPoints.ToArray(), curve.Name + " [conv]"));

                curve.Remove.Execute(curve);
            },
            () =>
            {
                return SelectedObjects.OfType<Shapes.Curves.ControlpointsBase>().Count() == 1;
            })
            { ToolTip = "Convert curve to Polyline" };

            #endregion

            SetDefaultVisualState();

        }

        #endregion

        #region Public Methods

        public void Render(WriteableBitmap raster)
        {
            Renderer.ViewMatrix = Camera.ViewMatrix;
            Renderer.InvertedViewMatrix = Camera.InvertedViewMatrix;

            var cursorScreenPosition = Renderer.PrerenderPoint(D3Cursor.Position);
            D3Cursor.ScreenX = cursorScreenPosition == null ? 0.0 : cursorScreenPosition.Value.x;
            D3Cursor.ScreenY = cursorScreenPosition == null ? 0.0 : cursorScreenPosition.Value.y;

            using (var context = raster.GetBitmapContext())
            {
                Renderer.SetContext(context);

                raster.Clear(CustomWritableBitmapExtensions.ConvertColor(ColorCodes.RenderBackground));

                foreach (var rect in rectangles)
                    rect.Render(Renderer);

                foreach (SceneObject mesh in SceneObjects.Where(obj => obj.Visible))
                    mesh.Render(Renderer);

                D3Cursor.Render(Renderer);

                visualState.Render(Renderer);
            }
        }

        #endregion

        #region Private Helpers

        private void Clear()
        {
            SetDefaultVisualState();

            D3Cursor.Position = new Maths.Vector3(0.0, 0.0, 0.0);

            SelectedObjects.Clear();

            foreach (var obj in SceneObjects)
            {
                obj.Changed -= SceneObjectChanged;
                obj.Removed -= SceneObjectRemoved;
            }

            SceneObjects.Clear();

            OnChanged(false);
        }

        private void SetDefaultVisualState()
        {
            VisualState = new VisualStates.DefaultVisualState(this);
        }

        private void EnterVisualState(VisualStates.VisualState newState)
        {
            if (VisualState is VisualStates.DefaultVisualState)
            {
                VisualState = newState;
                VisualState.OnEnter();
                VisualState.ExitRequest += () =>
                {
                    VisualState.OnExit();
                    SetDefaultVisualState();
                };
            }
        }

        private void OnChanged(bool recreateRenderer)
        {
            Changed?.Invoke(recreateRenderer);
        }

        public void AddObject(SceneObject obj)
        {
            obj.Changed += SceneObjectChanged;
            SceneObjects.Add(obj);
            obj.Removed += SceneObjectRemoved;
            OnChanged(false);
        }

        private void SceneObjectRemoved(SceneObject obj)
        {
            obj.Changed -= SceneObjectChanged;
            SceneObjects.Remove(obj);
            obj.Removed -= SceneObjectRemoved;
            OnChanged(false);
        }

        private void SceneObjectChanged()
        {
            OnChanged(false);
        }

        #endregion

        #region Private Data

        private VisualStates.VisualState visualState;

        public VisualStates.VisualState VisualState { get { return visualState; } private set { visualState = value; RaisePropertyChanged(); } }

        private double eyeSeparation = 0.02;
        private double fieldOfView = 45.0;
        private double convergence = 5.0;
        private bool stereoscopy = false;

        private double intersectionStep = 0.04;

        private string openDocumentPath = null;

        private GM1.Serialization.XMLSerializer xmlSerializer = new GM1.Serialization.XMLSerializer();

        private Shapes.Surfaces.Rectangle[] rectangles = new Shapes.Surfaces.Rectangle[]
        {
            new Shapes.Surfaces.Rectangle()
            {
                Plane = Shapes.Plane.XZ,
                Width = 15.0,
                Height = 15.0
            },
            new Shapes.Surfaces.Rectangle()
            {
                Plane = Shapes.Plane.XY,
                Width = 15.0,
                Height = 6.0
            },
            new Shapes.Surfaces.Rectangle()
            {
                Plane = Shapes.Plane.YZ,
                Width = 6.0,
                Height = 15.0
            }
        };

        private Type sceneObjectFilter = typeof(SceneObject);

        #endregion

    }
}
