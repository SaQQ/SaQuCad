﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SaQuCAD.Shapes.Properties
{
	interface ITorus
	{

		double R { get; set; }

		double r { get; set; }

	}
}
