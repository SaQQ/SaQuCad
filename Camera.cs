﻿using MathNet.Numerics.LinearAlgebra;
using System.Windows.Input;
using System.Windows;
using SaQuCAD.Infrastructure;
using SaQuCAD.Maths;
using System;

namespace SaQuCAD
{
	public sealed class Camera : BindableObject
    {

		#region Private Data

		private Vector3 position;
        private Vector3 front;
        private Vector3 up;
        private Vector3 right;
        private Vector3 worldUp;

        private double pitch = 0.0;
        private double yaw = -90.0;

        private const double movementSpeed = 6.0;
        private const double mouseSensitivity = 0.25;

		private const double minDistance = 0.1;
		private const double maxDistance = 25.0;

        private double distance = 10.0;

		private const double eyeSpan = 0.1;

        private static Point lastPosition;

		#endregion

		#region Public Properties

		public Vector3 Position { get { return position; } set { position = value; RaisePropertyChanged(nameof(X)); RaisePropertyChanged(nameof(Y)); RaisePropertyChanged(nameof(Z)); updateViewMatrices(); OnChanged(); } }

		public double X { get { return position.x; } set { position.x = value; updateViewMatrices(); RaisePropertyChanged(); RaisePropertyChanged(nameof(Position)); OnChanged(); } }

		public double Y { get { return position.y; } set { position.y = value; updateViewMatrices(); RaisePropertyChanged(); RaisePropertyChanged(nameof(Position)); OnChanged(); } }

		public double Z { get { return position.z; } set { position.z = value; updateViewMatrices(); RaisePropertyChanged(); RaisePropertyChanged(nameof(Position)); OnChanged(); } }

		public double Pitch { get { return pitch; } set { pitch = value; updateCameraVectors(); updateViewMatrices(); RaisePropertyChanged(); OnChanged(); } }

		public double Yaw { get { return yaw; } set { yaw = value; updateCameraVectors(); updateViewMatrices(); RaisePropertyChanged(); OnChanged(); } }

		public double Distance { get { return distance; } set { distance = Math.Min(Math.Max(minDistance, value), maxDistance); updateViewMatrices(); RaisePropertyChanged(); OnChanged(); } }

		public Matrix4x4 ViewMatrix { get; private set; }

		public Matrix4x4 InvertedViewMatrix { get; private set; }

		public event ChangedEventHandler Changed;

		#endregion

		#region Constructors

		public Camera(double posX, double posY, double posZ, double pitch = 0.0, double yaw = -90.0)
        {
			position = new Vector3(posX, posY, posZ);
			worldUp = new Vector3(0.0, 1.0, 0.00);
			this.pitch = pitch;
			this.yaw = yaw;
          
            updateCameraVectors();
			updateViewMatrices();
        }

		#endregion

		#region Public Methods

		public void Update(double dt)
        {
            Matrix<double> translation = Matrix<double>.Build.DenseIdentity(4);

            var dx = movementSpeed * dt;

			bool positionChanged = false;
			bool rotationChanged = false;

			if (KeyState.GetKeyState(Key.W))
			{
				position += front * dx;
				positionChanged = true;
			}
			if (KeyState.GetKeyState(Key.S))
			{
				position -= front * dx;
				positionChanged = true;
			}
			if (KeyState.GetKeyState(Key.D))
			{
				position += right * dx;
				positionChanged = true;
			}
			if (KeyState.GetKeyState(Key.A))
			{
				position -= right * dx;
				positionChanged = true;
			}
			if (KeyState.GetKeyState(Key.E))
			{
				position += worldUp * dx;
				positionChanged = true;
			}
            if (KeyState.GetKeyState(Key.Q))
			{
				position -= worldUp * dx;
				positionChanged = true;
			}

			if (positionChanged)
			{
				RaisePropertyChanged(nameof(Position));
				RaisePropertyChanged(nameof(X));
				RaisePropertyChanged(nameof(Y));
				RaisePropertyChanged(nameof(Z));
			}

			Point mousePosition = KeyState.GetMousePosition();

			if (KeyState.GetMouseState(MouseButton.Right))
            {
				if(mousePosition.X != lastPosition.X || mousePosition.Y != lastPosition.Y)
				{
					yaw += (mousePosition.X - lastPosition.X) * mouseSensitivity;
					pitch += -(mousePosition.Y - lastPosition.Y) * mouseSensitivity;

					if (pitch > 89.0f)
						pitch = 89.0f;
					if (pitch < -89.0f)
						pitch = -89.0f;

					RaisePropertyChanged(nameof(Pitch));
					RaisePropertyChanged(nameof(Yaw));

					updateCameraVectors();

					rotationChanged = true;
				}                
			}

			lastPosition = mousePosition;

			if(positionChanged || rotationChanged)
			{
				OnChanged();
				updateViewMatrices();
			}

        }

		#endregion

		#region Private Helpers

		private void OnChanged()
		{
			Changed?.Invoke();
		}

		private static double degreeToRadian(double angle)
		{
			return Math.PI * angle / 180.0;
		}

		private void updateCameraVectors()
		{
			front = new Vector3();

			front.x = Math.Cos(degreeToRadian(yaw)) * Math.Cos(degreeToRadian(pitch));
			front.y = Math.Sin(degreeToRadian(pitch));
			front.z = Math.Sin(degreeToRadian(yaw)) * Math.Cos(degreeToRadian(pitch));

			front = front.Normalized();
			right = Vector3.Cross(front, worldUp).Normalized();
			up = Vector3.Cross(right, front).Normalized();
		}


		private void updateViewMatrices()
		{
			var B = Matrix4x4.Identity();

			// 1'st row
			B.v11 = right.x;
			B.v12 = right.y;
			B.v13 = right.z;

			// 2'nd row
			B.v21 = up.x;
			B.v22 = up.y;
			B.v23 = up.z;

			// 3'rd row
			B.v31 = -front.x;
			B.v32 = -front.y;
			B.v33 = -front.z;

			var T = Matrix4x4.Translation(-position.x, -position.y, -position.z);

            var DT = Matrix4x4.Translation(0.0, 0.0, -distance);

			ViewMatrix = DT * B * T;

			B = B.Transpose();

			T = Matrix4x4.Translation(position.x, position.y, position.z);

            DT = Matrix4x4.Translation(0.0, 0.0, distance);

            InvertedViewMatrix = T * B * DT;
		}

		#endregion

	}
}
