﻿using System.Linq;
using System.Windows.Input;
using SaQuCAD.Shapes;

namespace SaQuCAD.VisualStates
{
	public sealed class DefaultVisualState : VisualState
	{

		#region Public Properties

		public override string Description { get { return "Default"; } }

		#endregion

		#region Constructors

		public DefaultVisualState(Scene scene) : base(scene)
		{
		}

		#endregion

		#region Public Methods.

		public override void MouseDown(IVector2 pos)
		{
			clickPos = pos;

			if (!Keyboard.IsKeyDown(Key.LeftCtrl))
			{
				Scene.D3Cursor.ResetExtensions();
				Scene.SelectedObjects.Clear();
			}

			var selectedPoint = TryClickPoints(pos, Scene.SceneObjects.Where(obj => obj is Point).Select(obj => obj as Point));

			if (selectedPoint != null)
			{
				Scene.D3Cursor.Position = selectedPoint.Position;
				selectSceneObject(selectedPoint);
			}
			else
			{
				var inter = cursorPlaneIntersection(pos);

				if (inter != null)
				{
					Scene.D3Cursor.Position = inter.Value;
				}
			}
		}

		public override void MouseMove(IVector2 pos)
		{
			if (!clickPos.HasValue)
				return;

			clickPos = pos;

			var inter = cursorPlaneIntersection(pos);

			if (inter == null)
				return;

			var posdiff = inter.Value - Scene.D3Cursor.Position;

			if (Scene.D3Cursor.XExtend)
			{
				posdiff.y = posdiff.z = 0.0;
			}
			else if (Scene.D3Cursor.YExtend)
			{
				posdiff.x = posdiff.z = 0.0;
			}
			else if (Scene.D3Cursor.ZExtend)
			{
				posdiff.x = posdiff.y = 0.0;
			}

			foreach (var obj in Scene.SelectedObjects)
			{
				if (obj is Point)
				{
					(obj as Point).Position += posdiff;
				}
			}

			Scene.D3Cursor.Position += posdiff;
		}

		public override void MouseUp(IVector2 pt)
		{
			clickPos = null;
		}

		public override void KeyUp(Key key)
		{
			if (key == Key.T)
			{
				if (Scene.SelectedObjects.Count < 1)
					return;

				if (Scene.D3Cursor.XExtend)
				{
					Scene.D3Cursor.XExtend = false;
					Scene.D3Cursor.YExtend = true;
				}
				else if (Scene.D3Cursor.YExtend)
				{
					Scene.D3Cursor.YExtend = false;
					Scene.D3Cursor.ZExtend = true;
				}
				else if (Scene.D3Cursor.ZExtend)
				{
					Scene.D3Cursor.ZExtend = false;
				}
				else
				{
					Scene.D3Cursor.XExtend = true;
				}
			}
			else if (key == Key.M)
			{
				Scene.Camera.Position = Scene.D3Cursor.Position;
			}
		}

		public override void Render(Renderer renderer)
		{
			
		}

		#endregion

		#region Private Helpers

		private void selectSceneObject(SceneObject obj)
		{
			if (Scene.SelectedObjects.Contains(obj))
				return;
			Scene.SelectedObjects.Add(obj);
		}

		#endregion

		#region Private Data

		private IVector2? clickPos = null;

		private const double selectionTolerance = 8.0;

		#endregion

	}
}
