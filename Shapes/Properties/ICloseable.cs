﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SaQuCAD.Shapes.Properties
{
	public interface ICloseable
	{
		bool Closed { get; set; }
	}
}
