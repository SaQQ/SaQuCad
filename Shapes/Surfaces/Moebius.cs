﻿using System;
using SaQuCAD.Maths;
using SaQuCAD.Shapes.Properties;

namespace SaQuCAD.Shapes.Surfaces
{
	public class Moebius : AffineUVMesh
    {

        #region Public Properties

        // public double R { get { return _R; } set { _R = value; RecreateMesh(); } }

        // public double r { get { return _r; } set { _r = value; RecreateMesh(); } }

		public override double UMin => 0.0;

        public override double UMax => 2.0 * Math.PI;

        public override double VMin => -1.0;

        public override double VMax => 1.0;

        public override ParamProps UProperties => ParamProps.Cyclic | ParamProps.Inverted;

        public override ParamProps VProperties => 0;
	
        #endregion

        #region Constructors

        public Moebius(double R, double r, string name = "New Torus")
		{
			Name = name;
			//_r = r;
			//_R = R;
			RecreateMesh();
		}

		#endregion

		#region Public Methods

		public override Vector3 Evaluate(double u, double v)
		{
			return (ModelMatrix * new Vector4((1.0 + v / 2.0 * Math.Cos(u / 2.0)) * Math.Cos(u), (1.0 + v / 2.0 * Math.Cos(u / 2.0)) * Math.Sin(u), v / 2.0 * Math.Sin(u / 2.0), 1.0)).WithoutW();
		}

		public override Vector3 EvaluateUDerivative(double u, double v)
		{
            return (ModelMatrix * new Vector4(
                -0.25 * Math.Sin(u / 2.0) * (v * (3.0 * Math.Cos(u) + 2.0) + 8.0 * Math.Cos(u / 2.0)),
                0.125 * v * Math.Cos(u / 2.0) + 0.375 * v * Math.Cos(3.0 * u / 2.0) + Math.Cos(u),
                0.25 * v * Math.Cos(u / 2.0), 
                0.0)).WithoutW();
		}

		public override Vector3 EvaluateVDerivative(double u, double v)
		{
			return (ModelMatrix * new Vector4(0.5 * Math.Cos(u / 2.0) * Math.Cos(u), 0.5 * Math.Cos(u / 2.0) * Math.Sin(u), 0.5 * Math.Sin(u / 2.0), 0.0)).WithoutW();
		}

		#endregion

		#region Private Data

		//private double _R;
		//private double _r;

		#endregion

	}
}
