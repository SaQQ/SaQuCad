﻿using System;

namespace SaQuCAD.Maths
{
	public struct Matrix3x3
	{
		public double v11;
		public double v21;
		public double v31;

		public double v12;
		public double v22;
		public double v32;

		public double v13;
		public double v23;
		public double v33;

		public static Matrix3x3 Identity()
		{
			Matrix3x3 i = new Matrix3x3();
			i.v11 = i.v22 = i.v33 = 1.0;
			return i;
		}

		public Matrix3x3 Transpose()
		{
			Matrix3x3 tm;

			tm.v11 = v11;
			tm.v12 = v21;
			tm.v13 = v31;

			tm.v21 = v12;
			tm.v22 = v22;
			tm.v23 = v32;

			tm.v31 = v13;
			tm.v32 = v23;
			tm.v33 = v33;

			return tm;
		}

		public Matrix3x3 Inverse()
		{
			Matrix3x3 inv;

			inv.v11 = v22 * v33 - v23 * v32;
			inv.v12 = v13 * v32 - v12 * v33;
			inv.v13 = v12 * v23 - v13 * v22;

			inv.v21 = v23 * v31 - v21 * v33;
			inv.v22 = v11 * v33 - v13 * v31;
			inv.v23 = v13 * v21 - v11 * v23;

			inv.v31 = v21 * v32 - v22 * v31;
			inv.v32 = v12 * v31 - v11 * v32;
			inv.v33 = v11 * v22 - v12 * v21;

			double det =
				+v11 * v22 * v33
				+ v21 * v32 * v13
				+ v31 * v12 * v23
				- v11 * v32 * v23
				- v31 * v22 * v13
				- v21 * v12 * v33;

			if (det == 0.0)
				throw new ArithmeticException("Odd matrix");

			det = 1.0 / det;

			inv *= det;

			return inv;
		}

		public static bool operator ==(Matrix3x3 lhs, Matrix3x3 rhs)
		{
			return 
				lhs.v11 == rhs.v11 && lhs.v21 == rhs.v21 && lhs.v31 == rhs.v31 &&
				lhs.v12 == rhs.v12 && lhs.v22 == rhs.v22 && lhs.v32 == rhs.v32 &&
				lhs.v13 == rhs.v13 && lhs.v23 == rhs.v23 && lhs.v33 == rhs.v33;
		}

		public static bool operator !=(Matrix3x3 lhs, Matrix3x3 rhs)
		{
			return !(lhs == rhs);
		}

		public static Matrix3x3 operator +(Matrix3x3 lhs, Matrix3x3 rhs)
		{
			Matrix3x3 m;
			m.v11 = lhs.v11 + rhs.v11; m.v21 = lhs.v21 + rhs.v21; m.v31 = lhs.v31 + rhs.v31;
			m.v12 = lhs.v12 + rhs.v12; m.v22 = lhs.v22 + rhs.v22; m.v32 = lhs.v32 + rhs.v32;
			m.v13 = lhs.v13 + rhs.v13; m.v23 = lhs.v23 + rhs.v23; m.v33 = lhs.v33 + rhs.v33;
			return m;
		}

		public static Matrix3x3 operator -(Matrix3x3 lhs, Matrix3x3 rhs)
		{
			return lhs + -rhs;
		}

		public static Matrix3x3 operator -(Matrix3x3 m)
		{
			Matrix3x3 mr;
			mr.v11 = -m.v11; mr.v21 = -m.v21; mr.v31 = -m.v31;
			mr.v12 = -m.v12; mr.v22 = -m.v22; mr.v32 = -m.v32;
			mr.v13 = -m.v13; mr.v23 = -m.v23; mr.v33 = -m.v33;
			return mr;
		}

		public static Matrix3x3 operator *(double s, Matrix3x3 m)
		{
			Matrix3x3 mr;
			mr.v11 = s * m.v11; mr.v21 = s * m.v21; mr.v31 = s * m.v31;
			mr.v12 = s * m.v12; mr.v22 = s * m.v22; mr.v32 = s * m.v32;
			mr.v13 = s * m.v13; mr.v23 = s * m.v23; mr.v33 = s * m.v33;
			return mr;
		}

		public static Matrix3x3 operator *(Matrix3x3 m, double s)
		{
			return s * m;
		}

		public static Vector3 operator *(Matrix3x3 m, Vector3 v)
		{
			Vector3 w;
			w.x = m.v11 * v.x + m.v12 * v.y + m.v13 * v.z;
			w.y = m.v21 * v.x + m.v22 * v.y + m.v23 * v.z;
			w.z = m.v31 * v.x + m.v32 * v.y + m.v33 * v.z;
			return w;
		}

		public static Vector3 operator *(Vector3 v, Matrix3x3 m)
		{
			Vector3 w;
			w.x = m.v11 * v.x + m.v21 * v.y + m.v31 * v.z;
			w.y = m.v12 * v.x + m.v22 * v.y + m.v32 * v.z;
			w.z = m.v13 * v.x + m.v23 * v.y + m.v33 * v.z;
			return w;
		}

		public static Matrix3x3 operator *(Matrix3x3 lhs, Matrix3x3 rhs)
		{
			Matrix3x3 m;

			// 1'st row
			m.v11 = lhs.v11 * rhs.v11 + lhs.v12 * rhs.v21 + lhs.v13 * rhs.v31;
			m.v12 = lhs.v11 * rhs.v12 + lhs.v12 * rhs.v22 + lhs.v13 * rhs.v32;
			m.v13 = lhs.v11 * rhs.v13 + lhs.v12 * rhs.v23 + lhs.v13 * rhs.v33;

			// 2'nd row
			m.v21 = lhs.v21 * rhs.v11 + lhs.v22 * rhs.v21 + lhs.v23 * rhs.v31;
			m.v22 = lhs.v21 * rhs.v12 + lhs.v22 * rhs.v22 + lhs.v23 * rhs.v32;
			m.v23 = lhs.v21 * rhs.v13 + lhs.v22 * rhs.v23 + lhs.v23 * rhs.v33;

			// 3'rd row
			m.v31 = lhs.v31 * rhs.v11 + lhs.v32 * rhs.v21 + lhs.v33 * rhs.v31;
			m.v32 = lhs.v31 * rhs.v12 + lhs.v32 * rhs.v22 + lhs.v33 * rhs.v32;
			m.v33 = lhs.v31 * rhs.v13 + lhs.v32 * rhs.v23 + lhs.v33 * rhs.v33;

			return m;
		}

		public override bool Equals(object obj)
		{
			try
			{
				return this == (Matrix3x3)obj;
			}
			catch
			{
				return false;
			}
		}

		public override int GetHashCode()
		{
			return base.GetHashCode();
		}
	}

	public struct Matrix4x4
	{
		public double v11;
		public double v21;
		public double v31;
		public double v41;

		public double v12;
		public double v22;
		public double v32;
		public double v42;

		public double v13;
		public double v23;
		public double v33;
		public double v43;

		public double v14;
		public double v24;
		public double v34;
		public double v44;

		public double this[int r, int c]
		{
			get
			{
				switch (r)
				{
					case 0:
						switch (c)
						{
							case 0: return v11;
							case 1: return v12;
							case 2: return v13;
							case 3: return v14;
							default: throw new ArgumentException("Invalid Matrix4x4 column", nameof(c));
						}
					case 1:
						switch (c)
						{
							case 0: return v21;
							case 1: return v22;
							case 2: return v23;
							case 3: return v24;
							default: throw new ArgumentException("Invalid Matrix4x4 column", nameof(c));
						}
					case 2:
						switch (c)
						{
							case 0: return v31;
							case 1: return v32;
							case 2: return v33;
							case 3: return v34;
							default: throw new ArgumentException("Invalid Matrix4x4 column", nameof(c));
						}
					case 3:
						switch (c)
						{
							case 0: return v41;
							case 1: return v42;
							case 2: return v43;
							case 3: return v44;
							default: throw new ArgumentException("Invalid Matrix4x4 column", nameof(c));
						}
					default: throw new ArgumentException("Invalid Matrix4x4 row", nameof(r));
				}
			}
			set
			{
				switch (r)
				{
					case 0:
						switch (c)
						{
							case 0: v11 = value; return;
							case 1: v12 = value; return;
							case 2: v13 = value; return;
							case 3: v14 = value; return;
							default: throw new ArgumentException("Invalid Matrix4x4 column", nameof(c));
						}
					case 1:
						switch (c)
						{
							case 0: v21 = value; return;
							case 1: v22 = value; return;
							case 2: v23 = value; return;
							case 3: v24 = value; return;
							default: throw new ArgumentException("Invalid Matrix4x4 column", nameof(c));
						}
					case 2:
						switch (c)
						{
							case 0: v31 = value; return;
							case 1: v32 = value; return;
							case 2: v33 = value; return;
							case 3: v34 = value; return;
							default: throw new ArgumentException("Invalid Matrix4x4 column", nameof(c));
						}
					case 3:
						switch (c)
						{
							case 0: v41 = value; return;
							case 1: v42 = value; return;
							case 2: v43 = value; return;
							case 3: v44 = value; return;
							default: throw new ArgumentException("Invalid Matrix4x4 column", nameof(c));
						}
					default: throw new ArgumentException("Invalid Matrix4x4 row", nameof(r));
				}
			}
		}

		public static Matrix4x4 Identity()
		{
			Matrix4x4 i = new Matrix4x4();
			i.v11 = i.v22 = i.v33 = i.v44 = 1.0;
			return i;
		}

		public static Matrix4x4 RotationX(double angle)
		{
			Matrix4x4 m = Identity();

			var sin = Math.Sin(angle);
			var cos = Math.Cos(angle);

			m.v22 = cos;
			m.v32 = sin;
			m.v23 = -sin;
			m.v33 = cos;

			return m;
		}

		public static Matrix4x4 RotationY(double angle)
		{
			Matrix4x4 m = Identity();

			var sin = Math.Sin(angle);
			var cos = Math.Cos(angle);

			m.v11 = cos;
			m.v31 = -sin;
			m.v13 = sin;
			m.v33 = cos;

			return m;
		}

		public static Matrix4x4 RotationZ(double angle)
		{
			Matrix4x4 m = Identity();

			var sin = Math.Sin(angle);
			var cos = Math.Cos(angle);

			m.v11 = cos;
			m.v21 = sin;
			m.v12 = -sin;
			m.v22 = cos;

			return m;
		}

		public static Matrix4x4 Translation(double x, double y, double z)
		{
			Matrix4x4 m = Identity();

			m.v14 = x;
			m.v24 = y;
			m.v34 = z;

			return m;
		}

		public static Matrix4x4 Scale(double x, double y, double z)
		{
			Matrix4x4 m = Identity();

			m.v11 = x;
			m.v22 = y;
			m.v33 = z;

			return m;
		}

		public static Matrix4x4 Ortho(double left, double right, double bottom, double top, double znear, double zfar)
		{
			Matrix4x4 mat = new Matrix4x4();

			mat.v11 = 2.0 / (right - left);
			mat.v22 = 2.0 / (top - bottom);
			mat.v33 = -2.0 / (zfar - znear);
			mat.v44 = 1.0;

			mat.v14 = -(right + left) / (right - left);
			mat.v24 = -(top + bottom) / (top - bottom);
			mat.v34 = -(zfar + znear) / (zfar - znear);

			return mat;
		}

		public static Matrix4x4 Frustum(double left, double right, double bottom, double top, double znear, double zfar)
		{
			Matrix4x4 mat = new Matrix4x4();

			double temp = 2.0 * znear;
			double width = right - left;
			double height = top - bottom;
			double depth = zfar - znear;

			mat.v11 = temp / width;
			mat.v22 = temp / height;
			mat.v13 = (right + left) / width;
			mat.v23 = (top + bottom) / height;
			mat.v33 = (-zfar - znear) / depth;
			mat.v43 = -1.0;
			mat.v34 = (-temp * zfar) / depth;

			return mat;
		}

		public static explicit operator Matrix3x3(Matrix4x4 oth)
		{
			Matrix3x3 mat;

			mat.v11 = oth.v11;
			mat.v12 = oth.v12;
			mat.v13 = oth.v13;

			mat.v21 = oth.v21;
			mat.v22 = oth.v22;
			mat.v23 = oth.v23;

			mat.v31 = oth.v31;
			mat.v32 = oth.v32;
			mat.v33 = oth.v33;

			return mat;
		}

		public Matrix4x4 Transpose()
		{
			Matrix4x4 tm;

			tm.v11 = v11;
			tm.v12 = v21;
			tm.v13 = v31;
			tm.v14 = v41;

			tm.v21 = v12;
			tm.v22 = v22;
			tm.v23 = v32;
			tm.v24 = v42;

			tm.v31 = v13;
			tm.v32 = v23;
			tm.v33 = v33;
			tm.v34 = v43;

			tm.v41 = v14;
			tm.v42 = v24;
			tm.v43 = v34;
			tm.v44 = v44;

			return tm;
		}

		public Matrix4x4 Inverse()
		{
			Matrix4x4 inv;

			inv.v11 = v22 * v33 * v44 -
					 v22 * v43 * v34 -
					 v23 * v32 * v44 +
					 v23 * v42 * v34 +
					 v24 * v32 * v43 -
					 v24 * v42 * v33;

			inv.v12 = -v12 * v33 * v44 +
					  v12 * v43 * v34 +
					  v13 * v32 * v44 -
					  v13 * v42 * v34 -
					  v14 * v32 * v43 +
					  v14 * v42 * v33;

			inv.v13 = v12 * v23 * v44 -
					 v12 * v43 * v24 -
					 v13 * v22 * v44 +
					 v13 * v42 * v24 +
					 v14 * v22 * v43 -
					 v14 * v42 * v23;

			inv.v14 = -v12 * v23 * v34 +
					   v12 * v33 * v24 +
					   v13 * v22 * v34 -
					   v13 * v32 * v24 -
					   v14 * v22 * v33 +
					   v14 * v32 * v23;

			inv.v21 = -v21 * v33 * v44 +
					  v21 * v43 * v34 +
					  v23 * v31 * v44 -
					  v23 * v41 * v34 -
					  v24 * v31 * v43 +
					  v24 * v41 * v33;

			inv.v22 = v11 * v33 * v44 -
					 v11 * v43 * v34 -
					 v13 * v31 * v44 +
					 v13 * v41 * v34 +
					 v14 * v31 * v43 -
					 v14 * v41 * v33;

			inv.v23 = -v11 * v23 * v44 +
					  v11 * v43 * v24 +
					  v13 * v21 * v44 -
					  v13 * v41 * v24 -
					  v14 * v21 * v43 +
					  v14 * v41 * v23;

			inv.v24 = v11 * v23 * v34 -
					  v11 * v33 * v24 -
					  v13 * v21 * v34 +
					  v13 * v31 * v24 +
					  v14 * v21 * v33 -
					  v14 * v31 * v23;

			inv.v31 = v21 * v32 * v44 -
					 v21 * v42 * v34 -
					 v22 * v31 * v44 +
					 v22 * v41 * v34 +
					 v24 * v31 * v42 -
					 v24 * v41 * v32;

			inv.v32 = -v11 * v32 * v44 +
					  v11 * v42 * v34 +
					  v12 * v31 * v44 -
					  v12 * v41 * v34 -
					  v14 * v31 * v42 +
					  v14 * v41 * v32;

			inv.v33 = v11 * v22 * v44 -
					  v11 * v42 * v24 -
					  v12 * v21 * v44 +
					  v12 * v41 * v24 +
					  v14 * v21 * v42 -
					  v14 * v41 * v22;

			inv.v34 = -v11 * v22 * v34 +
					   v11 * v32 * v24 +
					   v12 * v21 * v34 -
					   v12 * v31 * v24 -
					   v14 * v21 * v32 +
					   v14 * v31 * v22;

			inv.v41 = -v21 * v32 * v43 +
					  v21 * v42 * v33 +
					  v22 * v31 * v43 -
					  v22 * v41 * v33 -
					  v23 * v31 * v42 +
					  v23 * v41 * v32;

			inv.v42 = v11 * v32 * v43 -
					 v11 * v42 * v33 -
					 v12 * v31 * v43 +
					 v12 * v41 * v33 +
					 v13 * v31 * v42 -
					 v13 * v41 * v32;

			inv.v43 = -v11 * v22 * v43 +
					   v11 * v42 * v23 +
					   v12 * v21 * v43 -
					   v12 * v41 * v23 -
					   v13 * v21 * v42 +
					   v13 * v41 * v22;

			inv.v44 = v11 * v22 * v33 -
					  v11 * v32 * v23 -
					  v12 * v21 * v33 +
					  v12 * v31 * v23 +
					  v13 * v21 * v32 -
					  v13 * v31 * v22;

			double det = v11 * inv.v11 + v21 * inv.v12 + v31 * inv.v13 + v41 * inv.v14;

			if (det == 0.0)
				throw new ArithmeticException("Odd matrix");

			det = 1.0 / det;

			inv *= det;

			return inv;
		}

		public Vector4 Col1()
		{
			return new Vector4(v11, v21, v31, v41);
		}

		public Vector4 Col2()
		{
			return new Vector4(v12, v22, v32, v42);
		}

		public Vector4 Col3()
		{
			return new Vector4(v13, v23, v33, v43);
		}

		public Vector4 Col4()
		{
			return new Vector4(v14, v24, v34, v44);
		}

		public Vector4 Row1()
		{
			return new Vector4(v11, v12, v13, v14);
		}

		public Vector4 Row2()
		{
			return new Vector4(v21, v22, v23, v24);
		}

		public Vector4 Row3()
		{
			return new Vector4(v31, v32, v33, v34);
		}

		public Vector4 Row4()
		{
			return new Vector4(v41, v42, v43, v44);
		}

		public static bool operator ==(Matrix4x4 lhs, Matrix4x4 rhs)
		{
			return
				lhs.v11 == rhs.v11 && lhs.v21 == rhs.v21 && lhs.v31 == rhs.v31 && lhs.v41 == rhs.v41 &&
				lhs.v12 == rhs.v12 && lhs.v22 == rhs.v22 && lhs.v32 == rhs.v32 && lhs.v42 == rhs.v42 &&
				lhs.v13 == rhs.v13 && lhs.v23 == rhs.v23 && lhs.v33 == rhs.v33 && lhs.v43 == rhs.v43 &&
				lhs.v14 == rhs.v14 && lhs.v24 == rhs.v24 && lhs.v34 == rhs.v34 && lhs.v44 == rhs.v44;
		}

		public static bool operator !=(Matrix4x4 lhs, Matrix4x4 rhs)
		{
			return !(lhs == rhs);
		}

		public static Matrix4x4 operator +(Matrix4x4 lhs, Matrix4x4 rhs)
		{
			Matrix4x4 m;
			m.v11 = lhs.v11 + rhs.v11; m.v21 = lhs.v21 + rhs.v21; m.v31 = lhs.v31 + rhs.v31; m.v41 = lhs.v41 + rhs.v41;
			m.v12 = lhs.v12 + rhs.v12; m.v22 = lhs.v22 + rhs.v22; m.v32 = lhs.v32 + rhs.v32; m.v42 = lhs.v42 + rhs.v42;
			m.v13 = lhs.v13 + rhs.v13; m.v23 = lhs.v23 + rhs.v23; m.v33 = lhs.v33 + rhs.v33; m.v43 = lhs.v43 + rhs.v43;
			m.v14 = lhs.v14 + rhs.v14; m.v24 = lhs.v24 + rhs.v24; m.v34 = lhs.v34 + rhs.v34; m.v44 = lhs.v44 + rhs.v44;
			return m;
		}

		public static Matrix4x4 operator -(Matrix4x4 lhs, Matrix4x4 rhs)
		{
			return lhs + -rhs;
		}

		public static Matrix4x4 operator -(Matrix4x4 m)
		{
			Matrix4x4 mr;
			mr.v11 = -m.v11; mr.v21 = -m.v21; mr.v31 = -m.v31; mr.v41 = -m.v41;
			mr.v12 = -m.v12; mr.v22 = -m.v22; mr.v32 = -m.v32; mr.v42 = -m.v42;
			mr.v13 = -m.v13; mr.v23 = -m.v23; mr.v33 = -m.v33; mr.v43 = -m.v43;
			mr.v14 = -m.v14; mr.v24 = -m.v24; mr.v34 = -m.v34; mr.v44 = -m.v44;
			return mr;
		}

		public static Matrix4x4 operator *(double s, Matrix4x4 m)
		{
			Matrix4x4 mr;
			mr.v11 = s * m.v11; mr.v21 = s * m.v21; mr.v31 = s * m.v31; mr.v41 = s * m.v41;
			mr.v12 = s * m.v12; mr.v22 = s * m.v22; mr.v32 = s * m.v32; mr.v42 = s * m.v42;
			mr.v13 = s * m.v13; mr.v23 = s * m.v23; mr.v33 = s * m.v33; mr.v43 = s * m.v43;
			mr.v14 = s * m.v14; mr.v24 = s * m.v24; mr.v34 = s * m.v34; mr.v44 = s * m.v44;
			return mr;
		}

		public static Matrix4x4 operator *(Matrix4x4 m, double s)
		{
			return s * m;
		}

		public static Vector4 operator *(Matrix4x4 m, Vector4 v)
		{
			Vector4 w;
			w.x = m.v11 * v.x + m.v12 * v.y + m.v13 * v.z + m.v14 * v.w;
			w.y = m.v21 * v.x + m.v22 * v.y + m.v23 * v.z + m.v24 * v.w;
			w.z = m.v31 * v.x + m.v32 * v.y + m.v33 * v.z + m.v34 * v.w;
			w.w = m.v41 * v.x + m.v42 * v.y + m.v43 * v.z + m.v44 * v.w;
			return w;
		}

		public static Vector4 operator *(Vector4 v, Matrix4x4 m)
		{
			Vector4 w;
			w.x = m.v11 * v.x + m.v21 * v.y + m.v31 * v.z + m.v41 * v.w;
			w.y = m.v12 * v.x + m.v22 * v.y + m.v32 * v.z + m.v42 * v.w;
			w.z = m.v13 * v.x + m.v23 * v.y + m.v33 * v.z + m.v43 * v.w;
			w.w = m.v14 * v.x + m.v24 * v.y + m.v34 * v.z + m.v44 * v.w;
			return w;
		}

		public static Matrix4x4 operator *(Matrix4x4 lhs, Matrix4x4 rhs)
		{
			Matrix4x4 m;

			// 1'st row
			m.v11 = lhs.v11 * rhs.v11 + lhs.v12 * rhs.v21 + lhs.v13 * rhs.v31 + lhs.v14 * rhs.v41;
			m.v12 = lhs.v11 * rhs.v12 + lhs.v12 * rhs.v22 + lhs.v13 * rhs.v32 + lhs.v14 * rhs.v42;
			m.v13 = lhs.v11 * rhs.v13 + lhs.v12 * rhs.v23 + lhs.v13 * rhs.v33 + lhs.v14 * rhs.v43;
			m.v14 = lhs.v11 * rhs.v14 + lhs.v12 * rhs.v24 + lhs.v13 * rhs.v34 + lhs.v14 * rhs.v44;

			// 2'nd row
			m.v21 = lhs.v21 * rhs.v11 + lhs.v22 * rhs.v21 + lhs.v23 * rhs.v31 + lhs.v24 * rhs.v41;
			m.v22 = lhs.v21 * rhs.v12 + lhs.v22 * rhs.v22 + lhs.v23 * rhs.v32 + lhs.v24 * rhs.v42;
			m.v23 = lhs.v21 * rhs.v13 + lhs.v22 * rhs.v23 + lhs.v23 * rhs.v33 + lhs.v24 * rhs.v43;
			m.v24 = lhs.v21 * rhs.v14 + lhs.v22 * rhs.v24 + lhs.v23 * rhs.v34 + lhs.v24 * rhs.v44;

			// 3'rd row
			m.v31 = lhs.v31 * rhs.v11 + lhs.v32 * rhs.v21 + lhs.v33 * rhs.v31 + lhs.v34 * rhs.v41;
			m.v32 = lhs.v31 * rhs.v12 + lhs.v32 * rhs.v22 + lhs.v33 * rhs.v32 + lhs.v34 * rhs.v42;
			m.v33 = lhs.v31 * rhs.v13 + lhs.v32 * rhs.v23 + lhs.v33 * rhs.v33 + lhs.v34 * rhs.v43;
			m.v34 = lhs.v31 * rhs.v14 + lhs.v32 * rhs.v24 + lhs.v33 * rhs.v34 + lhs.v34 * rhs.v44;

			// 4'th row
			m.v41 = lhs.v41 * rhs.v11 + lhs.v42 * rhs.v21 + lhs.v43 * rhs.v31 + lhs.v44 * rhs.v41;
			m.v42 = lhs.v41 * rhs.v12 + lhs.v42 * rhs.v22 + lhs.v43 * rhs.v32 + lhs.v44 * rhs.v42;
			m.v43 = lhs.v41 * rhs.v13 + lhs.v42 * rhs.v23 + lhs.v43 * rhs.v33 + lhs.v44 * rhs.v43;
			m.v44 = lhs.v41 * rhs.v14 + lhs.v42 * rhs.v24 + lhs.v43 * rhs.v34 + lhs.v44 * rhs.v44;

			return m;
		}

		public override bool Equals(object obj)
		{
			try
			{
				return this == (Matrix4x4)obj;
			}
			catch
			{
				return false;
			}
		}

		public override int GetHashCode()
		{
			return base.GetHashCode();
		}

	}
}
