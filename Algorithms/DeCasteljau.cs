﻿using SaQuCAD.Maths;

namespace SaQuCAD.Algorithms
{
	public static class DeCasteljau
	{

		#region Public Methods

		public static void Evaluate(Vector3[] points, double t)
		{
			double u = 1.0 - t;
			for (int i = points.Length; i > 1; --i)
			{
				for (int j = 0; j < i - 1; ++j)
				{
					points[j] = points[j] * u + points[j + 1] * t;
				}
			}
		}

		public static Vector4 EvaluateBasis3(double t)
		{
			Vector4 B = new Vector4();

			B[0] = 1.0;

			for(int i = 1; i < 4; ++i)
			{
				double saved = 0.0;

				for(int j = 0; j <= i; ++j)
				{
					var tmp = B[j];
					B[j] = (1.0 - t) * B[j] + saved;
					saved = t * tmp;
				}
			}

			return B;
		}

		#endregion

	}
}
