﻿using SaQuCAD.Maths;
using System;
using System.Windows.Media.Imaging;

namespace SaQuCAD
{
	public struct IVector2
	{
		public int x;
		public int y;

		public IVector2(int x, int y)
		{
			this.x = x;
			this.y = y;
		}

		public static IVector2 operator +(IVector2 lhs, IVector2 rhs)
		{
			return new IVector2(lhs.x = rhs.x, lhs.y + rhs.y);
		}
	}

	public abstract class Renderer
	{
		protected const double nearClip = 0.1;
		protected const double farClip = 50.0;

		public Matrix4x4 ViewMatrix { get; set; } = Matrix4x4.Identity();
		public Matrix4x4 InvertedViewMatrix { get; set; } = Matrix4x4.Identity();

		protected BitmapContext context;

		public void SetContext(BitmapContext context)
		{
			this.context = context;
		}

		public abstract void DrawPoint(Vector3 coords, int colorCode);

		public abstract void DrawSegment(Vector3 from, Vector3 to, int colorCode);

		public abstract void DrawBezier(Vector3[] points, int colorCode);


		public abstract IVector2? PrerenderPoint(Vector3 point);

		public abstract Tuple<Vector3, Vector3> GetViewspaceRay(double x, double y);

	}
}
