﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;

namespace SaQuCAD.Shapes.Properties
{
    public class PropertyDataTemplateSelector : DataTemplateSelector
    {
        public DataTemplate ITranslatableDataTemplate { get; set; }
        public DataTemplate IFrameableDataTemplate { get; set; }
		public DataTemplate IEditableDataTemplate { get; set; }
		public DataTemplate IUVParametrizedDataTemplate { get; set; }
		public DataTemplate ITorusDataTemplate { get; set; }

		public override DataTemplate SelectTemplate(object item, DependencyObject container)
        {
            Type type = item as Type;

            if (type == typeof(IFrameable))
                return IFrameableDataTemplate;
            if (type == typeof(ITranslatable))
                return ITranslatableDataTemplate;
			if (type == typeof(IEditable))
				return IEditableDataTemplate;
			if (type == typeof(IUVParametrized))
				return IUVParametrizedDataTemplate;
			if (type == typeof(ITorus))
				return ITorusDataTemplate;
            return null;
        }
    }
}
