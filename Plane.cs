﻿using SaQuCAD.Maths;
using System;

namespace SaQuCAD
{
	public class Plane
	{
		public Plane(Vector3 normal, Vector3 p0)
		{
			Normal = normal;
			Distance = -(normal * p0);
			P0 = p0;
		}

		public double Distance { get; private set; }

		public Vector3 Normal { get; private set; }

		public Vector3 P0 { get; private set; }

		public Vector3? RayIntersection(Tuple<Vector3, Vector3> ray)
		{
			var l0 = ray.Item1;
			var target = ray.Item2;
			var l = target - l0;

			if (Math.Abs(l * Normal) <= 0.00001)
				return null;

			var d = (P0 - l0) * Normal / (l * Normal);

			return l0 + d * l;
		}

	}
}
