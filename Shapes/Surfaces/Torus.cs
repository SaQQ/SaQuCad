﻿using System;
using SaQuCAD.Maths;
using SaQuCAD.Shapes.Properties;

namespace SaQuCAD.Shapes.Surfaces
{
	public class Torus : AffineUVMesh, ITorus
    {

        #region Public Properties

        public double R { get { return _R; } set { _R = value; RecreateMesh(); } }

        public double r { get { return _r; } set { _r = value; RecreateMesh(); } }

		public override double UMin => 0.0;

        public override double UMax => 2.0 * Math.PI;

        public override double VMin => 0.0;

        public override double VMax => 2.0 * Math.PI;

        public override ParamProps UProperties => ParamProps.Cyclic;

        public override ParamProps VProperties => ParamProps.Cyclic;
	
        #endregion

        #region Constructors

        public Torus(double R, double r, string name = "New Torus")
		{
			Name = name;
			_r = r;
			_R = R;
			RecreateMesh();
		}

		#endregion

		#region Public Methods

		public override Vector3 Evaluate(double u, double v)
		{
			return (ModelMatrix * EvaluateInSelfSpace(u, v).AddW()).WithoutW();
		}

		public override Vector3 EvaluateUDerivative(double u, double v)
		{
			return (ModelMatrix * new Vector4(-(_R + _r * Math.Cos(v)) * Math.Sin(u), 0.0, (_R + _r * Math.Cos(v)) * Math.Cos(u), 0.0)).WithoutW();
		}

		public override Vector3 EvaluateVDerivative(double u, double v)
		{
			return (ModelMatrix * new Vector4(-_r * Math.Sin(v) * Math.Cos(u), _r * Math.Cos(v), -_r * Math.Sin(v) * Math.Sin(u), 0.0)).WithoutW();
		}

		#endregion

		#region Private Helpers

		private Vector3 EvaluateInSelfSpace(double u, double v)
		{
			return new Vector3(
				(_R + _r * Math.Cos(v)) * Math.Cos(u), 
				_r * Math.Sin(v),
				(_R + _r * Math.Cos(v)) * Math.Sin(u)
				);
		}

		#endregion

		#region Private Data

		private double _R;
		private double _r;

		#endregion

	}
}
