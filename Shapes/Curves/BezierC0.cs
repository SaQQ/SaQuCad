﻿using SaQuCAD.Maths;
using SaQuCAD.Shapes.Properties;

namespace SaQuCAD.Shapes.Curves
{
	public class BezierC0 : ControlpointsBase, IFrameable
	{

		#region Public Properties

		public bool FrameVisible { get { return frameVisible; } set { frameVisible = value; RaisePropertyChanged(); OnChanged(); } }

		#endregion

		#region Constructors

		public BezierC0(Point[] points, string name = "New Bezier") : base(points)
		{
			Name = name;
		}

		#endregion

		#region Private Fields

		private bool frameVisible;

		#endregion

		#region Public Methods

		public override void Render(Renderer renderer)
		{
			if (!Visible)
				return;

			if (ControlPoints.Count < 2)
				return;

            if(frameVisible)
            {
                for (int i = 0; i < ControlPoints.Count - 1; ++i)
                    renderer.DrawSegment(ControlPoints[i].Position, ControlPoints[i + 1].Position, ColorCodes.RenderFrame);
            }

			for (int i = 0; i < ControlPoints.Count - 1; )
			{
				int degree = System.Math.Min(ControlPoints.Count - i - 1, 3);

				Vector3[] points = new Vector3[degree + 1];

				for (int j = 0; j < degree + 1; ++j)
					points[j] = ControlPoints[i + j].Position;

				i += degree;

				renderer.DrawBezier(points, ColorCode);
			}

		}

		#endregion

	}
}
