﻿using SaQuCAD.Maths;

namespace SaQuCAD.Algorithms
{
	public static class DeBoor
	{
		#region Public Methods

		public static Vector4 Evaluate3Regular(double t)
		{
			double tm = 1.0 - t;

			Vector4 N = new Vector4();

			N[0] = 1.0;

			for (int j = 1; j <= 3; ++j)
			{
				double saved = 0.0;

				for (int k = 1; k <= j; ++k)
				{
					double term = N[k - 1] / (/*A[k]*/(tm + k - 1.0) + /*B[j + 1 - k]*/(t + j - k));
					N[k - 1] = saved + /*A[k]*/(tm + k - 1.0) * term;
					saved = /*B[j + 1 - k]*/(t + j - k) * term;
				}

				N[j] = saved;
			}

			return N;
		}

		public static Vector3 Evaluate2Regular(double t)
		{
			double tm = 1.0 - t;

			Vector3 N = new Vector3();

			N[0] = 1.0;

			for (int j = 1; j <= 2; ++j)
			{
				double saved = 0.0;

				for (int k = 1; k <= j; ++k)
				{
					double term = N[k - 1] / (/*A[k]*/(tm + k - 1.0) + /*B[j + 1 - k]*/(t + j - k));
					N[k - 1] = saved + /*A[k]*/(tm + k - 1.0) * term;
					saved = /*B[j + 1 - k]*/(t + j - k) * term;
				}

				N[j] = saved;
			}

			return N;
		}

		#endregion
	}
}
