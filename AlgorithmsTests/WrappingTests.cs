﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using SaQuCAD.Algorithms;
using SaQuCAD.Maths;
using SaQuCAD.Shapes.Properties;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlgorithmsTests
{
	[TestClass]
	public class WrappingTests
	{

		[TestMethod]
		public void Bounded()
		{
			var surf = new SurfaceMock(-1.0, 1.0, -1.0, 1.0, 0, 0);

			var wrap = Wrapping.CraftWrapFunc(surf);

			ExpectPreservingWrap(wrap, 
				new Vector2((surf.UMin + surf.UMax) / 2.0, (surf.VMin + surf.VMax) / 2.0), 
				Wrapping.WrapType.None, Wrapping.WrapType.None);

			ExpectPreservingWrap(wrap,
				new Vector2(surf.UMin - 0.1, surf.VMin + 0.1),
				Wrapping.WrapType.OutOfBounds, Wrapping.WrapType.None);

			ExpectPreservingWrap(wrap,
				new Vector2(surf.UMax + 0.1, surf.VMin + 0.1),
				Wrapping.WrapType.OutOfBounds, Wrapping.WrapType.None);

			ExpectPreservingWrap(wrap,
				new Vector2(surf.UMin + 0.1, surf.VMin - 0.1),
				Wrapping.WrapType.None, Wrapping.WrapType.OutOfBounds);

			ExpectPreservingWrap(wrap,
				new Vector2(surf.UMin + 0.1, surf.VMax + 0.1),
				Wrapping.WrapType.None, Wrapping.WrapType.OutOfBounds);

			ExpectPreservingWrap(wrap,
				new Vector2(surf.UMin - 0.1, surf.VMin - 0.1),
				Wrapping.WrapType.OutOfBounds, Wrapping.WrapType.OutOfBounds);

			ExpectPreservingWrap(wrap,
				new Vector2(surf.UMin - 0.1, surf.VMax + 0.1),
				Wrapping.WrapType.OutOfBounds, Wrapping.WrapType.OutOfBounds);

			ExpectPreservingWrap(wrap,
				new Vector2(surf.UMax + 0.1, surf.VMin - 0.1),
				Wrapping.WrapType.OutOfBounds, Wrapping.WrapType.OutOfBounds);

			ExpectPreservingWrap(wrap,
				new Vector2(surf.UMax + 0.1, surf.VMax + 0.1),
				Wrapping.WrapType.OutOfBounds, Wrapping.WrapType.OutOfBounds);

		}

		[TestMethod]
		public void UCyclic()
		{
			var surf = new SurfaceMock(-1.0, 1.0, -1.0, 1.0, ParamProps.Cyclic, 0);

			var wrap = Wrapping.CraftWrapFunc(surf);

			ExpectPreservingWrap(wrap,
				new Vector2((surf.UMin + surf.UMax) / 2.0, (surf.VMin + surf.VMax) / 2.0),
				Wrapping.WrapType.None, Wrapping.WrapType.None);

			ExpectPreservingWrap(wrap,
				new Vector2(surf.UMin + 0.1, surf.VMin - 0.1),
				Wrapping.WrapType.None, Wrapping.WrapType.OutOfBounds);

			ExpectPreservingWrap(wrap,
				new Vector2(surf.UMin + 0.1, surf.VMax + 0.1),
				Wrapping.WrapType.None, Wrapping.WrapType.OutOfBounds);

			ExpectWrap(wrap,
				new Vector2(surf.UMin - 0.1, surf.VMin + 0.1),
				new Vector2(surf.UMax - 0.1, surf.VMin + 0.1),
				Wrapping.WrapType.Min, Wrapping.WrapType.None);

			ExpectWrap(wrap,
				new Vector2(surf.UMax + 0.1, surf.VMin + 0.1),
				new Vector2(surf.UMin + 0.1, surf.VMin + 0.1),
				Wrapping.WrapType.Max, Wrapping.WrapType.None);

			ExpectWrap(wrap,
				new Vector2(surf.UMin - 0.1, surf.VMin - 0.1),
				new Vector2(surf.UMax - 0.1, surf.VMin - 0.1),
				Wrapping.WrapType.Min, Wrapping.WrapType.OutOfBounds);

			ExpectWrap(wrap,
				new Vector2(surf.UMin - 0.1, surf.VMax + 0.1),
				new Vector2(surf.UMax - 0.1, surf.VMax + 0.1),
				Wrapping.WrapType.Min, Wrapping.WrapType.OutOfBounds);

			ExpectWrap(wrap,
				new Vector2(surf.UMax + 0.1, surf.VMin - 0.1),
				new Vector2(surf.UMin + 0.1, surf.VMin - 0.1),
				Wrapping.WrapType.Max, Wrapping.WrapType.OutOfBounds);

			ExpectWrap(wrap,
				new Vector2(surf.UMax + 0.1, surf.VMax + 0.1),
				new Vector2(surf.UMin + 0.1, surf.VMax + 0.1),
				Wrapping.WrapType.Max, Wrapping.WrapType.OutOfBounds);

		}

		[TestMethod]
		public void UInvertedCyclic()
		{
			var surf = new SurfaceMock(-1.0, 1.0, -1.0, 1.0, ParamProps.Cyclic | ParamProps.Inverted, 0);

			var wrap = Wrapping.CraftWrapFunc(surf);

			ExpectPreservingWrap(wrap,
				new Vector2((surf.UMin + surf.UMax) / 2.0, (surf.VMin + surf.VMax) / 2.0),
				Wrapping.WrapType.None, Wrapping.WrapType.None);

			ExpectPreservingWrap(wrap,
				new Vector2(surf.UMin + 0.1, surf.VMin - 0.1),
				Wrapping.WrapType.None, Wrapping.WrapType.OutOfBounds);

			ExpectPreservingWrap(wrap,
				new Vector2(surf.UMin + 0.1, surf.VMax + 0.1),
				Wrapping.WrapType.None, Wrapping.WrapType.OutOfBounds);

			ExpectWrap(wrap,
				new Vector2(surf.UMin - 0.1, surf.VMin + 0.1),
				new Vector2(surf.UMax - 0.1, surf.VMax - 0.1),
				Wrapping.WrapType.Min | Wrapping.WrapType.Inverted, Wrapping.WrapType.None);

			ExpectWrap(wrap,
				new Vector2(surf.UMax + 0.1, surf.VMin + 0.1),
				new Vector2(surf.UMin + 0.1, surf.VMax - 0.1),
				Wrapping.WrapType.Max | Wrapping.WrapType.Inverted, Wrapping.WrapType.None);

			ExpectWrap(wrap,
				new Vector2(surf.UMin - 0.1, surf.VMin - 0.1),
				new Vector2(surf.UMax - 0.1, surf.VMax + 0.1),
				Wrapping.WrapType.Min | Wrapping.WrapType.Inverted, Wrapping.WrapType.OutOfBounds);

			ExpectWrap(wrap,
				new Vector2(surf.UMin - 0.1, surf.VMax + 0.1),
				new Vector2(surf.UMax - 0.1, surf.VMin - 0.1),
				Wrapping.WrapType.Min | Wrapping.WrapType.Inverted, Wrapping.WrapType.OutOfBounds);

			ExpectWrap(wrap,
				new Vector2(surf.UMax + 0.1, surf.VMin - 0.1),
				new Vector2(surf.UMin + 0.1, surf.VMax + 0.1),
				Wrapping.WrapType.Max | Wrapping.WrapType.Inverted, Wrapping.WrapType.OutOfBounds);

			ExpectWrap(wrap,
				new Vector2(surf.UMax + 0.1, surf.VMax + 0.1),
				new Vector2(surf.UMin + 0.1, surf.VMin - 0.1),
				Wrapping.WrapType.Max | Wrapping.WrapType.Inverted, Wrapping.WrapType.OutOfBounds);

		}

		[TestMethod]
		public void UVCyclic()
		{
			var surf = new SurfaceMock(-1.0, 1.0, -1.0, 1.0, ParamProps.Cyclic, ParamProps.Cyclic);

			var wrap = Wrapping.CraftWrapFunc(surf);

			ExpectPreservingWrap(wrap,
				new Vector2((surf.UMin + surf.UMax) / 2.0, (surf.VMin + surf.VMax) / 2.0),
				Wrapping.WrapType.None, Wrapping.WrapType.None);

			ExpectWrap(wrap,
				new Vector2(surf.UMin + 0.1, surf.VMin - 0.1),
				new Vector2(surf.UMin + 0.1, surf.VMax - 0.1),
				Wrapping.WrapType.None, Wrapping.WrapType.Min);

			ExpectWrap(wrap,
				new Vector2(surf.UMin + 0.1, surf.VMax + 0.1),
				new Vector2(surf.UMin + 0.1, surf.VMin + 0.1),
				Wrapping.WrapType.None, Wrapping.WrapType.Max);

			ExpectWrap(wrap,
				new Vector2(surf.UMin - 0.1, surf.VMin + 0.1),
				new Vector2(surf.UMax - 0.1, surf.VMin + 0.1),
				Wrapping.WrapType.Min, Wrapping.WrapType.None);

			ExpectWrap(wrap,
				new Vector2(surf.UMax + 0.1, surf.VMin + 0.1),
				new Vector2(surf.UMin + 0.1, surf.VMin + 0.1),
				Wrapping.WrapType.Max, Wrapping.WrapType.None);

			ExpectWrap(wrap,
				new Vector2(surf.UMin - 0.1, surf.VMin - 0.1),
				new Vector2(surf.UMax - 0.1, surf.VMax - 0.1),
				Wrapping.WrapType.Min, Wrapping.WrapType.Min);

			ExpectWrap(wrap,
				new Vector2(surf.UMin - 0.1, surf.VMax + 0.1),
				new Vector2(surf.UMax - 0.1, surf.VMin + 0.1),
				Wrapping.WrapType.Min, Wrapping.WrapType.Max);

			ExpectWrap(wrap,
				new Vector2(surf.UMax + 0.1, surf.VMin - 0.1),
				new Vector2(surf.UMin + 0.1, surf.VMax - 0.1),
				Wrapping.WrapType.Max, Wrapping.WrapType.Min);

			ExpectWrap(wrap,
				new Vector2(surf.UMax + 0.1, surf.VMax + 0.1),
				new Vector2(surf.UMin + 0.1, surf.VMin + 0.1),
				Wrapping.WrapType.Max, Wrapping.WrapType.Max);

		}

		private void ExpectPreservingWrap(Wrapping.WrapFuncDelegate wrap, Vector2 input, Wrapping.WrapType uType, Wrapping.WrapType vType)
		{
			ExpectWrap(wrap, input, input, uType, vType);
		}

		private void ExpectWrap(Wrapping.WrapFuncDelegate wrap, Vector2 input, Vector2 output, Wrapping.WrapType uType, Wrapping.WrapType vType)
		{
			var type = wrap(ref input);
			Assert.AreEqual(uType, type.UWrap);
			Assert.AreEqual(vType, type.VWrap);
			Assert.AreEqual(output, input);
		}

	}
}
