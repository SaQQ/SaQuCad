﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SaQuCAD.Shapes.Curves
{
    public class Polyline : ControlpointsBase
    {

        #region Constructors

        public Polyline(Point[] points, string name = "New Polyline") : base(points)
        {
            Name = name;
        }

        #endregion

        #region Public Methods

        public override void Render(Renderer renderer)
        {
            if (!Visible)
                return;

            if (ControlPoints.Count < 2)
                return;

            for (int i = 0; i < ControlPoints.Count - 1; ++i)
                renderer.DrawSegment(ControlPoints[i].Position, ControlPoints[i + 1].Position, ColorCode);
        }

        #endregion

    }
}
