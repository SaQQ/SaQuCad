﻿using SaQuCAD.Infrastructure;
using SaQuCAD.Maths;
using SaQuCAD.Shapes.Properties;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace SaQuCAD.Algorithms
{
    public static class Intersections
    {

        #region Public Types

        public enum PointType
        {
            Regular,
            Start,
            ParametrizationCorner,
            BoundHit,
            BoundWrapStart,
            BoundWrapEnd,
        }

        public class IntersectionPoint
        {

            #region Public Properties

            public PointType Type { get; set; }

            public Wrapping.Boundary Boundary { get; set; }

            public Vector2 UV { get; set; }

            public IntersectionPoint Next { get; set; }

            public IntersectionPoint Previous { get; set; }

            #endregion

            #region Public Methods

            public override string ToString()
            {
                return string.Format("({0:0.00}, {1:0.00})", UV.x, UV.y);
            }

            #endregion

        }

        public enum Side
        {
            A = 1, B = 2
        }

        public class IntersectionEdge
        {

            #region Public Properties

            public IntersectionPoint From { get; set; }

            public IntersectionPoint To { get; set; }

            public IntersectionEdge Next { get; set; }

            public IntersectionEdge Fused { get; set; }

            public Side Side { get; set; } = 0;

            #endregion

        }

        public class Trimmer : BindableObject, ITrimmer
        {

            #region Public Properties

            public bool Inverted { get { return inverted; } set { inverted = value; Changed?.Invoke(); RaisePropertyChanged(); } }

            public WriteableBitmap Image { get; private set; }

            public event ChangedEventHandler Changed;

			public ICommand ShowUV { get; set; }

            #endregion

            #region Constructors

            public Trimmer(IUVParametrized surf, List<List<IntersectionEdge>> polygons)
            {
                this.polygons = polygons.Select(p => p.Select(e => e.From.UV).ToArray()).ToList();

                const int scale = 200;
                const int margin = 20;

                const int div = 20;

                var uWidth = surf.UMax - surf.UMin;
                var vWidth = surf.VMax - surf.VMin;

                WriteableBitmap bmp = new WriteableBitmap((int)(uWidth * scale) + 2 * margin, (int)(vWidth * scale) + 2 * margin, 96, 96, PixelFormats.Bgra32, null);

                using (var ctx = bmp.GetBitmapContext())
                {
                    foreach (var polygon in polygons)
                        foreach (var edge in polygon)
                            CustomWritableBitmapExtensions.DrawLineDDACustom(ctx,
                                (int)(scale * (edge.From.UV.x - surf.UMin)) + margin,
                                (int)(scale * (edge.From.UV.y - surf.VMin)) + margin,
                                (int)(scale * (edge.To.UV.x - surf.UMin)) + margin,
                                (int)(scale * (edge.To.UV.y - surf.VMin)) + margin,
                                ColorCodes.RenderRed);


                    for (int u = 0; u < div; ++u)
                        for (int v = 0; v < div; ++v)
                            if (Test(surf.UMin + u * uWidth / (div - 1), surf.VMin + v * vWidth / (div - 1)))
                                CustomWritableBitmapExtensions.DrawCircleCustom(ctx, (int)(scale * u * uWidth / (div - 1)) + margin, (int)(scale * v * vWidth / (div - 1)) + margin, 2, ColorCodes.RenderBlue);

                }

                Image = bmp;

            }

            #endregion

            #region Public Methods

            public bool Test(double u, double v)
            {
                foreach (var polygon in polygons)
                    if (TestPolygon(polygon, u, v))
                        return !inverted;
                return inverted;
            }

            #endregion

            #region Private Helpers

            private bool TestPolygon(Vector2[] polygon, double u, double v)
            {
                return Geometry.PointInPolygon(polygon, new Vector2(u, v));
            }

            #endregion

            #region Private Data

            private bool inverted;

            List<Vector2[]> polygons;

            #endregion

        }

        public class IntersectionGraph
        {
            #region Constructors

            public IntersectionGraph(IUVParametrized surf)
            {
                surface = surf;

                UMinVMin = new IntersectionPoint() { Type = PointType.ParametrizationCorner, UV = new Vector2(surf.UMin, surf.VMin) };
                UMinVMax = new IntersectionPoint() { Type = PointType.ParametrizationCorner, UV = new Vector2(surf.UMin, surf.VMax) };
                UMaxVMin = new IntersectionPoint() { Type = PointType.ParametrizationCorner, UV = new Vector2(surf.UMax, surf.VMin) };
                UMaxVMax = new IntersectionPoint() { Type = PointType.ParametrizationCorner, UV = new Vector2(surf.UMax, surf.VMax) };

                var uMinEdge = new IntersectionEdge() { From = UMinVMin, To = UMinVMax };
                var uMaxEdge = new IntersectionEdge() { From = UMaxVMax, To = UMaxVMin };
                var vMinEdge = new IntersectionEdge() { From = UMaxVMin, To = UMinVMin };
                var vMaxEdge = new IntersectionEdge() { From = UMinVMax, To = UMaxVMax };

                BoundaryEdges.Add(Wrapping.Boundary.UMin, new List<IntersectionEdge>() { uMinEdge });
                BoundaryEdges.Add(Wrapping.Boundary.UMax, new List<IntersectionEdge>() { uMaxEdge });
                BoundaryEdges.Add(Wrapping.Boundary.VMin, new List<IntersectionEdge>() { vMinEdge });
                BoundaryEdges.Add(Wrapping.Boundary.VMax, new List<IntersectionEdge>() { vMaxEdge });

                uMinEdge.Next = vMaxEdge;
                vMaxEdge.Next = uMaxEdge;
                uMaxEdge.Next = vMinEdge;
                vMinEdge.Next = uMinEdge;

                if ((surf.UProperties & ParamProps.Cyclic) == ParamProps.Cyclic)
                {
                    uMinEdge.Fused = uMaxEdge;
                    uMaxEdge.Fused = uMinEdge;
                }

                if ((surf.VProperties & ParamProps.Cyclic) == ParamProps.Cyclic)
                {
                    vMinEdge.Fused = vMaxEdge;
                    vMaxEdge.Fused = vMinEdge;
                }
            }

            #endregion

            #region Public Methods

            public void InsertIntersectionPoints(IntersectionPoint start)
            {

                IntersectionEdge prevForwardEdge = null;
                IntersectionEdge prevBackwardEdge = null;

                var prevPoint = start;

                bool run = true;
                bool looped = false;

                while (run)
                {
                    var currPoint = prevPoint.Next;

                    if (currPoint == null) // Curve unexpectedly ended
                        break;

                    switch (currPoint.Type)
                    {
                        case PointType.Start:
                            if (prevForwardEdge == null)
                            {
                                StartForwardEdge = prevForwardEdge = new IntersectionEdge() { From = prevPoint, To = currPoint };
                                StartBackwardEdge = prevBackwardEdge = new IntersectionEdge() { From = currPoint, To = prevPoint };
                                prevForwardEdge.Next = prevBackwardEdge;
                                prevBackwardEdge.Next = prevForwardEdge;
                            }
                            else
                            {
                                prevForwardEdge.Next = new IntersectionEdge() { From = prevPoint, To = currPoint, Next = StartForwardEdge };
                                StartBackwardEdge.Next = new IntersectionEdge() { From = currPoint, To = prevPoint, Next = prevBackwardEdge };
                            }

                            run = false;
                            looped = true;
                            break;
                        case PointType.Regular:
                            if (prevForwardEdge == null)
                            {
                                StartForwardEdge = prevForwardEdge = new IntersectionEdge() { From = prevPoint, To = currPoint };
                                StartBackwardEdge = prevBackwardEdge = new IntersectionEdge() { From = currPoint, To = prevPoint };
                                prevForwardEdge.Next = prevBackwardEdge;
                                prevBackwardEdge.Next = prevForwardEdge;
                            }
                            else
                            {
                                prevForwardEdge.Next = new IntersectionEdge() { From = prevPoint, To = currPoint };
                                prevForwardEdge = prevForwardEdge.Next;
                                prevBackwardEdge = new IntersectionEdge() { From = currPoint, To = prevPoint, Next = prevBackwardEdge };
                                prevForwardEdge.Next = prevBackwardEdge;
                            }
                            break;
                        case PointType.ParametrizationCorner:
                            throw new ArgumentException("Parametrization Corner found on intersection edge");
                        case PointType.BoundWrapStart:
                            {
                                var wrapStart = currPoint;
                                var wrapEnd = wrapStart.Next;
                                var nextPoint = wrapEnd.Next;

                                if (wrapEnd.Type != PointType.BoundWrapEnd)
                                    throw new ArgumentException("Bound Wrap Start not followed by Bound Wrap End");
                                if (nextPoint.Type != PointType.Regular)
                                    throw new ArgumentException("Bound Wrap End not followed by Regular Point");

                                IntersectionEdge startPrev, startNext, endPrev, endNext;
                                SplitOnBoundaryWrap(wrapStart, wrapEnd, out startPrev, out startNext, out endPrev, out endNext);

                                if (prevForwardEdge == null)
                                {
                                    StartForwardEdge = new IntersectionEdge() { From = prevPoint, To = wrapStart, Next = startNext };
                                    StartBackwardEdge = startPrev.Next = new IntersectionEdge() { From = wrapStart, To = prevPoint };
                                    StartBackwardEdge.Next = StartForwardEdge;

                                    prevForwardEdge = endPrev.Next = new IntersectionEdge() { From = wrapEnd, To = nextPoint };
                                    prevBackwardEdge = new IntersectionEdge() { From = nextPoint, To = wrapEnd, Next = endNext };
                                    prevForwardEdge.Next = prevBackwardEdge;
                                }
                                else
                                {
                                    prevForwardEdge.Next = new IntersectionEdge() { From = prevPoint, To = wrapStart, Next = startNext };
                                    startPrev.Next = new IntersectionEdge() { From = wrapStart, To = prevPoint, Next = prevBackwardEdge };

                                    prevForwardEdge = endPrev.Next = new IntersectionEdge() { From = wrapEnd, To = nextPoint };
                                    prevBackwardEdge = new IntersectionEdge() { From = nextPoint, To = wrapEnd, Next = endNext };
                                    prevForwardEdge.Next = prevBackwardEdge;
                                }

                                currPoint = nextPoint;
                            }
                            break;
                        case PointType.BoundWrapEnd:
                            throw new ArgumentException("Unmached Bound Wrap End found on intersection edge");
                        case PointType.BoundHit:
                            {
                                IntersectionEdge prev, next;
                                SplitBoundaryEdges(currPoint, out prev, out next);

                                if (prevForwardEdge == null)
                                {
                                    StartForwardEdge = new IntersectionEdge() { From = prevPoint, To = currPoint, Next = next };
                                    StartBackwardEdge = prev.Next = new IntersectionEdge() { From = currPoint, To = prevPoint };
                                    StartBackwardEdge.Next = StartForwardEdge;
                                }
                                else
                                {
                                    prevForwardEdge.Next = new IntersectionEdge() { From = prevPoint, To = currPoint, Next = next };
                                    prev.Next = new IntersectionEdge() { From = currPoint, To = prevPoint, Next = prevBackwardEdge };
                                }

                                run = false;
                            }
                            break;
                    }

                    prevPoint = currPoint;
                }

                if (looped)
                    return;

                prevForwardEdge = StartForwardEdge;
                prevBackwardEdge = StartBackwardEdge;

                prevPoint = start;

                run = true;

                while (run)
                {
                    var currPoint = prevPoint.Previous;

                    if (currPoint == null) // Curve unexpectedly ended
                        break;

                    switch (currPoint.Type)
                    {
                        case PointType.Start:
                            throw new Exception("Odd loop topology");
                        case PointType.Regular:
                            
                            prevBackwardEdge.Next = new IntersectionEdge() { From = prevPoint, To = currPoint };
                            prevBackwardEdge = prevBackwardEdge.Next;
                            prevForwardEdge = new IntersectionEdge() { From = currPoint, To = prevPoint, Next = prevForwardEdge };
                            prevBackwardEdge.Next = prevForwardEdge;

                            break;
                        case PointType.ParametrizationCorner:
                            throw new ArgumentException("Parametrization Corner found on intersection edge");
                        case PointType.BoundWrapStart:
                            throw new ArgumentException("Unmached Bound Wrap Start found on intersection edge");
                        case PointType.BoundWrapEnd:
                            {
                                var wrapEnd = currPoint;
                                var wrapStart = wrapEnd.Previous;
                                var nextPoint = wrapStart.Previous;

                                if (wrapStart.Type != PointType.BoundWrapStart)
                                    throw new ArgumentException("Bound Wrap End not preceded by Bound Wrap Start");
                                if (nextPoint.Type != PointType.Regular)
                                    throw new ArgumentException("Bound Wrap Start not preceded by Regular Point");

                                IntersectionEdge startPrev, startNext, endPrev, endNext;
                                SplitOnBoundaryWrap(wrapStart, wrapEnd, out startPrev, out startNext, out endPrev, out endNext);

                                endPrev.Next = new IntersectionEdge() { From = wrapEnd, To = prevPoint, Next = prevForwardEdge };
                                prevBackwardEdge.Next = new IntersectionEdge() { From = prevPoint, To = wrapEnd, Next = endNext };

                                prevForwardEdge =  new IntersectionEdge() { From = nextPoint, To = wrapStart, Next = startNext };
                                prevBackwardEdge =  startPrev.Next = new IntersectionEdge() { From = wrapStart, To = nextPoint };

                                prevBackwardEdge.Next = prevForwardEdge;

                                currPoint = nextPoint;
                            }
                            break;
                        case PointType.BoundHit:
                            {
                                IntersectionEdge prev, next;
                                SplitBoundaryEdges(currPoint, out prev, out next);

                                prev.Next = new IntersectionEdge() { From = currPoint, To = prevPoint, Next = prevForwardEdge };
                                prevBackwardEdge.Next = new IntersectionEdge() { From = prevPoint, To = currPoint, Next = next };

                                run = false;
                            }
                            break;
                    }

                    prevPoint = currPoint;
                }

            }

            public Trimmer ClassifyEdges()
            {
                List<IntersectionEdge> polygonsStarts = new List<IntersectionEdge>();

                FloodEdges(StartForwardEdge, Side.A, polygonsStarts);
                // FloodEdges(StartBackwardEdge, Side.B, new List<IntersectionEdge>());

                return BuildTrimmer(polygonsStarts);
            }

            public void SplitBoundaryEdges(IntersectionPoint bounaryPoint, out IntersectionEdge prevEdge, out IntersectionEdge nextEdge)
            {
                var edges = BoundaryEdges[bounaryPoint.Boundary];

                var ind = GetIntervalIndex(
                    EdgesToPoints(edges, (IntersectionPoint p) => (bounaryPoint.Boundary == Wrapping.Boundary.UMin || bounaryPoint.Boundary == Wrapping.Boundary.UMax) ? p.UV.y : p.UV.x),
                    (bounaryPoint.Boundary == Wrapping.Boundary.UMin || bounaryPoint.Boundary == Wrapping.Boundary.UMax) ? bounaryPoint.UV.y : bounaryPoint.UV.x
                );

                var splittedEdge = edges[ind];

                var newEdge = new IntersectionEdge() { From = bounaryPoint, To = splittedEdge.To, Next = splittedEdge.Next };
                splittedEdge.To = bounaryPoint;
                splittedEdge.Next = newEdge;

                edges.Insert(ind + 1, newEdge);

                prevEdge = splittedEdge;
                nextEdge = newEdge;
            }

            public void SplitOnBoundaryWrap(IntersectionPoint wrapStart, IntersectionPoint wrapEnd,
                out IntersectionEdge startPrev, out IntersectionEdge startNext,
                out IntersectionEdge endPrev, out IntersectionEdge endNext)
            {
                SplitBoundaryEdges(wrapStart, out startPrev, out startNext);
                SplitBoundaryEdges(wrapEnd, out endPrev, out endNext);

                bool inverted;

                if (wrapStart.Boundary == Wrapping.Boundary.UMin || wrapStart.Boundary == Wrapping.Boundary.UMax)
                {
                    if ((surface.UProperties & ParamProps.Cyclic) != ParamProps.Cyclic)
                        throw new ArgumentException("Wrap Split on non cyclic U parametrization");

                    inverted = (surface.UProperties & ParamProps.Inverted) == ParamProps.Inverted;
                }
                else if (wrapStart.Boundary == Wrapping.Boundary.VMin || wrapStart.Boundary == Wrapping.Boundary.VMax)
                {
                    if ((surface.VProperties & ParamProps.Cyclic) != ParamProps.Cyclic)
                        throw new ArgumentException("Wrap Split on non cyclic V parametrization");

                    inverted = (surface.VProperties & ParamProps.Inverted) == ParamProps.Inverted;
                }
                else throw new NotImplementedException();

                if (!inverted)
                {
                    startPrev.Fused = endNext;
                    endNext.Fused = startPrev;
                    startNext.Fused = endPrev;
                    endPrev.Fused = startNext;
                }
                else
                {
                    startPrev.Fused = endPrev;
                    endPrev.Fused = startPrev;
                    startNext.Fused = endNext;
                    endNext.Fused = startNext;
                }
            }

            #endregion

            #region Private Helpers

            private Trimmer BuildTrimmer(List<IntersectionEdge> polygonsStarts)
            {
                List<List<IntersectionEdge>> polygons = new List<List<IntersectionEdge>>();

                foreach (var start in polygonsStarts)
                {
                    List<IntersectionEdge> polygon = new List<IntersectionEdge>();
                    polygon.Add(start);

                    var curr = start.Next;

                    while (curr != start)
                    {
                        polygon.Add(curr);
                        curr = curr.Next;
                    }

                    polygons.Add(polygon);
                }

                return new Trimmer(surface, polygons);
            }

            private void FloodEdges(IntersectionEdge edge, Side side, List<IntersectionEdge> polygons, bool polygonStart = true)
            {
                if (edge.Side == side)
                    return;
                if (edge.Side != 0)
                    throw new ArgumentException("Sides conflict detected");

                if (polygonStart)
                    polygons.Add(edge);

                edge.Side = side;
                FloodEdges(edge.Next, side, polygons, false);
                if (edge.Fused != null)
                    FloodEdges(edge.Fused, side, polygons, true);
            }

            private Wrapping.Boundary PrevBoundary(Wrapping.Boundary boundary)
            {
                switch (boundary)
                {
                    case Wrapping.Boundary.UMin: return Wrapping.Boundary.VMin;
                    case Wrapping.Boundary.VMin: return Wrapping.Boundary.UMax;
                    case Wrapping.Boundary.UMax: return Wrapping.Boundary.VMax;
                    case Wrapping.Boundary.VMax: return Wrapping.Boundary.UMin;
                    default: throw new ArgumentException("Bounds composition does not have predecessor", nameof(boundary));
                }
            }

            private static List<T> EdgesToPoints<T>(List<IntersectionEdge> edges, Func<IntersectionPoint, T> selector)
            {
                var ret = edges.Select(e => selector(e.From)).ToList();
                ret.Add(selector(edges.Last().To));
                return ret;
            }

            private static int GetIntervalIndex(List<double> intervalPoints, double val)
            {
                for (int i = 0; i < intervalPoints.Count - 1; ++i)
                    if (val >= Math.Min(intervalPoints[i], intervalPoints[i + 1]) && val <= Math.Max(intervalPoints[i], intervalPoints[i + 1]))
                        return i;
                throw new ArgumentException("Value out of interval range", nameof(val));
            }

            #endregion

            #region Private Data

            public IntersectionPoint UMinVMin { get; private set; }
            public IntersectionPoint UMinVMax { get; private set; }
            public IntersectionPoint UMaxVMin { get; private set; }
            public IntersectionPoint UMaxVMax { get; private set; }

            public Dictionary<Wrapping.Boundary, List<IntersectionEdge>> BoundaryEdges { get; private set; } = new Dictionary<Wrapping.Boundary, List<IntersectionEdge>>();

            private IUVParametrized surface;

            private IntersectionEdge StartForwardEdge;
            private IntersectionEdge StartBackwardEdge;

            #endregion

        }

        #endregion

        #region Public Methods

		public static Vector3 FindIntersection(IUVParametrized surface, IUParametrized curve, Vector3 nerbyPoint)
		{
			var surfaceWrap = Wrapping.CraftWrapFunc(surface);

			Newton.WrapFuncDelegate<Vector3> newtonWrapFunc = (ref Vector3 arg) =>
			{
				var uv = new Vector2(arg.x, arg.y);

				var pWrapResult = surfaceWrap(ref uv);

				return pWrapResult.IsOutOfBounds || arg.z < curve.UMin || arg.z > curve.UMax;
			};

			Func<Vector3, double> distanceFunction = (Vector3 arg) =>
			(surface.Evaluate(u: arg.x, v: arg.y) - curve.Evaluate(u: arg.z)).LengthSquared;

			Func<Vector3, Vector3> distanceGradient = (Vector3 arg) =>
			{
				var diff = (surface.Evaluate(u: arg.x, v: arg.y) - curve.Evaluate(u: arg.z));

				var du = surface.EvaluateUDerivative(u: arg.x, v: arg.y);
				var dv = surface.EvaluateVDerivative(u: arg.x, v: arg.y);
				var dt = curve.EvaluateDerivative(u: arg.z);

				return 2.0 * new Vector3(diff * du, diff * dv, -diff * dt);
			};

			return Gradients.Simple(distanceFunction, distanceGradient, AutoSelectStartPoint(surface, curve, nerbyPoint), newtonWrapFunc);
		}

        public static Tuple<Trimmer, Trimmer, IntersectionPoint, IntersectionPoint> Intersect(IUVParametrized P, IUVParametrized Q, Vector3 cursorPos, Action<Vector4> newPoint, double d = 0.04)
        {
            var points = FindIntersection(P, Q, cursorPos, newPoint, d);

            return Tuple.Create(PostprocessIntersection(P, points.Item1), PostprocessIntersection(Q, points.Item2), points.Item1, points.Item2);
        }

        public static Tuple<IntersectionPoint, IntersectionPoint> FindIntersection(IUVParametrized P, IUVParametrized Q, Vector3 cursorPos, Action<Vector4> newPoint, double d)
        {
            var pWrap = Wrapping.CraftWrapFunc(P);
            var qWrap = Wrapping.CraftWrapFunc(Q);

            Newton.WrapFuncDelegate<Vector4> newtonWrapFunc = (ref Vector4 arg) =>
            {
                var uv = new Vector2(arg.x, arg.y);
                var st = new Vector2(arg.z, arg.w);

                var pWrapResult = pWrap(ref uv);
                var qWrapResult = qWrap(ref st);

                return pWrapResult.IsOutOfBounds || qWrapResult.IsOutOfBounds;
            };

            Func<Vector4, double> distanceFunction = (Vector4 arg) =>
            (P.Evaluate(u: arg.x, v: arg.y) - Q.Evaluate(u: arg.z, v: arg.w)).LengthSquared;

            Func<Vector4, Vector4> distanceGradient = (Vector4 arg) =>
            {
                var diff = (P.Evaluate(u: arg.x, v: arg.y) - Q.Evaluate(u: arg.z, v: arg.w));

                var du = P.EvaluateUDerivative(u: arg.x, v: arg.y);
                var dv = P.EvaluateVDerivative(u: arg.x, v: arg.y);
                var ds = Q.EvaluateUDerivative(u: arg.z, v: arg.w);
                var dt = Q.EvaluateVDerivative(u: arg.z, v: arg.w);

                return 2.0 * new Vector4(diff * du, diff * dv, -diff * ds, -diff * dt);
            };

            var startParams = Gradients.Simple(distanceFunction, distanceGradient, AutoSelectStartPoint(P, Q, cursorPos), newtonWrapFunc);

            var pStart = P.Evaluate(startParams.x, startParams.y);
            var qStart = Q.Evaluate(startParams.z, startParams.w);

            newPoint(startParams);

            if ((pStart - qStart).LengthSquared > Constants.Eps)
                throw new Exception("Too distant surfaces");

            newPoint(startParams);

            Func<Vector4, bool, Matrix4x4> jacobian = (Vector4 arg, bool invert) =>
            {
                var du = P.EvaluateUDerivative(u: arg.x, v: arg.y);
                var dv = P.EvaluateVDerivative(u: arg.x, v: arg.y);
                var ds = Q.EvaluateUDerivative(u: arg.z, v: arg.w);
                var dt = Q.EvaluateVDerivative(u: arg.z, v: arg.w);

                var np = Vector3.Cross(du, dv);
                var nq = Vector3.Cross(ds, dt);

                var t = (invert ? Vector3.Cross(nq, np) : Vector3.Cross(np, nq)).Normalized();

                return new Matrix4x4()
                {
                    v11 = du.x,
                    v12 = dv.x,
                    v13 = -ds.x,
                    v14 = -dt.x,
                    v21 = du.y,
                    v22 = dv.y,
                    v23 = -ds.y,
                    v24 = -dt.y,
                    v31 = du.z,
                    v32 = dv.z,
                    v33 = -ds.z,
                    v34 = -dt.z,
                    v41 = t * du,
                    v42 = t * dv,
                    v43 = 0.0,
                    v44 = 0.0
                };
            };

            Func<Vector4, Vector3, bool, Vector4> differenceFunc = (Vector4 arg, Vector3 p0, bool invert) =>
            {
                var du = P.EvaluateUDerivative(u: arg.x, v: arg.y);
                var dv = P.EvaluateVDerivative(u: arg.x, v: arg.y);
                var ds = Q.EvaluateUDerivative(u: arg.z, v: arg.w);
                var dt = Q.EvaluateVDerivative(u: arg.z, v: arg.w);

                var np = Vector3.Cross(du, dv);
                var nq = Vector3.Cross(ds, dt);

                var t = (invert ? Vector3.Cross(nq, np) : Vector3.Cross(np, nq)).Normalized();

                var p = P.Evaluate(u: arg.x, v: arg.y);
                var q = Q.Evaluate(u: arg.z, v: arg.w);

                return (p - q).AddW(t * (p - p0) - d);
            };

            var startPoint = pStart;

            var prevParams = startParams;

            var prevPoint = startPoint;

            var prevDistance = 0.0;

            var pStartPoint = new IntersectionPoint() { Type = PointType.Start, UV = new Vector2(startParams.x, startParams.y) };
            var qStartPoint = new IntersectionPoint() { Type = PointType.Start, UV = new Vector2(startParams.z, startParams.w) };

            var pPoint = pStartPoint;
            var qPoint = qStartPoint;

            while (true)
            {
                var newParams = Newton.Solve((Vector4 arg) => differenceFunc(arg, prevPoint, false), (Vector4 arg) => jacobian(arg, false), newtonWrapFunc, prevParams);

                var uv = new Vector2(newParams.x, newParams.y);
                var st = new Vector2(newParams.z, newParams.w);

                var pWrapResult = pWrap(ref uv);
                var qWrapResult = qWrap(ref st);

                bool boundsHit = false;

                if (pWrapResult.IsOutOfBounds)
                {
                    Wrapping.Boundary resultBoundary;
                    var intersection = Wrapping.BoundaryIntersection(P, new Vector2(prevParams.x, prevParams.y), new Vector2(newParams.x, newParams.y), out resultBoundary);

                    pPoint.Next = new IntersectionPoint() { Type = PointType.BoundHit, UV = intersection, Boundary = resultBoundary, Previous = pPoint };
                    pPoint = pPoint.Next;

                    boundsHit = true;
                }
                else if (pWrapResult.IsWrap)
                {
                    Wrapping.Boundary resultBoundary;
                    var intersection = Wrapping.BoundaryIntersection(P, new Vector2(prevParams.x, prevParams.y), new Vector2(newParams.x, newParams.y), out resultBoundary);
                    pPoint.Next = new IntersectionPoint() { Type = PointType.BoundWrapStart, UV = intersection, Boundary = resultBoundary, Previous = pPoint };
                    pPoint = pPoint.Next;

                    var wrappedIntersection = pWrapResult.ForceWrap(P, intersection);
                    pPoint.Next = new IntersectionPoint() { Type = PointType.BoundWrapEnd, UV = wrappedIntersection, Boundary = Wrapping.OppositeBoundary(resultBoundary), Previous = pPoint };
                    pPoint = pPoint.Next;

                    pPoint.Next = new IntersectionPoint() { Type = PointType.Regular, UV = uv, Previous = pPoint };
                    pPoint = pPoint.Next;
                }
                else
                {
                    pPoint.Next = new IntersectionPoint() { Type = PointType.Regular, UV = uv, Previous = pPoint };
                    pPoint = pPoint.Next;
                }

                if (qWrapResult.IsOutOfBounds)
                {
                    Wrapping.Boundary resultBoundary;
                    var intersection = Wrapping.BoundaryIntersection(Q, new Vector2(prevParams.z, prevParams.w), new Vector2(newParams.z, newParams.w), out resultBoundary);

                    qPoint.Next = new IntersectionPoint() { Type = PointType.BoundHit, UV = intersection, Boundary = resultBoundary, Previous = qPoint };
                    qPoint = qPoint.Next;

                    boundsHit = true;
                }
                else if (qWrapResult.IsWrap)
                {
                    Wrapping.Boundary resultBoundary;
                    var intersection = Wrapping.BoundaryIntersection(Q, new Vector2(prevParams.z, prevParams.w), new Vector2(newParams.z, newParams.w), out resultBoundary);
                    qPoint.Next = new IntersectionPoint() { Type = PointType.BoundWrapStart, UV = intersection, Boundary = resultBoundary, Previous = qPoint };
                    qPoint = qPoint.Next;

                    var wrappedIntersection = qWrapResult.ForceWrap(Q, intersection);
                    qPoint.Next = new IntersectionPoint() { Type = PointType.BoundWrapEnd, UV = wrappedIntersection, Boundary = Wrapping.OppositeBoundary(resultBoundary), Previous = qPoint };
                    qPoint = qPoint.Next;

                    qPoint.Next = new IntersectionPoint() { Type = PointType.Regular, UV = st, Previous = qPoint };
                    qPoint = qPoint.Next;
                }
                else
                {
                    qPoint.Next = new IntersectionPoint() { Type = PointType.Regular, UV = st, Previous = qPoint };
                    qPoint = qPoint.Next;
                }

                if (boundsHit)
                    break;

                newPoint(new Vector4(uv.x, uv.y, st.x, st.y));

                prevParams = new Vector4(uv.x, uv.y, st.x, st.y);

                prevPoint = P.Evaluate(u: prevParams.x, v: prevParams.y);

                var dist = (prevPoint - startPoint).LengthSquared;

                if (dist < 4.0 * d * d && prevDistance > dist)
                {
                    pPoint.Next = pStartPoint;
                    qPoint.Next = qStartPoint;
                    pStartPoint.Previous = pPoint;
                    qStartPoint.Previous = qPoint;
                    return new Tuple<IntersectionPoint, IntersectionPoint>(pStartPoint, qStartPoint);
                }

                prevDistance = dist;
            }

            prevParams = startParams;

            prevPoint = startPoint;

            prevDistance = 0.0;

            pPoint = pStartPoint;
            qPoint = qStartPoint;

            while (true)
            {
                var newParams = Newton.Solve((Vector4 arg) => differenceFunc(arg, prevPoint, true), (Vector4 arg) => jacobian(arg, true), newtonWrapFunc, prevParams);

                var uv = new Vector2(newParams.x, newParams.y);
                var st = new Vector2(newParams.z, newParams.w);

                var pWrapResult = pWrap(ref uv);
                var qWrapResult = qWrap(ref st);

                bool boundsHit = false;

                if (pWrapResult.IsOutOfBounds)
                {
                    Wrapping.Boundary resultBoundary;
                    var intersection = Wrapping.BoundaryIntersection(P, new Vector2(prevParams.x, prevParams.y), new Vector2(newParams.x, newParams.y), out resultBoundary);

                    pPoint = new IntersectionPoint() { Type = PointType.BoundHit, UV = intersection, Boundary = resultBoundary, Next = pPoint };
                    pPoint.Next.Previous = pPoint;

                    boundsHit = true;
                }
                else if (pWrapResult.IsWrap)
                {
                    Wrapping.Boundary resultBoundary;
                    var intersection = Wrapping.BoundaryIntersection(P, new Vector2(prevParams.x, prevParams.y), new Vector2(newParams.x, newParams.y), out resultBoundary);
                    pPoint = new IntersectionPoint() { Type = PointType.BoundWrapEnd, UV = intersection, Boundary = resultBoundary, Next = pPoint };
                    pPoint.Next.Previous = pPoint;

                    var wrappedIntersection = pWrapResult.ForceWrap(P, intersection);
                    pPoint = new IntersectionPoint() { Type = PointType.BoundWrapStart, UV = wrappedIntersection, Boundary = Wrapping.OppositeBoundary(resultBoundary), Next = pPoint };
                    pPoint.Next.Previous = pPoint;

                    pPoint = new IntersectionPoint() { Type = PointType.Regular, UV = uv, Next = pPoint };
                    pPoint.Next.Previous = pPoint;
                }
                else
                {
                    pPoint = new IntersectionPoint() { Type = PointType.Regular, UV = uv, Next = pPoint };
                    pPoint.Next.Previous = pPoint;
                }

                if (qWrapResult.IsOutOfBounds)
                {
                    Wrapping.Boundary resultBoundary;
                    var intersection = Wrapping.BoundaryIntersection(Q, new Vector2(prevParams.z, prevParams.w), new Vector2(newParams.z, newParams.w), out resultBoundary);

                    qPoint = new IntersectionPoint() { Type = PointType.BoundHit, UV = intersection, Boundary = resultBoundary, Next = qPoint };
                    qPoint.Next.Previous = qPoint;

                    boundsHit = true;
                }
                else if (qWrapResult.IsWrap)
                {
                    Wrapping.Boundary resultBoundary;
                    var intersection = Wrapping.BoundaryIntersection(Q, new Vector2(prevParams.z, prevParams.w), new Vector2(newParams.z, newParams.w), out resultBoundary);
                    qPoint = new IntersectionPoint() { Type = PointType.BoundWrapEnd, UV = intersection, Boundary = resultBoundary, Next = qPoint };
                    qPoint.Next.Previous = qPoint;

                    var wrappedIntersection = qWrapResult.ForceWrap(Q, intersection);
                    qPoint = new IntersectionPoint() { Type = PointType.BoundWrapStart, UV = wrappedIntersection, Boundary = Wrapping.OppositeBoundary(resultBoundary), Next = qPoint };
                    qPoint.Next.Previous = qPoint;

                    qPoint = new IntersectionPoint() { Type = PointType.Regular, UV = st, Next = qPoint };
                    qPoint.Next.Previous = qPoint;
                }
                else
                {
                    qPoint = new IntersectionPoint() { Type = PointType.Regular, UV = st, Next = qPoint };
                    qPoint.Next.Previous = qPoint;
                }

                if (boundsHit)
                    return new Tuple<IntersectionPoint, IntersectionPoint>(pStartPoint, qStartPoint);

                newPoint(new Vector4(uv.x, uv.y, st.x, st.y));

                prevParams = new Vector4(uv.x, uv.y, st.x, st.y);

                prevPoint = P.Evaluate(u: prevParams.x, v: prevParams.y);

                var dist = (prevPoint - startPoint).LengthSquared;

                if (dist < 4.0 * d * d && prevDistance > dist)
                {
                    pStartPoint.Next = pPoint;
                    qStartPoint.Next = qPoint;
                    pPoint.Previous = pStartPoint;
                    qPoint.Previous = qStartPoint;
                    return new Tuple<IntersectionPoint, IntersectionPoint>(pStartPoint, qStartPoint);
                }

                prevDistance = dist;
            }

        }

        public static Trimmer PostprocessIntersection(IUVParametrized surf, IntersectionPoint start)
        {
            var graph = new IntersectionGraph(surf);

            graph.InsertIntersectionPoints(start);

            return graph.ClassifyEdges();
        }

        #endregion

        #region Private Helpers

		private static Vector3 AutoSelectStartPoint(IUVParametrized surface, IUParametrized curve, Vector3 cursorPos, int subdivision = 16)
		{
			var surfaceNearest = FindNearestPoint(surface, cursorPos, subdivision);
			var curveNearest = FindNearestPoint(curve, cursorPos, subdivision);
			return new Vector3(surfaceNearest.x, surfaceNearest.y, curveNearest);
		}

        private static Vector4 AutoSelectStartPoint(IUVParametrized P, IUVParametrized Q, Vector3 cursorPos, int subdivision = 16)
        {
            var pNearest = FindNearestPoint(P, cursorPos, subdivision);
            var qNearest = FindNearestPoint(Q, cursorPos, subdivision);
            return new Vector4(pNearest.x, pNearest.y, qNearest.x, qNearest.y);
        }

		private static double FindNearestPoint(IUParametrized curve, Vector3 point, int subdivision = 10)
		{
			var du = (curve.UMax - curve.UMin) / subdivision;

			var min = curve.UMin;

			var minDist = double.MaxValue;

			for (int u = 0; u <= subdivision; ++u)
			{
				var dist = (curve.Evaluate(u * du) - point).LengthSquared;
				if (dist < minDist)
				{
					minDist = dist;
					min = u * du;
				}
			}

			Newton.WrapFuncDelegate<double> wrapFunc = (ref double arg) =>
			{
				if (arg < curve.UMin || arg > curve.UMax)
					return true;
				return false;
			};

			Func<double, double> distanceFunc = (double arg) => (curve.Evaluate(u: arg) - point).LengthSquared;

			Func<double, double> distanceGradient = (double arg) =>
			{
				var diff = (curve.Evaluate(u: arg) - point);

				var cdu = curve.EvaluateDerivative(u: arg);

				return 2.0 * diff * cdu;
			};

			return Gradients.Simple(distanceFunc, distanceGradient, min, wrapFunc);
		}

        private static Vector2 FindNearestPoint(IUVParametrized surf, Vector3 point, int subdivision = 10)
        {
            var du = (surf.UMax - surf.UMin) / subdivision;
            var dv = (surf.VMax - surf.VMin) / subdivision;

            var min = new Vector2(surf.UMin, surf.VMin);

            var minDist = double.MaxValue;

            for (int u = 0; u <= subdivision; ++u)
            {
                for (int v = 0; v <= subdivision; ++v)
                {
                    var dist = (surf.Evaluate(u * du, v * dv) - point).LengthSquared;
                    if (dist < minDist)
                    {
                        minDist = dist;
                        min = new Vector2(u * du, v * dv);
                    }
                }
            }

            var wrap = Wrapping.CraftWrapFunc(surf);

            Newton.WrapFuncDelegate<Vector2> wrapFunc = (ref Vector2 arg) =>
            {
                var uv = arg;

                var pWrapResult = wrap(ref uv);

                return pWrapResult.IsOutOfBounds;
            };

            Func<Vector2, double> distanceFunc = (Vector2 arg) => (surf.Evaluate(u: arg.x, v: arg.y) - point).LengthSquared;

            Func<Vector2, Vector2> distanceGradient = (Vector2 arg) =>
            {
                var diff = (surf.Evaluate(u: arg.x, v: arg.y) - point);

                var sdu = surf.EvaluateUDerivative(u: arg.x, v: arg.y);
                var sdv = surf.EvaluateVDerivative(u: arg.x, v: arg.y);

                return 2.0 * new Vector2(diff * sdu, diff * sdv);
            };

            return Gradients.Simple(distanceFunc, distanceGradient, min, wrapFunc);
        }

        #endregion

    }
}