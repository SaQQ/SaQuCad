﻿using System;

namespace SaQuCAD.Maths
{
	public interface IScaleable<T>
	{
		T Multiply(double scale);

		T Divide(double scale);
	}

	public interface ISummable<T>
	{
		T Add(T oth);

		T Subtract(T oth);
	}

	public struct Vector2 : IScaleable<Vector2>
	{
		public double x;
		public double y;

		public Vector2(double x, double y)
		{
			this.x = x;
			this.y = y;
		}

		public double Length => Math.Sqrt(this * this);
		public double LengthSquared => this * this;

		public Vector2 Normalized()
		{
			double len = Length;
			return new Vector2(x / len, y / len);
		}

		public Vector2 PointwiseMultiply(Vector2 oth)
		{
			Vector2 v;
			v.x = x * oth.x;
			v.y = y * oth.y;
			return v;
		}

		public static bool operator ==(Vector2 lhs, Vector2 rhs)
		{
			return Math.Abs(lhs.x - rhs.x) < Constants.Eps && Math.Abs(lhs.y - rhs.y) < Constants.Eps;
		}

		public static bool operator !=(Vector2 lhs, Vector2 rhs)
		{
			return !(lhs == rhs);
		}

		public static Vector2 operator +(Vector2 lhs, Vector2 rhs)
		{
			return new Vector2(lhs.x + rhs.x, lhs.y + rhs.y);
		}

		public static Vector2 operator -(Vector2 lhs, Vector2 rhs)
		{
			return lhs + -rhs;
		}

		public static Vector2 operator -(Vector2 v)
		{
			return new Vector2(-v.x, -v.y);
		}

		public Vector2 Multiply(double s)
		{
			return new Vector2(s * x, s * y);
		}

		public static Vector2 operator *(double s, Vector2 v)
		{
			return v.Multiply(s);
		}

		public static Vector2 operator *(Vector2 v, double s)
		{
			return v.Multiply(s);
		}

		public Vector2 Divide(double s)
		{
			return new Vector2(x / s, y / s);
		}

		public static Vector2 operator /(Vector2 v, double s)
		{
			return v.Divide(s);
		}

		public static double operator *(Vector2 lhs, Vector2 rhs)
		{
			return lhs.x * rhs.x + lhs.y * rhs.y;
		}

		public override bool Equals(object obj)
		{
			try
			{
				return this == (Vector2)obj;
			}
			catch
			{
				return false;
			}
		}

		public override int GetHashCode()
		{
			return base.GetHashCode();
		}

        public override string ToString()
        {
            return string.Format("({0:0.00}, {1:0.00})", x, y);
        }

    }

	public struct Vector3 : IScaleable<Vector3>, ISummable<Vector3>
	{
		public double x;
		public double y;
		public double z;

		public Vector3(double x, double y, double z)
		{
			this.x = x;
			this.y = y;
			this.z = z;
		}

		public Vector4 AddW(double w = 1.0)
		{
			return new Vector4(x, y, z, w);
		}

		public double Length => Math.Sqrt(this * this);
		public double LengthSquared => this * this;

		public Vector3 Normalized()
		{
			double len = Length;
			return new Vector3(x / len, y / len, z / len);
		}

		public static Vector3 Cross(Vector3 lhs, Vector3 rhs)
		{
			Vector3 v;
			v.x = lhs.y * rhs.z - lhs.z * rhs.y;
			v.y = -lhs.x * rhs.z + lhs.z * rhs.x;
			v.z = lhs.x * rhs.y - lhs.y * rhs.x;
			return v;
		}

		public Vector3 PointwiseMultiply(Vector3 oth)
		{
			Vector3 v;
			v.x = x * oth.x;
			v.y = y * oth.y;
			v.z = z * oth.z;
			return v;
		}

		public static bool operator ==(Vector3 lhs, Vector3 rhs)
		{
			return lhs.x == rhs.x && lhs.y == rhs.y && lhs.z == rhs.z;
		}

		public static bool operator !=(Vector3 lhs, Vector3 rhs)
		{
			return !(lhs == rhs);
		}

		public Vector3 Add(Vector3 oth)
		{
			return new Vector3(x + oth.x, y + oth.y, z + oth.z);
		}

		public static Vector3 operator +(Vector3 lhs, Vector3 rhs)
		{
			return lhs.Add(rhs);
		}

		public Vector3 Subtract(Vector3 oth)
		{
			return new Vector3(x - oth.x, y - oth.y, z - oth.z);
		}

		public static Vector3 operator -(Vector3 lhs, Vector3 rhs)
		{
			return lhs.Subtract(rhs);
		}

		public static Vector3 operator -(Vector3 v)
		{
			return new Vector3(-v.x, -v.y, -v.z);
		}

		public Vector3 Multiply(double s)
		{
			return new Vector3(s * x, s * y, s * z);
		}

		public static Vector3 operator *(double s, Vector3 v)
		{
			return v.Multiply(s);
		}

		public static Vector3 operator *(Vector3 v, double s)
		{
			return v.Multiply(s);
		}

		public Vector3 Divide(double s)
		{
			return new Vector3(x / s, y / s, z / s);
		}

		public static Vector3 operator /(Vector3 v, double s)
		{
			return v.Divide(s);
		}

		public static double operator *(Vector3 lhs, Vector3 rhs)
		{
			return lhs.x * rhs.x + lhs.y * rhs.y + lhs.z * rhs.z;
		}

		public double this[int i]
		{
			get
			{
				switch (i)
				{
					case 0: return x;
					case 1: return y;
					case 2: return z;
					default: throw new ArgumentException("Invalid Vector3 index", nameof(i));
				}
			}
			set
			{
				switch (i)
				{
					case 0: x = value; break;
					case 1: y = value; break;
					case 2: z = value; break;
					default: throw new ArgumentException("Invalid Vector3 index", nameof(i));
				}
			}
		}

		public override bool Equals(object obj)
		{
			try
			{
				return this == (Vector3)obj;
			}
			catch
			{
				return false;
			}
		}

		public override int GetHashCode()
		{
			return base.GetHashCode();
		}

        public override string ToString()
        {
            return string.Format("({0:0.00}, {1:0.00}, {2:0.00})", x, y, z);
        }

    }

	public struct Vector4 : IScaleable<Vector4>
	{
		public double x;
		public double y;
		public double z;
		public double w;

		public Vector4(double x, double y, double z, double w)
		{
			this.x = x;
			this.y = y;
			this.z = z;
			this.w = w;
		}

		public Vector3 WithoutW()
		{
			return new Vector3(x, y, z);
		}

		public static explicit operator Vector3(Vector4 oth)
		{
			return new Vector3(oth.x, oth.y, oth.z);
		}

		public double Length => Math.Sqrt(this * this);
		public double LengthSquared => this * this;

		public Vector4 Normalized()
		{
			double len = Length;
			return new Vector4(x / len, y / len, z / len, w / len);
		}

		public Vector4 WNormalized()
		{
			return new Vector4(x / w, y / w, z / w, 1.0);
		}

		public Matrix4x4 MatrixMultiply(Vector4 oth)
		{
			Matrix4x4 mat;

			mat.v11 = x * oth.x;
			mat.v21 = y * oth.x;
			mat.v31 = z * oth.x;
			mat.v41 = w * oth.x;

			mat.v12 = x * oth.y;
			mat.v22 = y * oth.y;
			mat.v32 = z * oth.y;
			mat.v42 = w * oth.y;
		
			mat.v13 = x * oth.z;
			mat.v23 = y * oth.z;
			mat.v33 = z * oth.z;
			mat.v43 = w * oth.z;

			mat.v14 = x * oth.w;
			mat.v24 = y * oth.w;
			mat.v34 = z * oth.w;
			mat.v44 = w * oth.w;

			return mat;
		}

		public static bool operator ==(Vector4 lhs, Vector4 rhs)
		{
			return lhs.x == rhs.x && lhs.y == rhs.y && lhs.z == rhs.z && lhs.w == rhs.w;
		}

		public static bool operator !=(Vector4 lhs, Vector4 rhs)
		{
			return !(lhs == rhs);
		}

		public static Vector4 operator +(Vector4 lhs, Vector4 rhs)
		{
			return new Vector4(lhs.x + rhs.x, lhs.y + rhs.y, lhs.z + rhs.z, lhs.w + rhs.w);
		}

		public static Vector4 operator -(Vector4 lhs, Vector4 rhs)
		{
			return lhs + -rhs;
		}

		public static Vector4 operator -(Vector4 v)
		{
			return new Vector4(-v.x, -v.y, -v.z, -v.w);
		}

		public Vector4 Multiply(double s)
		{
			return new Vector4(s * x, s * y, s * z, s * w);
		}

		public static Vector4 operator *(double s, Vector4 v)
		{
			return v.Multiply(s);
		}

		public static Vector4 operator *(Vector4 v, double s)
		{
			return v.Multiply(s);
		}

		public Vector4 Divide(double s)
		{
			return new Vector4(x / s, y / s, z / s, w / s);
		}

		public static Vector4 operator /(Vector4 v, double s)
		{
			return v.Divide(s);
		}

		public static double operator *(Vector4 lhs, Vector4 rhs)
		{
			return lhs.x * rhs.x + lhs.y * rhs.y + lhs.z * rhs.z + lhs.w * rhs.w;
		}

		public double this[int i]
		{
			get
			{
				switch (i)
				{
					case 0: return x;
					case 1: return y;
					case 2: return z;
					case 3: return w;
					default: throw new ArgumentException("Invalid vector 4 index", nameof(i));
				}
			}
			set
			{
				switch (i)
				{
					case 0: x = value; break;
					case 1: y = value; break;
					case 2: z = value; break;
					case 3: w = value; break;
					default: throw new ArgumentException("Invalid vector 4 index", nameof(i));
				}
			}
		}

		public override bool Equals(object obj)
		{
			try
			{
				return this == (Vector4)obj;
			}
			catch
			{
				return false;
			}
		}

		public override int GetHashCode()
		{
			return base.GetHashCode();
		}

        public override string ToString()
        {
            return string.Format("({0:0.00}, {1:0.00}, {2:0.00}, {3:0.00})", x, y, z, w);
        }

    }
}
