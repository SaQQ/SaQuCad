﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SaQuCAD.Shapes
{
	public class EllipsoidRaycastView : RaycastView
	{
		private Matrix4x4 Ellipsoid;
		private Vector3 radii;
		private Matrix4x4 mvpInversed;
		private Matrix4x4 projectionInversed;
		private Matrix4x4 mvInversed;
		private Matrix4x4 mv;

		public EllipsoidRaycastView(Matrix4x4 model, Matrix4x4 view, Matrix4x4 projection, double xRadius, double yRadius, double zRadius)
		{
			Matrix4x4 D = new Matrix4x4();
			radii.x = D.v11 = xRadius;
			radii.y = D.v22 = yRadius;
			radii.z = D.v33 = zRadius;
			D.v44 = -1.0;

			mv = view * model;
			mvInversed = mv.Inverse();
			projectionInversed = projection.Inverse();
			mvpInversed = mvInversed * projectionInversed;

			Ellipsoid = mvpInversed.Transpose() * D * mvpInversed;
		}

		public Tuple<Vector4, Vector3, double> Cast(double ndcX, double ndcY)
		{
			Vector4 znull = new Vector4(ndcX, ndcY, 0.0, 1.0);

			Vector4 row3 = Ellipsoid.Row3();
			Vector4 col3 = Ellipsoid.Col3();

			double c = znull * Ellipsoid * znull;
			double b = row3 * znull + col3 * znull;
			double a = Ellipsoid.v33;

			double delta = b * b - 4 * a * c;

			if (delta < 0.0)
				return null;

			double ndcZ = (-b - (a > 0.0 ? Math.Sqrt(delta) : -Math.Sqrt(delta))) / (2 * a);

			if (ndcZ < -1.0 | ndcZ > 1.0)
				return null;

			var ncdIntersection = new Vector4(ndcX, ndcY, ndcZ, 1.0);
			var localIntersection = mvInversed * (projectionInversed * ncdIntersection).WNormalized();
			var localNormal = localIntersection.WithoutW().PointwiseMultiply(2.0 * radii).Normalized();

			return new Tuple<Vector4, Vector3, double>(mv * localIntersection, ((Matrix3x3)mvInversed).Transpose() * localNormal, ndcZ);
		}

		public void Dispose()
		{
			
		}
	}

	public class Ellipsoid : ImplicitMesh
	{
		public Ellipsoid(double xR, double yR, double zR)
		{
			xRadius = xR;
			yRadius = yR;
			zRadius = zR;
		}

		private double xRadius;

		public double XRadius
		{
			get { return 1.0 / Math.Sqrt(xRadius); }
			set { xRadius = 1.0 / (value * value); OnChanged(); }
		}

		private double yRadius;

		public double YRadius
		{
			get { return 1.0 / Math.Sqrt(yRadius); }
			set { yRadius = 1.0 / (value * value); OnChanged(); }
		}

		private double zRadius;

		public double ZRadius
		{
			get { return 1.0 / Math.Sqrt(zRadius); }
			set { zRadius = 1.0 / (value * value); OnChanged(); }
		}

		public override RaycastView GetRaycastView(Matrix4x4 projection, Matrix4x4 view)
		{
			return new EllipsoidRaycastView(ModelMatrix, view, projection, xRadius, yRadius, zRadius);
		}
	}
}
