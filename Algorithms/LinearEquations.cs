﻿using SaQuCAD.Maths;

namespace SaQuCAD.Algorithms
{
	public static class LinearEquations
	{

		#region Public Methods

		public static T[] SolveTridiagonal<T>(double[] a, double[] d, double[] c, T[] b)
			where T : IScaleable<T>, ISummable<T>
		{
			var N = d.Length;

			double[] l = new double[N];
			double[] u = new double[N];

			u[1] = d[1];

			for (int i = 2; i < N; ++i)
			{
				l[i] = a[i] / u[i - 1];
				u[i] = d[i] - l[i] * c[i - 1];
			}

			T[] y = new T[N + 1];
			T[] x = new T[N + 1];

			y[1] = b[1];

			for (int i = 2; i < N; ++i)
				y[i] = b[i].Subtract(y[i - 1].Multiply(l[i]));

			x[N - 1] = y[N - 1].Divide(u[N - 1]);

			for (int i = N - 2; i > 0; --i)
				x[i] = (y[i].Subtract(x[i + 1].Multiply(c[i]))).Divide(u[i]);

			return x;
		}

		#endregion

	}
}
