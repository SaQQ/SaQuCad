﻿using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Threading;

namespace SaQuCAD
{
	public partial class MainWindow : Window
	{

		#region Private Data

		private WriteableBitmap bitmap;
		private DispatcherTimer timer = new DispatcherTimer();
		private Stopwatch watch = new Stopwatch();
		private long lastTicks;

		private bool sizeChanged = true;
		private Size size;

        private bool redrawRequested = true;

		private Scene scene;

		#endregion

		#region Constructors

		public MainWindow()
		{
			InitializeComponent();

			DataContext = scene = new Scene();

			KeyState.HookEvents(Raster, Raster);
			Raster.KeyUp += Raster_KeyUp;
			Raster.MouseWheel += Raster_MouseWheel;

			var binder = new SelectedItemsBinder(ObjectList, scene.SelectedObjects);
			binder.Bind();

			scene.Changed += (bool recreateRenderer) =>
			{
				if (recreateRenderer)
					sizeChanged = true;
				RequestRedraw();
			};

            ColorCodes.RenderModeChanged += () => { RequestRedraw(); };

			SetupTimers();
		}

		#endregion

		#region Private Helpers

		private void SetupTimers()
		{
			timer.Interval = new TimeSpan(1000);
			timer.Tick += Tick;
			timer.Start();
			watch.Start();
		}

		private void Tick(object sender, EventArgs e)
		{
			Update();
			if (redrawRequested)
			{
				Draw();
                redrawRequested = false;
			}
		}
		
		private void Update()
		{
			var ticks = Math.Min(watch.ElapsedMilliseconds, 40);
			scene.Camera.Update(ticks / 1000.0);
			lastTicks = ticks;
			watch.Reset();
			watch.Start();
		}

		private void RequestRedraw()
		{
			redrawRequested = true;
		}

		private void Draw()
		{
			if (sizeChanged)
			{
				sizeChanged = false;

				bitmap = new WriteableBitmap(Math.Max((int)size.Width, 1), Math.Max((int)size.Height, 1), 96, 96, PixelFormats.Bgra32, null);
				Raster.Source = bitmap;

				scene.Renderer = createRenderer(size, scene.Stereocopy, scene.FieldOfView, scene.EyeSeparation, scene.Convergence);
			}

			scene.Render(bitmap);
		}

		private void Raster_SizeChanged(object sender, SizeChangedEventArgs e)
		{
			sizeChanged = true;
			size = e.NewSize;
			RequestRedraw();
		}

		private static Renderer createRenderer(Size size, bool stereo, double fieldOfView, double eyeSeparation, double convergence)
		{
			Renderer renderer;

			if (stereo)
				renderer = new StereoRenderer(size, fieldOfView, eyeSeparation, convergence);
			else
				renderer = new MonoRenderer(size, fieldOfView);

			return renderer;
		}

		private void Raster_MouseWheel(object sender, MouseWheelEventArgs e)
		{
			scene.Camera.Distance += e.Delta / 500.0;
		}

		private void Raster_MouseUp(object sender, MouseButtonEventArgs e)
		{
            if (e.ChangedButton != MouseButton.Left)
                return;

            ((UIElement)sender).ReleaseMouseCapture();

            var pt = e.GetPosition(Raster);

			scene.VisualState.MouseUp(new IVector2((int)pt.X, (int)pt.Y));
		}

        private void Raster_MouseDown(object sender, MouseButtonEventArgs e)
        {
            Raster.Focus();

            if (e.ChangedButton != MouseButton.Left)
                return;

			((UIElement)sender).CaptureMouse();

			var pt = e.GetPosition(Raster);

            scene.VisualState.MouseDown(new IVector2((int)pt.X, (int)pt.Y));
        }

		private void Raster_MouseMove(object sender, MouseEventArgs e)
		{
            var pt = e.GetPosition(Raster);

			scene.VisualState.MouseMove(new IVector2((int)pt.X, (int)pt.Y));
		}

        private void Raster_KeyUp(object sender, KeyEventArgs e)
        {
			scene.VisualState.KeyUp(e.Key);
        }

		#endregion

	}
}
