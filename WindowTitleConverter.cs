﻿using System;
using System.Globalization;
using System.Windows.Data;

namespace SaQuCAD
{
	public class WindowTitleConverter : IValueConverter
	{

        #region Public Properties

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
		{
			string openFilePath = value as string;

			return System.Reflection.Assembly.GetExecutingAssembly().GetName().Name + (openFilePath == null ? "" : (" - " + openFilePath));
		}

		public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
		{
			throw new NotImplementedException();
		}

        #endregion

    }

}
