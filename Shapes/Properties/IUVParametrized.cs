﻿using SaQuCAD.Maths;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Windows.Input;

namespace SaQuCAD.Shapes.Properties
{
    [Flags]
    public enum ParamProps
    {
        Cyclic = 1,
		Inverted = 2
    }

    public enum TrimMode
    {
        Each, Any
    }

	public interface ITrimmer
	{
        bool Inverted { get; set; }

        bool Test(double u, double v);

        event ChangedEventHandler Changed;

		ICommand ShowUV { get; set; }
	}

	
	public interface IUParametrized
	{
		double UMin { get; }
		double UMax { get; }

		Vector3 Evaluate(double u);
		Vector3 EvaluateDerivative(double u);
	}


    public interface IUVParametrized
    {
        int USegments { get; set; }
        int VSegments { get; set; }

        double UMin { get; }
        double UMax { get; }

        double VMin { get; }
        double VMax { get; }

        ParamProps UProperties { get; }
        ParamProps VProperties { get; }

        Vector3 Evaluate(double u, double v);
        Vector3 EvaluateUDerivative(double u, double v);
        Vector3 EvaluateVDerivative(double u, double v);

        ObservableCollection<ITrimmer> Trimmers { get; }

        TrimMode TrimMode { get; set; }
	}
}
