﻿using SaQuCAD.Shapes.Properties;
using System.Collections.Generic;

namespace SaQuCAD.Shapes.Surfaces
{
	public abstract class ControlpointsBase : UVMesh, IPointBased, IFrameable
	{

		#region Public Properties

		public Point[,] ControlPoints { get; private set; }

		public bool FrameVisible { get { return frameVisible; } set { frameVisible = value; RaisePropertyChanged(); OnChanged(); } }

        #endregion

        #region Constructors

        public ControlpointsBase(Point[,] points)
		{
			ControlPoints = points;

            foreach (var p in ControlPoints)
            {
                p.Removed += PointRemoved;
                p.Changed += PointChanged;
            }

            RecreateMesh();
        }  

        #endregion

        #region Public Methods

		public int SwapPoint(Point existing, Point replacement)
		{
			int swapped = 0;

			for (int u = 0; u < ControlPoints.GetLength(0); ++u)
			{
				for (int v = 0; v < ControlPoints.GetLength(1); ++v)
				{
					if (ControlPoints[u, v] == existing)
					{
						ControlPoints[u, v].Removed -= PointRemoved;
						ControlPoints[u, v].Changed -= PointChanged;
						ControlPoints[u, v] = replacement;
						ControlPoints[u, v].Removed += PointRemoved;
						ControlPoints[u, v].Changed += PointChanged;
						swapped++;
					}
				}
			}

			if (swapped > 0)
				OnChanged();
			
			return swapped;
		}

		#endregion

		#region Protected Methods

		protected void DrawFrame(Renderer renderer)
		{
			int U = ControlPoints.GetLength(0);
			int V = ControlPoints.GetLength(1);

			for (int i = 0; i < U - 1; ++i)
				for (int j = 0; j < V - 1; ++j)
				{
					renderer.DrawSegment(ControlPoints[i, j].Position, ControlPoints[i, j + 1].Position, ColorCodes.RenderFrame);
					renderer.DrawSegment(ControlPoints[i, j].Position, ControlPoints[i + 1, j].Position, ColorCodes.RenderFrame);
				}

			for (int i = 0; i < V - 1; ++i)
				renderer.DrawSegment(ControlPoints[U - 1, i].Position, ControlPoints[U - 1, i + 1].Position, ColorCodes.RenderFrame);

			for (int i = 0; i < U - 1; ++i)
				renderer.DrawSegment(ControlPoints[i, V - 1].Position, ControlPoints[i + 1, V - 1].Position, ColorCodes.RenderFrame);
		}

		protected Point[,] GetMirroredPoints(Plane plane)
		{
			var mirroredPoints = new Point[ControlPoints.GetLength(0), ControlPoints.GetLength(1)];

            Dictionary<Point, Point> pointsMap = new Dictionary<Point, Point>();

            for (int u = 0; u < ControlPoints.GetLength(0); ++u)
            {
                for (int v = 0; v < ControlPoints.GetLength(1); ++v)
                {
                    if (pointsMap.ContainsKey(ControlPoints[u, v]))
                    {
                        mirroredPoints[u, v] = pointsMap[ControlPoints[u, v]];
                    }
                    else
                    {
                        var newPoint = ControlPoints[u, v].MirroredThroughPlane(plane);
                        pointsMap.Add(ControlPoints[u, v], newPoint);
                        mirroredPoints[u, v] = newPoint;
                    }
                }
            }

			return mirroredPoints;
		}

        #endregion

        #region Private Helpers

        

        private void PointRemoved(SceneObject obj)
		{
			OnRemoved();
		}

        private void PointChanged()
        {
            RecreateMesh();
        }

		#endregion

		#region Private Data

		private bool frameVisible;

        #endregion

    }
}
