﻿using SaQuCAD.Infrastructure;
using SaQuCAD.Shapes.Properties;
using System.Collections.ObjectModel;
using System.Windows.Input;
using System;

namespace SaQuCAD.Shapes.Curves
{
	public delegate void ControlpointsCurveEditEventHandler(ControlpointsBase bezier);

    public abstract class ControlpointsBase : SceneObject, IEditable, IPointBased
    {
        #region Public Properties

        public ObservableCollection<Point> ControlPoints { get; private set; } = new ObservableCollection<Point>();

        public ICommand Edit { get; }

        public event ControlpointsCurveEditEventHandler EditRequested;

        #endregion

        #region Constructors

        public ControlpointsBase(Point[] points)
        {
            ControlPoints.CollectionChanged += (object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e) => {
                if (e.NewItems != null)
                    foreach (var point in e.NewItems)
                    {
                        (point as Point).Removed += PointRemoved;
                        (point as Point).Changed += PointChanged;
                    }
                if (e.OldItems != null)
                    foreach (var point in e.OldItems)
                    {
                        (point as Point).Removed -= PointRemoved;
                        (point as Point).Changed -= PointChanged;
                    }
                OnChanged();
            };

            foreach (var p in points)
                ControlPoints.Add(p);

            Edit = new RelayCommand(() => OnEditRequested());
        }

		#endregion

		#region Public Methods
		
		public int SwapPoint(Point existing, Point replacement)
		{
			int swapped = 0;

			for(int i = 0; i < ControlPoints.Count; ++i)
			{
				if(ControlPoints[i] == existing)
				{
					ControlPoints[i].Removed -= PointRemoved;
					ControlPoints[i].Changed -= PointChanged;
					ControlPoints[i] = replacement;
					ControlPoints[i].Removed += PointRemoved;
					ControlPoints[i].Changed += PointChanged;
					swapped++;
				}
			}

			if (swapped > 0)
				OnChanged();

			return swapped;
		}

		#endregion

		#region Protected Methods

		protected virtual void OnEditRequested()
        {
            EditRequested?.Invoke(this);
        }

        #endregion

        #region Private Helpers

        private void PointRemoved(SceneObject obj)
        {
            ControlPoints.Remove(obj as Point);
        }

        private void PointChanged()
        {
            OnChanged();
        }

        #endregion

    }
}
